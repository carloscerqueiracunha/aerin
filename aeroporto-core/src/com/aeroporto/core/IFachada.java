package com.aeroporto.core;

import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.EntidadeImpl;

/**
 * Interface de Fachada.
 * A fachada � chamada pelo command com uma a��o de mesmo nome,
 * portanto � importante criar o m�todo correspondente aqui.
 * @author Carlos
 *
 */
public interface IFachada {

	public Resultado salvar(EntidadeImpl entidade);
	public Resultado alterar(EntidadeImpl entidade);
	public Resultado excluir(EntidadeImpl entidade);
	public Resultado ativar(EntidadeImpl entidade);
	public Resultado consultar(EntidadeImpl entidade);
	public Resultado visualizar(EntidadeImpl entidade);
	
}
