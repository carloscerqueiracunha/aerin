package com.aeroporto.core;

import com.aeroporto.dominio.EntidadeImpl;

/**
 * Interface de Strategy.
 * Strategy � respons�vel pelas regras de neg�cio do projeto.
 * S� possui um m�todo para processar.
 * @author Carlos
 *
 */
public interface IStrategy 
{

	public String processar(EntidadeImpl entidade);
	
}
