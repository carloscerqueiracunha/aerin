package com.aeroporto.core;

import java.sql.SQLException;
import java.util.List;

import com.aeroporto.dominio.EntidadeImpl;

/**
 * Interface de DAO para ser implementada pelos outros DAOs e usada na fachada.
 * Deve conter todas as a��es que ser�o feitas no banco.
 * @author Carlos
 *
 */
public interface IDAO {

	public void salvar(EntidadeImpl entidade) throws SQLException;
	public void alterar(EntidadeImpl entidade)throws SQLException;
	public void excluir(EntidadeImpl entidade)throws SQLException;
	public void ativar(EntidadeImpl entidade)throws SQLException;
	public List<EntidadeImpl> consultar(EntidadeImpl entidade)throws SQLException;
	
}
