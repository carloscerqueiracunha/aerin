package com.aeroporto.core.impl.controle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aeroporto.core.IDAO;
import com.aeroporto.core.IFachada;
import com.aeroporto.core.IStrategy;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.core.impl.dao.AeroportoDAO;
import com.aeroporto.core.impl.dao.BagagemDAO;
import com.aeroporto.core.impl.dao.CartaoDAO;
import com.aeroporto.core.impl.dao.GraficoDAO;
import com.aeroporto.core.impl.dao.PassageiroDAO;
import com.aeroporto.core.impl.dao.PassagemDAO;
import com.aeroporto.core.impl.dao.PedidoDAO;
import com.aeroporto.core.impl.dao.TrajetoDAO;
import com.aeroporto.core.impl.dao.UsuarioDAO;
import com.aeroporto.core.impl.dao.ViagemDAO;
import com.aeroporto.core.impl.negocio.ValidadorAssentosDisponiveisPedido;
import com.aeroporto.core.impl.negocio.ValidadorCamposGrafico;
import com.aeroporto.core.impl.negocio.ValidadorCidade;
import com.aeroporto.core.impl.negocio.ValidadorDadosObrigatoriosAeroporto;
import com.aeroporto.core.impl.negocio.ValidadorDadosObrigatoriosCartao;
import com.aeroporto.core.impl.negocio.ValidadorDadosObrigatoriosPassageiro;
import com.aeroporto.core.impl.negocio.ValidadorDadosObrigatoriosPedido;
import com.aeroporto.core.impl.negocio.ValidadorDadosObrigatoriosUsuario;
import com.aeroporto.core.impl.negocio.ValidadorDadosObrigatoriosViagem;
import com.aeroporto.core.impl.negocio.ValidadorDataViagem;
import com.aeroporto.core.impl.negocio.ValidadorEmailUnico;
import com.aeroporto.core.impl.negocio.ValidadorEstado;
import com.aeroporto.core.impl.negocio.ValidadorPais;
import com.aeroporto.core.impl.negocio.ValidadorPrecoTotalPedido;
import com.aeroporto.core.impl.negocio.ValidadorSenhaForte;
import com.aeroporto.core.impl.negocio.ValidadorSenhaIgual;
import com.aeroporto.core.impl.negocio.ValidadorTipoPassagemPedido;
import com.aeroporto.core.impl.negocio.ValidadorTrajetoIgual;
import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.Bagagem;
import com.aeroporto.dominio.Cartao;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Grafico;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Passagem;
import com.aeroporto.dominio.Pedido;
import com.aeroporto.dominio.Trajeto;
import com.aeroporto.dominio.Usuario;
import com.aeroporto.dominio.Viagem;

/**
 * Fachada � chamada pelo command para realizar as a��es do sistema
 * Possui um mapa de DAOs e de regras de neg�cio que ser�o chamadas
 * Seu construtor mapeia os DAOs e regras de neg�cio
 * Seus m�todos chamam as regras de neg�cio e o DAO respons�vel pelo ojeto que vier como par�metro
 * @author Carlos
 *
 */
public class Fachada implements IFachada {

	/**
	 * Mapa de DAOs onde ser�o armazenados os DAOs de cada entidade
	 * String = Nome da classe da entidade
	 * IDAO = DAO da entidade
	 */
	private Map<String, IDAO> daos;
	
	/**
	 * Mapa de regras de neg�cio que armazena uma lista de regras de cada entidade
	 * String do primeiro map = Nome da classe entidade
	 * String do segundo map = Nome da opera��o a ser validada
	 * List = Lista de Regras de neg�cio que ser�o executadas
	 */
	private Map<String, Map<String, List<IStrategy>>> rns;
	
	private Resultado resultado;
	
	public Fachada() {
		/* Instancia o Map de DAOs */
		daos = new HashMap<String, IDAO>();
		/* Instancia o Map de Regras de neg�cio */
		rns = new HashMap<String, Map<String, List<IStrategy>>>();
		
		/* Cria inst�ncias dos DAOs a serem utilizados */
		PassageiroDAO passageiroDAO = new PassageiroDAO();
		AeroportoDAO aeroportoDAO = new AeroportoDAO();
		TrajetoDAO trajetoDAO = new TrajetoDAO();
		ViagemDAO viagemDAO = new ViagemDAO();
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		CartaoDAO cartaoDAO = new CartaoDAO();
		PassagemDAO passagemDAO = new PassagemDAO();
		PedidoDAO pedidoDAO = new PedidoDAO();
		BagagemDAO bagagemDAO = new BagagemDAO();
		GraficoDAO graficoDAO = new GraficoDAO();
		
		/* Adiciona cada d�o no MAP, indexando pelo nome da classe */
		daos.put(Passageiro.class.getName(), passageiroDAO);
		daos.put(Aeroporto.class.getName(), aeroportoDAO);
		daos.put(Trajeto.class.getName(), trajetoDAO);
		daos.put(Viagem.class.getName(), viagemDAO);
		daos.put(Usuario.class.getName(), usuarioDAO);
		daos.put(Cartao.class.getName(), cartaoDAO);
		daos.put(Passagem.class.getName(), passagemDAO);
		daos.put(Pedido.class.getName(), pedidoDAO);
		daos.put(Bagagem.class.getName(), bagagemDAO);
		daos.put(Grafico.class.getName(), graficoDAO);
		
		/* Cria inst�ncias de regras de neg�cio que ser�o utilizadas */
		// Passageiro
		ValidadorSenhaIgual vrSenhaIgual = new ValidadorSenhaIgual();
		ValidadorDadosObrigatoriosUsuario vrDadosObrigatoriosUsuario = new ValidadorDadosObrigatoriosUsuario();
		ValidadorDadosObrigatoriosCartao vrDadosObrigatoriosCartao = new ValidadorDadosObrigatoriosCartao();
		ValidadorDadosObrigatoriosPassageiro vrDadosObrigatoriosPassageiro = new ValidadorDadosObrigatoriosPassageiro();
		ValidadorSenhaForte vrSenhaForte = new ValidadorSenhaForte();
		ValidadorEmailUnico vrEmailUnico = new ValidadorEmailUnico();
		// Aeroporto
		ValidadorDadosObrigatoriosAeroporto vrDadosObrigatoriosAeroporto =  new ValidadorDadosObrigatoriosAeroporto();
		ValidadorPais vrPais = new ValidadorPais();
		ValidadorEstado vrEstado = new ValidadorEstado();
		ValidadorCidade vrCidade = new ValidadorCidade();
		// Viagem
		ValidadorDadosObrigatoriosViagem vrDadosObrigatoriosViagem = new ValidadorDadosObrigatoriosViagem();
		ValidadorTrajetoIgual vrTrajetoIgual = new ValidadorTrajetoIgual();
		ValidadorDataViagem vrDataViagem = new ValidadorDataViagem();
		// Pedido
		ValidadorDadosObrigatoriosPedido vrDadosObrigatoriosPedido = new ValidadorDadosObrigatoriosPedido();
		ValidadorTipoPassagemPedido vrTipoPassagemPedido = new ValidadorTipoPassagemPedido();
		ValidadorPrecoTotalPedido vrPrecoTotalPedido = new ValidadorPrecoTotalPedido();
		ValidadorAssentosDisponiveisPedido vrAssentosDisponiveisPedido = new ValidadorAssentosDisponiveisPedido();
		// Grafico
		ValidadorCamposGrafico vrCamposGrafico = new ValidadorCamposGrafico();
		
		
		
		List<IStrategy> rnsSalvarPassageiro = new ArrayList<IStrategy>();
		rnsSalvarPassageiro.add(vrDadosObrigatoriosUsuario);
		rnsSalvarPassageiro.add(vrDadosObrigatoriosCartao);
		rnsSalvarPassageiro.add(vrDadosObrigatoriosPassageiro);
		rnsSalvarPassageiro.add(vrSenhaIgual);
		rnsSalvarPassageiro.add(vrSenhaForte);
		rnsSalvarPassageiro.add(vrEmailUnico);
		List<IStrategy> rnsAlterarPassageiro = new ArrayList<IStrategy>();
		rnsAlterarPassageiro.add(vrDadosObrigatoriosUsuario);
		rnsAlterarPassageiro.add(vrDadosObrigatoriosCartao);
		rnsAlterarPassageiro.add(vrDadosObrigatoriosPassageiro);
		rnsAlterarPassageiro.add(vrSenhaIgual);
		rnsAlterarPassageiro.add(vrSenhaForte);
		rnsAlterarPassageiro.add(vrEmailUnico);
		Map<String, List<IStrategy>> rnsPassageiro = new HashMap<String, List<IStrategy>>();
		rnsPassageiro.put("SALVAR", rnsSalvarPassageiro);
		rnsPassageiro.put("ALTERAR", rnsAlterarPassageiro);
		
		
		
		List<IStrategy> rnsSalvarAeroporto = new ArrayList<IStrategy>();
		rnsSalvarAeroporto.add(vrDadosObrigatoriosAeroporto);
		rnsSalvarAeroporto.add(vrPais);
		rnsSalvarAeroporto.add(vrEstado);
		rnsSalvarAeroporto.add(vrCidade);
		List<IStrategy> rnsAlterarAeroporto = new ArrayList<IStrategy>();
		rnsAlterarAeroporto.add(vrDadosObrigatoriosAeroporto);
		rnsAlterarAeroporto.add(vrPais);
		rnsAlterarAeroporto.add(vrEstado);
		rnsAlterarAeroporto.add(vrCidade);
		Map<String, List<IStrategy>> rnsAeroporto = new HashMap<String, List<IStrategy>>();
		rnsAeroporto.put("SALVAR", rnsSalvarAeroporto);
		rnsAeroporto.put("ALTERAR", rnsAlterarAeroporto);
		
		
		
		List<IStrategy> rnsSalvarViagem = new ArrayList<IStrategy>();
		rnsSalvarViagem.add(vrDadosObrigatoriosViagem);
		rnsSalvarViagem.add(vrTrajetoIgual);
		rnsSalvarViagem.add(vrDataViagem);
		List<IStrategy> rnsAlterarViagem = new ArrayList<IStrategy>();
		rnsAlterarViagem.add(vrDadosObrigatoriosViagem);
		rnsAlterarViagem.add(vrTrajetoIgual);
		rnsAlterarViagem.add(vrDataViagem);
		Map<String, List<IStrategy>> rnsViagem = new HashMap<String, List<IStrategy>>();
		rnsViagem.put("SALVAR", rnsSalvarViagem);
		rnsViagem.put("ALTERAR", rnsAlterarViagem);
		
		
		
		List<IStrategy> rnsSalvarPedido = new ArrayList<IStrategy>();
		rnsSalvarPedido.add(vrDadosObrigatoriosPedido);
		rnsSalvarPedido.add(vrTipoPassagemPedido);
		rnsSalvarPedido.add(vrPrecoTotalPedido);
		rnsSalvarPedido.add(vrAssentosDisponiveisPedido);
		Map<String, List<IStrategy>> rnsPedido = new HashMap<String, List<IStrategy>>();
		rnsPedido.put("SALVAR", rnsSalvarPedido);
		
		
		
		List<IStrategy> rnsConsultarGrafico = new ArrayList<IStrategy>();
		rnsConsultarGrafico.add(vrCamposGrafico);
		Map<String, List<IStrategy>> rnsGrafico = new HashMap<String, List<IStrategy>>();
		rnsGrafico.put("CONSULTAR", rnsConsultarGrafico);
		
		rns.put(Passageiro.class.getName(), rnsPassageiro);
		rns.put(Aeroporto.class.getName(), rnsAeroporto);
		rns.put(Viagem.class.getName(), rnsViagem);
		rns.put(Pedido.class.getName(), rnsPedido);
		rns.put(Grafico.class.getName(), rnsGrafico);
	}

	/**
	 * M�todo que salva uma entidade
	 * Primeiro executa as regras referentes � opera��o de salvar a entidade
	 * Depois executa o DAO espec�fico para salvar essa entidade
	 * @param entidade
	 * @return resultado: null = OK / !null = Erro ao realizar o registro
	 */
	@Override
	public Resultado salvar(EntidadeImpl entidade) {
		resultado = new Resultado();
		String nomeClasse = entidade.getClass().getName();
		
		String mensagem = executarRegras(entidade, "SALVAR");
		
		if(mensagem == null) {
			IDAO dao = daos.get(nomeClasse);
			try {
				dao.salvar(entidade);
				List<EntidadeImpl> entidades = new ArrayList<EntidadeImpl>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				resultado.setMensagem("N�o foi poss�vel realizar o registro!");
			}
		} else {
			resultado.setMensagem(mensagem);
		}
		return resultado;
	}

	/**
	 * M�todo que altera os dados de uma entidade
	 * Primeiro executa as regras referentes � altera��o entidade
	 * Depois executa o DAO espec�fico para alterar essa entidade
	 * @param entidade
	 * @return resultado: null = OK / !null = Erro ao realizar o registro
	 */
	@Override
	public Resultado alterar(EntidadeImpl entidade) {
		resultado = new Resultado();
		String nomeClasse = entidade.getClass().getName();
		
		String mensagem = executarRegras(entidade, "ALTERAR");
		
		if(mensagem == null) {
			IDAO dao = daos.get(nomeClasse);
			try {
				dao.alterar(entidade);
				List<EntidadeImpl> entidades = new ArrayList<EntidadeImpl>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				resultado.setMensagem("N�o foi poss�vel realizar o registro!");
			}
		} else {
			resultado.setMensagem(mensagem);
		}
		return resultado;
	}

	/**
	 * M�todo que excluir uma entidade
	 * Primeiro executa as regras referentes � exclus�o da entidade
	 * Depois executa o DAO espec�fico para excluir essa entidade
	 * @param entidade
	 * @return resultado: null = OK / !null = Erro ao realizar o registro
	 */
	@Override
	public Resultado excluir(EntidadeImpl entidade) {
		resultado = new Resultado();
		String nomeClasse = entidade.getClass().getName();
		
		String mensagem = executarRegras(entidade, "EXCLUIR");
		
		if(mensagem == null) {
			IDAO dao = daos.get(nomeClasse);
			try {
				dao.excluir(entidade);
				List<EntidadeImpl> entidades = new ArrayList<EntidadeImpl>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				resultado.setMensagem("N�o foi poss�vel realizar o registro!");
			}
		} else {
			resultado.setMensagem(mensagem);
		}
		return resultado;
	}

	/**
	 * M�todo que ativa uma entidade
	 * Primeiro executa as regras referentes � ativa��o da entidade
	 * Depois executa o DAO espec�fico para ativar essa entidade
	 * @param entidade
	 * @return resultado: null = OK / !null = Erro ao realizar o registro
	 */
	@Override
	public Resultado ativar(EntidadeImpl entidade) {
		resultado = new Resultado();
		String nomeClasse = entidade.getClass().getName();
		
		String mensagem = executarRegras(entidade, "ATIVAR");
		
		if(mensagem == null) {
			IDAO dao = daos.get(nomeClasse);
			try {
				dao.ativar(entidade);
				List<EntidadeImpl> entidades = new ArrayList<EntidadeImpl>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				resultado.setMensagem("N�o foi poss�vel realizar o registro!");
			}
		} else {
			resultado.setMensagem(mensagem);
		}
		return resultado;
	}

	/**
	 * M�todo que consulta uma entidade
	 * Primeiro executa as regras referentes � consulta da entidade
	 * Depois executa o DAO espec�fico para consultar essa entidade
	 * @param entidade
	 * @return resultado: null = OK / !null = Erro ao realizar o registro
	 */
	@Override
	public Resultado consultar(EntidadeImpl entidade) {
		resultado = new Resultado();
		String nomeClasse = entidade.getClass().getName();
		
		String mensagem = executarRegras(entidade, "CONSULTAR");
		
		if(mensagem == null) {
			IDAO dao = daos.get(nomeClasse);
			try {
				resultado.setEntidades(dao.consultar(entidade));
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				resultado.setMensagem("N�o foi poss�vel realizar o registro!");
			}
		} else {
			resultado.setMensagem(mensagem);
		}
		return resultado;
	}

	/**
	 * M�todo para visualizar uma entidade
	 * Salva um ArrayList de entidades em resultado
	 * @param entidade
	 * @return resultado com as entidades salvas
	 */
	@Override
	public Resultado visualizar(EntidadeImpl entidade) {
		resultado = new Resultado();
		resultado.setEntidades(new ArrayList<EntidadeImpl>(1));
		resultado.getEntidades().add(entidade);
		return resultado;
	}
	
	/**
	 * M�todo que executa as regras de neg�cio de uma opera��o espec�fica de uma entidade espec�fica
	 * @param entidade
	 * @param operacao
	 * @return
	 */
	private String executarRegras(EntidadeImpl entidade, String operacao) {
		String nomeClasse = entidade.getClass().getName();
		StringBuilder mensagem = new StringBuilder();
		
		/* Pega todas as regras da entidade */
		Map<String, List<IStrategy>> regrasOperacao = rns.get(nomeClasse);
		
		if(regrasOperacao != null) {	// Existem regras de neg�cio para essa classe?
			List<IStrategy> regras = regrasOperacao.get(operacao);
			
			if(regras != null) {	// Existem regras de neg�cio para essa opera��o dessa classe?
				for(IStrategy regra : regras) {
					String msg = regra.processar(entidade);	// Processa regra por regra
					
					if(msg != null) {
						mensagem.append(msg);
						mensagem.append("\n");
					}
				}
			}
		}
		if(mensagem.length() > 0)
			return mensagem.toString();
		else
			return null;
	}
	
}
