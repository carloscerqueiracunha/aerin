package com.aeroporto.core.impl.negocio;

import java.sql.SQLException;
import java.util.List;

import com.aeroporto.core.IStrategy;
import com.aeroporto.core.impl.dao.UsuarioDAO;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;

// Regra de negocio que valida os estados permitidos no sistema
public class ValidadorEmailUnico implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Passageiro) {
			Passageiro passageiro = (Passageiro) entidade;
			UsuarioDAO usuarioDAO = new UsuarioDAO();
			List<EntidadeImpl> usuarios = null;
			
			if(passageiro.getUsuario().getEmail() == null || passageiro.getUsuario().getEmail().trim().equals("")) {
				return "Campo e-mail � obrigat�rio";
			}
			try {
				usuarios = usuarioDAO.consultar(passageiro.getUsuario());
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				return "N�o foi poss�vel realizar o registro!";
			}
			
			if(usuarios == null || usuarios.isEmpty()) {
				return null;
			}
			else
				return "E-mail j� cadastrado no sistema";
		} else {
			return "Deve ser registrado um Aeroporto!";
		}
		
	}

}
