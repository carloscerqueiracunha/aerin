package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Pedido;

public class ValidadorDadosObrigatoriosPedido implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Pedido) {
			Pedido pedido = (Pedido) entidade;
			
			Long idCartao = pedido.getCartaoUtilizado().getId();
			String tipoPassagem = pedido.getTipoPassagem();
			
			if (idCartao == null) {
				return "� obrigat�rio selecionar um cart�o de pagamento";
			}
			if (tipoPassagem == null || tipoPassagem.trim().equals("")) {
				return "� obrigat�rio escolher um tipo de passagem";
			}
			
		} else {
			return "Deve ser registrado um Pedido!";
		}
		return null;
	}

}
