package com.aeroporto.core.impl.negocio;

import java.time.LocalDateTime;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Viagem;

// Regra de negocio que valida os dados obrigatorios de aeroporto
public class ValidadorDataViagem implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Viagem) {
			Viagem viagem = (Viagem) entidade;

			LocalDateTime dataHoraPartida = viagem.getDataHoraPartida();

			Boolean idaVolta = viagem.getIdaVolta();
			LocalDateTime dataHoraRetorno = viagem.getDataHoraRetorno();

			if (dataHoraPartida.isBefore(LocalDateTime.now())) {
				return "N�o � poss�vel cadastrar uma partida com data e hora inferior � de agora!";
			}

			if (idaVolta != null && idaVolta == true) {
				if (dataHoraRetorno != null) {
					if (dataHoraRetorno.isBefore(dataHoraPartida)) {
						return "A data de retorno n�o pode ser inferior � data de partida!";
					}
				}
			}

		} else {
			return "Deve ser registrado uma Viagem!";
		}
		return null;
	}

}
