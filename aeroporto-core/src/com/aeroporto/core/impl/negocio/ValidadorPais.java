package com.aeroporto.core.impl.negocio;

import java.sql.SQLException;
import java.util.List;

import com.aeroporto.core.IStrategy;
import com.aeroporto.core.impl.dao.PaisDAO;
import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.EntidadeImpl;

// Regra de negocio que valida os pa�ses permitidos no sistema
public class ValidadorPais implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Aeroporto) {
			Aeroporto aeroporto = (Aeroporto) entidade;
			PaisDAO paisDAO = new PaisDAO();
			List<EntidadeImpl> paises = null;
			if(aeroporto.getPais().getId() == null) {
				return "Campo Pais � obrigat�rio";
			}
			try {
				paises = paisDAO.consultar(aeroporto.getPais());
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				return "N�o foi poss�vel realizar o registro!";
			}
			
			if(paises == null || paises.isEmpty()) {
				return "Pa�s n�o permitido";
			}
		} else {
			return "Deve ser registrado um Aeroporto!";
		}
		return null;
	}

}
