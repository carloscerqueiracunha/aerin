package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Usuario;

public class ValidadorDadosObrigatoriosUsuario implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		Usuario usuario = null;
		if (entidade instanceof Usuario) {
			usuario = (Usuario) entidade;
		}
		if (entidade instanceof Passageiro) {
			Passageiro passageiro = (Passageiro) entidade;
			usuario = passageiro.getUsuario();
		}
		
		String email = usuario.getEmail();
		String senha = usuario.getSenha();
		
		if (email == null || email.trim().equals("")) {
			return "Campo e-mail obrigatório!";
		}
		if (senha == null || senha.trim().equals("")) {
			return "Campo senha obrigatório!";
		}
		return null;
	}

}
