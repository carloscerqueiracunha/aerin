package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;

public class ValidadorSenhaIgual implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Passageiro) {
			Passageiro passageiro = (Passageiro) entidade;
			if (passageiro.getUsuario().getSenha() != null && passageiro.getUsuario().getConfirmaSenha() != null) {
				if (!passageiro.getUsuario().getSenha().equals(passageiro.getUsuario().getConfirmaSenha())) {
					return "A senha e a confirmação de senha devem ser iguais!";
				}
			}
		} else
			return "Deve ser registrado um passageiro!";

		return null;
	}

}
