package com.aeroporto.core.impl.negocio;

import java.time.LocalDate;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Grafico;

// Regra de negocio que valida os dados obrigatorios de aeroporto
public class ValidadorCamposGrafico implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Grafico) {
			Grafico grafico = (Grafico) entidade;

			LocalDate dataInicio = grafico.getDataInicio();
			LocalDate dataFim = grafico.getDataFim();
			
			if(dataInicio == null) {
				return "Deve ser inserida uma data de inicio";
			}
			if(dataFim == null) {
				return "Deve ser inserida uma data de fim";
			}
			
			if(dataFim.isBefore(dataInicio) || dataFim.isEqual(dataInicio)) {
				return "A data de fim deve ser superior � data de inicio";
			}
			
		} else {
			return "Deve ser registrado uma Viagem!";
		}
		return null;
	}

}
