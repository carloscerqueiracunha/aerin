package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Pedido;

// Regra de negocio que valida os dados obrigatorios de cliente
public class ValidadorTipoPassagemPedido implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Pedido) {
			Pedido pedido = (Pedido) entidade;
			
			String tipoPassagem = pedido.getTipoPassagem();
			
			if (tipoPassagem != null && !tipoPassagem.trim().equals("")) {
				if(!tipoPassagem.equals("Economica") && !tipoPassagem.equals("Executiva") && !tipoPassagem.equals("Primeira")) {
					return "Deve ser selecionado um tipo de Passagem v�lido";
				}
			}
			
		} else {
			return "Deve ser registrado um Pedido!";
		}
		return null;
	}

}
