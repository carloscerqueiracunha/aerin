package com.aeroporto.core.impl.negocio;

import java.sql.SQLException;
import java.util.List;

import com.aeroporto.core.IStrategy;
import com.aeroporto.core.impl.dao.CidadeDAO;
import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.EntidadeImpl;

// Regra de negocio que valida os estados permitidos no sistema
public class ValidadorCidade implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Aeroporto) {
			Aeroporto aeroporto = (Aeroporto) entidade;
			CidadeDAO cidadeDAO = new CidadeDAO();
			List<EntidadeImpl> cidades = null;
			if(aeroporto.getCidade().getId() == null) {
				return "Campo Cidade � obrigat�rio";
			}
			try {
				cidades = cidadeDAO.consultar(aeroporto.getCidade());
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				return "N�o foi poss�vel realizar o registro!";
			}
			
			if(cidades == null || cidades.isEmpty()) {
				return "Estado n�o permitido";
			}
		} else {
			return "Deve ser registrado um Aeroporto!";
		}
		return null;
	}

}
