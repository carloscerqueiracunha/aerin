package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Viagem;

public class ValidadorTrajetoIgual implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Viagem) {
			Viagem viagem = (Viagem) entidade;
			if (viagem.getTrajeto().getOrigem() != null && viagem.getTrajeto().getDestino() != null) {
				if (viagem.getTrajeto().getOrigem().getId() == viagem.getTrajeto().getDestino().getId()) {
					return "A origem e o destino n�o podem ser iguais!";
				}
			}
		} else
			return "Deve ser registrado uma viagem!";

		return null;
	}

}
