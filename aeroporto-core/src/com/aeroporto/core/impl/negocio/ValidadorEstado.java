package com.aeroporto.core.impl.negocio;

import java.sql.SQLException;
import java.util.List;

import com.aeroporto.core.IStrategy;
import com.aeroporto.core.impl.dao.EstadoDAO;
import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.EntidadeImpl;

// Regra de negocio que valida os estados permitidos no sistema
public class ValidadorEstado implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Aeroporto) {
			Aeroporto aeroporto = (Aeroporto) entidade;
			EstadoDAO estadoDAO = new EstadoDAO();
			List<EntidadeImpl> estados = null;
			if(aeroporto.getEstado().getId() == null) {
				return "Campo Estado � obrigat�rio";
			}
			try {
				estados = estadoDAO.consultar(aeroporto.getEstado());
			} catch (SQLException sqlException) {
				sqlException.printStackTrace();
				return "N�o foi poss�vel realizar o registro!";
			}
			
			if(estados == null || estados.isEmpty()) {
				return "Estado n�o permitido";
			}
		} else {
			return "Deve ser registrado um Aeroporto!";
		}
		return null;
	}

}
