package com.aeroporto.core.impl.negocio;

import java.time.LocalDateTime;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Viagem;

// Regra de negocio que valida os dados obrigatorios de aeroporto
public class ValidadorDadosObrigatoriosViagem implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Viagem) {
			Viagem viagem = (Viagem) entidade;
			
			// Campos que sempre ser�o obrigat�rios:
			Long origem = viagem.getTrajeto().getOrigem().getId();
			Long destino = viagem.getTrajeto().getDestino().getId();
			LocalDateTime dataHoraPartida = viagem.getDataHoraPartida();
			
			// Campos que s� ser�o obrigat�rios em casos espec�ficos
			Boolean idaVolta = viagem.getIdaVolta();
			LocalDateTime dataHoraRetorno = viagem.getDataHoraRetorno();
			Integer qtdeAssentoEconomica = viagem.getQtdeAssentosEconomica();
			Integer qtdeAssentoExecutiva = viagem.getQtdeAssentosExecutiva();
			Integer qtdeAssentoPrimeira = viagem.getQtdeAssentosPrimeira();
			Double precoEconomica = viagem.getPrecoEconomica();
			Double precoExecutiva = viagem.getPrecoExecutiva();
			Double precoPrimeira = viagem.getPrecoPrimeira();
			
			// Valida��o dos campos que sempre ser�o obrigat�rios:
			if(origem == null) {
				return "� obrigat�rio selecionar um aeroporto de origem!";
			}
			
			if(destino == null) {
				return "� obrigat�rio selecionar um aeroporto de destino!";
			}
			
			if(dataHoraPartida == null) {
				return "Campo de data e hora de partida � obrigat�rio!";
			}
			
			// Valida��o dos campos que s� ser�o obrigat�rios em casos espec�ficos
			if(idaVolta != null && idaVolta == true) {
				if(dataHoraRetorno == null) {
					return "Campo de data e hora de retorno � obrigat�rio para viagens de ida e volta!";
				}
			}
			
			if(qtdeAssentoEconomica != null && qtdeAssentoEconomica > 0) {
				if(precoEconomica == null) {
					return "� obrigat�rio inserir o pre�o da classe econ�mica caso tenha inserido a quantidade de assentos na mesma!";
				}
			}
			
			if(qtdeAssentoExecutiva != null && qtdeAssentoExecutiva > 0) {
				if(precoExecutiva == null) {
					return "� obrigat�rio inserir o pre�o da classe executiva caso tenha inserido a quantidade de assentos na mesma!";
				}
			}
			
			if(qtdeAssentoPrimeira != null && qtdeAssentoPrimeira > 0) {
				if(precoPrimeira == null) {
					return "� obrigat�rio inserir o pre�o da primeira classe caso tenha inserido a quantidade de assentos na mesma!";
				}
			}
			
		} else {
			return "Deve ser registrado uma Viagem!";
		}
		return null;
	}

}
