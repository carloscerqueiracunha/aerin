package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.BandeiraCartao;
import com.aeroporto.dominio.Cartao;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;

public class ValidadorDadosObrigatoriosCartao implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		Cartao cartao = null;
		if (entidade instanceof Cartao) {
			cartao = (Cartao) entidade;
		}
		if (entidade instanceof Passageiro) {
			Passageiro passageiro = (Passageiro) entidade;
			cartao = passageiro.getCartoes().get(0);
		}
		
		String numero = cartao.getNumero();
		String codigoSeguranca = cartao.getCodigoSeguranca();
		BandeiraCartao bandeira = cartao.getBandeiraCartao();
		
		if(numero == null || numero.trim().equals("")) {
			return "Campo numero do cart�o obrigat�rio!";
		}
		if(codigoSeguranca == null || codigoSeguranca.trim().equals("")) {
			return "Campo c�digo de seguran�a obrigat�rio!";
		}
		if(bandeira == null) {
			return "Campo bandeira obrigat�rio!";
		}
		return null;
	}

}
