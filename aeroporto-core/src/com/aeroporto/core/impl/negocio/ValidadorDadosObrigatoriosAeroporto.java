package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.EntidadeImpl;

// Regra de negocio que valida os dados obrigatorios de aeroporto
public class ValidadorDadosObrigatoriosAeroporto implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Aeroporto) {
			Aeroporto aeroporto = (Aeroporto) entidade;
			
			String nome = aeroporto.getNome();
			String cepOuZip = aeroporto.getCepOuZip();
			String bairro = aeroporto.getBairro();
			String logradouro = aeroporto.getLogradouro();
			String numero = aeroporto.getNumero();
			
			if (nome == null || nome.trim().equals("")) {
				return "Campo nome obrigatório!";
			}
			if (cepOuZip == null || cepOuZip.trim().equals("")) {
				return "Campo CEP/ZIP obrigatório!";
			}
			if (bairro == null || bairro.trim().equals("")) {
				return "Campo Bairro obrigatório!";
			}
			if (logradouro == null || logradouro.trim().equals("")) {
				return "Campo Logradouro obrigatório!";
			}
			if (numero == null || numero.trim().equals("")) {
				return "Campo numero obrigatório!";
			}
		} else {
			return "Deve ser registrado um Aeroporto!";
		}
		return null;
	}

}
