package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Pedido;

public class ValidadorPrecoTotalPedido implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Pedido) {
			Pedido pedido = (Pedido) entidade;
			
			if("Economica".equals(pedido.getTipoPassagem())) {
				pedido.setPrecoTotal((pedido.getQuantidadePassagensAdulto() * pedido.getViagem().getPrecoEconomica()) +
						(pedido.getQuantidadePassagensCrianca() * pedido.getViagem().getPrecoEconomica() / 2));
			} else if("Executiva".equals(pedido.getTipoPassagem())) {
				pedido.setPrecoTotal((pedido.getQuantidadePassagensAdulto() * pedido.getViagem().getPrecoExecutiva()) +
						(pedido.getQuantidadePassagensCrianca() * pedido.getViagem().getPrecoExecutiva() / 2));
			} else if("Primeira".equals(pedido.getTipoPassagem())) {
				pedido.setPrecoTotal((pedido.getQuantidadePassagensAdulto() * pedido.getViagem().getPrecoPrimeira()) +
						(pedido.getQuantidadePassagensCrianca() * pedido.getViagem().getPrecoPrimeira() / 2));
			}
		} else {
			return "Deve ser registrado um Pedido!";
		}
		return null;
	}

}
