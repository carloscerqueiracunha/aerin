package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Pedido;

public class ValidadorAssentosDisponiveisPedido implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Pedido) {
			Pedido pedido = (Pedido) entidade;

			Integer qtdeTotalAssentos = 0;
			qtdeTotalAssentos = pedido.getQuantidadePassagensAdulto() + pedido.getQuantidadePassagensBebe()
					+ pedido.getQuantidadePassagensCrianca();

			if ("Economica".equals(pedido.getTipoPassagem())) {
				if (pedido.getViagem() != null && pedido.getViagem().getQtdeAssentosEconomica() < qtdeTotalAssentos) {
					return "Impossível efetuar a compra, certifique-se de que a viagem "
							+ "selecionada possui a quantidade necessária de assentos disponíveis";
				}
			} else if ("Executiva".equals(pedido.getTipoPassagem())) {
				if (pedido.getViagem() != null && pedido.getViagem().getQtdeAssentosExecutiva() < qtdeTotalAssentos) {
					return "Impossível efetuar a compra, certifique-se de que a viagem "
							+ "selecionada possui a quantidade necessária de assentos disponíveis";
				}
			} else if ("Primeira".equals(pedido.getTipoPassagem())) {
				if (pedido.getViagem() != null && pedido.getViagem().getQtdeAssentosPrimeira() < qtdeTotalAssentos) {
					return "Impossível efetuar a compra, certifique-se de que a viagem "
							+ "selecionada possui a quantidade necessária de assentos disponíveis";
				}
			}
		} else {
			return "Deve ser registrado um Pedido!";
		}
		return null;
	}

}
