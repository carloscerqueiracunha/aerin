package com.aeroporto.core.impl.negocio;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;

public class ValidadorSenhaForte implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Passageiro) {
			Passageiro passageiro = (Passageiro) entidade;

			if (passageiro.getUsuario().getSenha() != null && passageiro.getUsuario().getConfirmaSenha() != null) {
				if (passageiro.getUsuario().getSenha().length() < 8)
					return "A senha deve conter pelo menos 8 caracteres dentre esses caracteres deve ter pelo menos uma letra mai�scula, uma min�scula, um n�mero e um caractere especial";

				boolean temNumero = false;
				boolean temMaiuscula = false;
				boolean temMinuscula = false;
				boolean temSimbolo = false;
				for (char c : passageiro.getUsuario().getSenha().toCharArray()) {
					if (c >= '0' && c <= '9') {
						temNumero = true;
					} else if (c >= 'A' && c <= 'Z') {
						temMaiuscula = true;
					} else if (c >= 'a' && c <= 'z') {
						temMinuscula = true;
					} else {
						temSimbolo = true;
					}
				}
				if (!temNumero || !temMaiuscula || !temMinuscula || !temSimbolo) {
					return "A senha deve conter pelo menos 8 caracteres dentre esses caracteres deve ter pelo menos uma letra mai�scula, uma min�scula, um n�mero e um caractere especial";
				}
			}
		} else
			return "Deve ser registrado um passageiro!";
		return null;
	}

}
