package com.aeroporto.core.impl.negocio;

import java.time.LocalDate;

import com.aeroporto.core.IStrategy;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Sexo;
import com.aeroporto.dominio.TipoTelefone;

// Regra de negocio que valida os dados obrigatorios de cliente
public class ValidadorDadosObrigatoriosPassageiro implements IStrategy {

	@Override
	public String processar(EntidadeImpl entidade) {
		if (entidade instanceof Passageiro) {
			Passageiro passageiro = (Passageiro) entidade;
			
			String nome = passageiro.getNome();
			LocalDate dtNascimento = passageiro.getDtNascimento();
			Sexo sexo = passageiro.getSexo();
			String cpf = passageiro.getCpf();
			TipoTelefone tipoTelefone = passageiro.getTipoTelefone();
			String telefone = passageiro.getTelefone();
			
			if (nome == null || nome.trim().equals("")) {
				return "Campo nome obrigatório!";
			}
			if (dtNascimento == null) {
				return "Campo data de nascimento obrigatório!";
			}
			if (sexo == null) {
				return "Campo gênero obrigatório!";
			}
			if (cpf == null || cpf.trim().equals("")) {
				return "Campo CPF obrigatório!";
			}
			if (tipoTelefone == null) {
				return "Campo tipo de telefone obrigatório!";
			}
			if (telefone == null || telefone.trim().equals("")) {
				return "Campo número do telefone obrigatório!";
			}
		} else {
			return "Deve ser registrado um Cliente!";
		}
		return null;
	}

}
