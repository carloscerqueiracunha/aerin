package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Estado;

public class EstadoDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Estado estado = (Estado) entidade;
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM estado");
		
		if(estado.getId() != null)
			sql.append(" where id=" + estado.getId());

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());
			
			List<EntidadeImpl> estados = new ArrayList<EntidadeImpl>();
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Estado e = new Estado();
				e.setId(rs.getLong("id"));
				e.setNome(rs.getString("nome"));
				
				estados.add(e);
			}

			return estados;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
