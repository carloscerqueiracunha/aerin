package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Trajeto;
import com.aeroporto.dominio.Viagem;

public class ViagemDAO extends AbstractJdbcDAO{

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Viagem viagem = (Viagem)entidade;
		Trajeto trajeto = viagem.getTrajeto();
		
		try {
			connection.setAutoCommit(false);
			TrajetoDAO trajetoDAO = new TrajetoDAO();
			trajetoDAO.connection = connection;
			trajetoDAO.ctrlTransaction = false;
			trajetoDAO.salvar(trajeto);
			
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO viagem(dt_cadastro, dt_ultima_alteracao, ");
			sql.append("ativo, hash, ida_volta, dataHoraPartida, dataHoraRetorno, qtde_assentos_economica, qtde_assentos_executiva, qtde_assentos_primeira, ");
			sql.append("preco_economica, preco_executiva, preco_primeira, acessibilidade, trajeto) ");
			sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(viagem.getDtCadastro()));
			pst.setTimestamp(2, Timestamp.valueOf(viagem.getDtUltimaAlteracao()));
			pst.setBoolean(3, viagem.isAtivo());
			pst.setString(4, viagem.getHash().toString());
			pst.setBoolean(5, viagem.getIdaVolta());
			pst.setTimestamp(6, Timestamp.valueOf(viagem.getDataHoraPartida()));
			if(viagem.getDataHoraRetorno() == null) 
				pst.setNull(7, java.sql.Types.TIMESTAMP);
			else
				pst.setTimestamp(7, Timestamp.valueOf(viagem.getDataHoraRetorno()));
			pst.setInt(8, viagem.getQtdeAssentosEconomica());
			pst.setInt(9, viagem.getQtdeAssentosExecutiva());
			pst.setInt(10, viagem.getQtdeAssentosPrimeira());
			pst.setDouble(11, viagem.getPrecoEconomica());
			pst.setDouble(12, viagem.getPrecoExecutiva());
			pst.setDouble(13, viagem.getPrecoPrimeira());
			pst.setBoolean(14, viagem.getAcessibilidade());
			pst.setLong(15, viagem.getTrajeto().getId());
			pst.executeUpdate();
			
			connection.commit(); // commita
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Viagem viagem = (Viagem)entidade;
		Trajeto trajeto = viagem.getTrajeto();
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE viagem SET dt_ultima_alteracao=?, ");
			sql.append("ida_volta=?, dataHoraPartida=?, dataHoraRetorno=?, qtde_assentos_economica=?, qtde_assentos_executiva=?, qtde_assentos_primeira=?, ");
			sql.append("preco_economica=?, preco_executiva=?, preco_primeira=?, acessibilidade=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, viagem.getIdaVolta());
			pst.setTimestamp(3, Timestamp.valueOf(viagem.getDataHoraPartida()));
			if(viagem.getDataHoraRetorno() == null) 
				pst.setNull(4, java.sql.Types.TIMESTAMP);
			else
				pst.setTimestamp(4, Timestamp.valueOf(viagem.getDataHoraRetorno()));
			pst.setInt(5, viagem.getQtdeAssentosEconomica());
			pst.setInt(6, viagem.getQtdeAssentosExecutiva());
			pst.setInt(7, viagem.getQtdeAssentosPrimeira());
			pst.setDouble(8, viagem.getPrecoEconomica());
			pst.setDouble(9, viagem.getPrecoExecutiva());
			pst.setDouble(10, viagem.getPrecoPrimeira());
			pst.setBoolean(11, viagem.getAcessibilidade());
			pst.setLong(12, viagem.getId());
			pst.executeUpdate();
			
			TrajetoDAO trajetoDAO = new TrajetoDAO();
			trajetoDAO.connection = connection;
			trajetoDAO.ctrlTransaction = false;
			trajetoDAO.alterar(trajeto);
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void alterarTotalAssentosEconomica(EntidadeImpl entidade) throws SQLException{
		if (connection == null) {
			openConnection();
		}
		PreparedStatement pst = null;
		Viagem viagem = (Viagem) entidade;
		StringBuilder sql = new StringBuilder();

		try {
			connection.setAutoCommit(false);

			sql.append("UPDATE viagem SET dt_ultima_alteracao=?, qtde_assentos_economica = qtde_assentos_economica ? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setInt(2, viagem.getQtdeAssentosEconomica());
			pst.setLong(3, viagem.getId());

			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public void alterarTotalAssentosExecutiva(EntidadeImpl entidade) throws SQLException{
		if (connection == null) {
			openConnection();
		}
		PreparedStatement pst = null;
		Viagem viagem = (Viagem) entidade;
		StringBuilder sql = new StringBuilder();

		try {
			connection.setAutoCommit(false);

			sql.append("UPDATE viagem SET dt_ultima_alteracao=?, qtde_assentos_executiva = qtde_assentos_executiva ? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setInt(2, viagem.getQtdeAssentosExecutiva());
			pst.setLong(3, viagem.getId());

			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public void alterarTotalAssentosPrimeira(EntidadeImpl entidade) throws SQLException{
		if (connection == null) {
			openConnection();
		}
		PreparedStatement pst = null;
		Viagem viagem = (Viagem) entidade;
		StringBuilder sql = new StringBuilder();

		try {
			connection.setAutoCommit(false);

			sql.append("UPDATE viagem SET dt_ultima_alteracao=?, qtde_assentos_primeira = qtde_assentos_primeira ? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setInt(2, viagem.getQtdeAssentosPrimeira());
			pst.setLong(3, viagem.getId());

			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Viagem viagem = (Viagem)entidade;
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE viagem SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, false);
			pst.setLong(3, viagem.getId());
			pst.executeUpdate();
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Viagem viagem = (Viagem)entidade;
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE viagem SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, true);
			pst.setLong(3, viagem.getId());
			pst.executeUpdate();
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Viagem viagem = (Viagem) entidade;
		ArrayList<String> whr = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();

		if (viagem.getId() != null) {
			whr.add(" v.id = " + viagem.getId());
		}

		if (viagem.getIdaVolta() != null) {
			whr.add(" v.ida_volta = " + viagem.getIdaVolta());
		}

		if (viagem.getDataHoraPartida() != null) {
			whr.add(" v.dataHoraPartida = '" + viagem.getDataHoraPartida() + "'");
		}
		
		if (viagem.getDataHoraRetorno() != null) {
			whr.add(" v.dataHoraRetorno = '" + viagem.getDataHoraRetorno() + "'");
		}
		
		if (viagem.getQtdeAssentosEconomica() != null) {
			whr.add(" v.qtde_assentos_economica = '" + viagem.getQtdeAssentosEconomica() + "'");
		}
		
		if (viagem.getQtdeAssentosExecutiva() != null) {
			whr.add(" v.qtde_assentos_executiva = '" + viagem.getQtdeAssentosExecutiva() + "'");
		}
		
		if (viagem.getQtdeAssentosPrimeira() != null) {
			whr.add(" v.qtde_assentos_primeira = '" + viagem.getQtdeAssentosPrimeira() + "'");
		}
		
		if (viagem.getPrecoEconomica() != null) {
			whr.add(" v.preco_economica = '" + viagem.getPrecoEconomica() + "'");
		}
		
		if (viagem.getPrecoExecutiva() != null) {
			whr.add(" v.preco_executiva = '" + viagem.getPrecoExecutiva() + "'");
		}
		
		if (viagem.getPrecoPrimeira() != null) {
			whr.add(" v.preco_primeira = '" + viagem.getPrecoPrimeira() + "'");
		}
		
		if (viagem.getAcessibilidade() != null) {
			whr.add(" v.acessibilidade = " + viagem.getAcessibilidade());
		}
		
		if(viagem.getTrajeto().getId() != null) {
			whr.add(" v.trajeto = " + viagem.getTrajeto().getId());
		}
		

		sql.append("SELECT * FROM viagem v ");
		sql.append("left join trajeto t");
		sql.append(" on(v.trajeto = t.id) ");
		sql.append("left join aeroporto o");
		sql.append(" on(t.origem = o.id) ");
		sql.append("left join aeroporto d");
		sql.append(" on(t.destino = d.id) ");
		
		if (!whr.isEmpty()) {
			sql.append("where");
			for (String where : whr) {
				if(where.equals(whr.get(whr.size()-1)))
					sql.append(where);
				else
					sql.append(where + " and");
			}
		}

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeImpl> viagens = new ArrayList<EntidadeImpl>();
			while (rs.next()) {
				Viagem v = new Viagem();
				Trajeto t = new Trajeto();
				Aeroporto o = new Aeroporto();
				Aeroporto d = new Aeroporto();
				v.setId(rs.getLong("v.id"));
				v.setDtCadastro(rs.getTimestamp("v.dt_cadastro").toLocalDateTime());
				v.setDtUltimaAlteracao(rs.getTimestamp("v.dt_ultima_alteracao").toLocalDateTime());
				v.setAtivo(rs.getBoolean("v.ativo"));
				v.setHash(UUID.fromString(rs.getString("v.hash")));
				v.setIdaVolta(rs.getBoolean("v.ida_volta"));
				v.setDataHoraPartida(rs.getTimestamp("v.dataHoraPartida").toLocalDateTime());
				if(rs.getTimestamp("v.dataHoraRetorno") == null)
					v.setDataHoraRetorno(null);
				else
					v.setDataHoraRetorno(rs.getTimestamp("v.dataHoraRetorno").toLocalDateTime());
				v.setQtdeAssentosEconomica(rs.getInt("v.qtde_assentos_economica"));
				v.setQtdeAssentosExecutiva(rs.getInt("v.qtde_assentos_executiva"));
				v.setQtdeAssentosPrimeira(rs.getInt("v.qtde_assentos_primeira"));
				v.setPrecoEconomica(rs.getDouble("v.preco_economica"));
				v.setPrecoExecutiva(rs.getDouble("v.preco_executiva"));
				v.setPrecoPrimeira(rs.getDouble("v.preco_primeira"));
				v.setAcessibilidade(rs.getBoolean("v.acessibilidade"));
				t.setId(rs.getLong("v.trajeto"));
				
				o.setId(rs.getLong("o.id"));
				o.setNome(rs.getString("o.nome"));
				
				d.setId(rs.getLong("d.id"));
				d.setNome(rs.getString("d.nome"));
				
				t.setOrigem(o);
				t.setDestino(d);
				
				v.setTrajeto(t);

				viagens.add(v);
			}
			return viagens;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
