package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Passagem;
import com.aeroporto.dominio.Pedido;
import com.aeroporto.dominio.Trajeto;
import com.aeroporto.dominio.Viagem;

public class PedidoDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Pedido pedido = (Pedido) entidade;

		try {
			connection.setAutoCommit(false);

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO pedido(dt_cadastro, dt_ultima_alteracao, ");
			sql.append("ativo, hash, preco_total, tipo_passagem, quantidade_passagens_adulto, ");
			sql.append("quantidade_passagens_crianca, quantidade_passagens_bebe, viagem, passageiro, cartao_utilizado) ");
			sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			pst.setTimestamp(1, Timestamp.valueOf(pedido.getDtCadastro()));
			pst.setTimestamp(2, Timestamp.valueOf(pedido.getDtUltimaAlteracao()));
			pst.setBoolean(3, pedido.isAtivo());
			pst.setString(4, pedido.getHash().toString());
			pst.setDouble(5, pedido.getPrecoTotal());
			pst.setString(6, pedido.getTipoPassagem());
			pst.setInt(7, pedido.getQuantidadePassagensAdulto());
			pst.setInt(8, pedido.getQuantidadePassagensCrianca());
			pst.setInt(9, pedido.getQuantidadePassagensBebe());
			pst.setLong(10, pedido.getViagem().getId());
			pst.setLong(11, pedido.getPassageiro().getId());
			pst.setLong(12, pedido.getCartaoUtilizado().getId());

			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();
			Long id = null;
			if (rs.next())
				id = rs.getLong(1);
			pedido.setId(id);

			PassagemDAO passagemDAO;
			for(Passagem passagem : pedido.getPassagens()) {
				passagem.setPedido(pedido);
				passagemDAO = new PassagemDAO();
				passagemDAO.connection = connection;
				passagemDAO.ctrlTransaction = false;
				passagemDAO.salvar(passagem);
			}
			Viagem viagem = new Viagem();
			viagem.setId(pedido.getViagem().getId());
			Integer quantidadeAssentosTotal = pedido.getQuantidadePassagensAdulto() + pedido.getQuantidadePassagensBebe() + pedido.getQuantidadePassagensCrianca();
			quantidadeAssentosTotal *= -1;
			ViagemDAO viagemDAO = new ViagemDAO();
			viagemDAO.connection = connection;
			viagemDAO.ctrlTransaction = false;
			if("Economica".equals(pedido.getTipoPassagem())) {
				viagem.setQtdeAssentosEconomica(quantidadeAssentosTotal);
				viagemDAO.alterarTotalAssentosEconomica(viagem);
			} else if("Executiva".equals(pedido.getTipoPassagem())) {
				viagem.setQtdeAssentosExecutiva(quantidadeAssentosTotal);
				viagemDAO.alterarTotalAssentosExecutiva(viagem);
			} else if("Primeira".equals(pedido.getTipoPassagem())) {
				viagem.setQtdeAssentosPrimeira(quantidadeAssentosTotal);
				viagemDAO.alterarTotalAssentosPrimeira(viagem);
			}
			
			connection.commit(); // commita
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Pedido pedido = (Pedido) entidade;

		try {
			connection.setAutoCommit(false);

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE pedido SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, false);
			pst.setLong(3, pedido.getId());
			pst.executeUpdate();

			PassagemDAO passagemDAO;
			for(Passagem passagem : pedido.getPassagens()) {
				passagem.setPedido(pedido);
				passagemDAO = new PassagemDAO();
				passagemDAO.connection = connection;
				passagemDAO.ctrlTransaction = false;
				passagemDAO.excluir(passagem);
			}
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Pedido pedido = (Pedido) entidade;

		try {
			connection.setAutoCommit(false);

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE pedido SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, true);
			pst.setLong(3, pedido.getId());
			pst.executeUpdate();

			PassagemDAO passagemDAO;
			for(Passagem passagem : pedido.getPassagens()) {
				passagem.setPedido(pedido);
				passagemDAO = new PassagemDAO();
				passagemDAO.connection = connection;
				passagemDAO.ctrlTransaction = false;
				passagemDAO.ativar(passagem);
			}
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Pedido pedido = (Pedido) entidade;
		ArrayList<String> whr = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();
		PassagemDAO passagemDAO;

		if (pedido.getId() != null) {
			whr.add(" id = " + pedido.getId());
		}

		if (pedido.getPassageiro() != null && pedido.getPassageiro().getId() != null) {
			whr.add(" passageiro = " + pedido.getPassageiro().getId());
		}

		if (pedido.getViagem() != null && pedido.getViagem().getId() != null) {
			whr.add(" viagem = " + pedido.getViagem().getId());
		}

		if (pedido.getTipoPassagem() == null || pedido.getTipoPassagem().equals("")) {
			pedido.setTipoPassagem("");
		} else {
			whr.add(" tipo_passagem like " + "%'" + pedido.getTipoPassagem() + "'%");
		}

		sql.append("SELECT * FROM pedido pe ");
		sql.append("left join viagem vi ");
		sql.append("on(pe.viagem = vi.id) ");
		sql.append("left join passageiro pa ");
		sql.append("on(pe.passageiro = pa.id) ");
		sql.append("left join trajeto t ");
		sql.append("on(vi.trajeto = t.id) ");
		sql.append("left join aeroporto o ");
		sql.append("on(t.origem = o.id) ");
		sql.append("left join aeroporto d ");
		sql.append("on(t.destino = d.id) ");

		if (!whr.isEmpty()) {
			sql.append("where");
			for (String where : whr) {
				if (where.equals(whr.get(whr.size() - 1)))
					sql.append(where);
				else
					sql.append(where + " and");
			}
		}

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());

			ResultSet rs = pst.executeQuery();
			List<EntidadeImpl> pedidos = new ArrayList<EntidadeImpl>();
			while (rs.next()) {
				Pedido pe = new Pedido();
				Viagem v = new Viagem();
				Passageiro passa =  new Passageiro();
				Aeroporto o = new Aeroporto();
				Aeroporto d = new Aeroporto();
				Trajeto t = new Trajeto();
				pe.setId(rs.getLong("pe.id"));
				pe.setDtCadastro(rs.getTimestamp("pe.dt_cadastro").toLocalDateTime());
				pe.setDtUltimaAlteracao(rs.getTimestamp("pe.dt_ultima_alteracao").toLocalDateTime());
				pe.setAtivo(rs.getBoolean("pe.ativo"));
				pe.setHash(UUID.fromString(rs.getString("pe.hash")));
				pe.setPrecoTotal(rs.getDouble("pe.preco_total"));
				pe.setQuantidadePassagensAdulto(rs.getInt("quantidade_passagens_adulto"));
				pe.setQuantidadePassagensCrianca(rs.getInt("quantidade_passagens_crianca"));
				pe.setQuantidadePassagensBebe(rs.getInt("quantidade_passagens_bebe"));
				pe.setTipoPassagem(rs.getString("tipo_passagem"));
				
				passa.setNome(rs.getString("pa.nome"));
				
				v.setId(rs.getLong("vi.id"));
				passa.setId(rs.getLong("pa.id"));
				v.setDataHoraPartida(rs.getTimestamp("vi.dataHoraPartida").toLocalDateTime());
				v.setDataHoraRetorno(rs.getTimestamp("vi.dataHoraRetorno").toLocalDateTime());
				
				o.setId(rs.getLong("o.id"));
				o.setNome(rs.getString("o.nome"));
				
				d.setId(rs.getLong("d.id"));
				d.setNome(rs.getString("d.nome"));
				
				t.setOrigem(o);
				t.setDestino(d);
				v.setTrajeto(t);

				pe.setViagem(v);
				pe.setPassageiro(passa);
				
				// Consultar as passagens desse pedido
				Passagem passagemPedido = new Passagem();
				Passagem pas = new Passagem();
				passagemPedido.setPedido(pe);
				passagemDAO = new PassagemDAO();
				passagemDAO.ctrlTransaction = false;
				passagemDAO.connection = connection;
				ArrayList<Passagem> passagens = new ArrayList<Passagem>();
				try {
					for (EntidadeImpl passagem : passagemDAO.consultar(passagemPedido)) {
						pas = (Passagem) passagem;
						passagens.add(pas);
					}
				} catch (Exception ex) {
					// Pra n�o pegar mais cart�es que o passageiro possui
				}
				pe.setPassagens(passagens);
				
				pedidos.add(pe);
			}
			return pedidos;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
