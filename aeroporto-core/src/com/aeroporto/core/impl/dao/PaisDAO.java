package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Pais;

public class PaisDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Pais pais = (Pais) entidade;
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM pais");
		
		if(pais.getId() != null)
			sql.append(" where id=" + pais.getId());

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());
			
			List<EntidadeImpl> paises = new ArrayList<EntidadeImpl>();
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Pais p = new Pais();
				p.setId(rs.getLong("id"));
				p.setNome(rs.getString("nome"));
				
				paises.add(p);
			}

			return paises;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
