package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.aeroporto.dominio.BandeiraCartao;
import com.aeroporto.dominio.Cartao;
import com.aeroporto.dominio.EntidadeImpl;

public class CartaoDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		if (connection == null) {
			openConnection();
		}
		PreparedStatement pst = null;
		Cartao cartao = (Cartao) entidade;
		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO cartao(dt_cadastro, dt_ultima_alteracao, ");
		sql.append("ativo, hash, numero, codigo_seguranca, bandeira_cartao, passageiro)");
		sql.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		try {
			connection.setAutoCommit(false);

			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			pst.setTimestamp(1, Timestamp.valueOf(cartao.getDtCadastro()));
			pst.setTimestamp(2, Timestamp.valueOf(cartao.getDtUltimaAlteracao()));
			pst.setBoolean(3, cartao.isAtivo());
			pst.setString(4, cartao.getHash().toString());
			pst.setString(5, cartao.getNumero());
			pst.setString(6, cartao.getCodigoSeguranca());
			pst.setLong(7, cartao.getBandeiraCartao().getId());
			pst.setLong(8, cartao.getPassageiro().getId());
			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();
			Long idCar = null;
			if (rs.next())
				idCar = rs.getLong(1);
			cartao.setId(idCar);

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		if(connection == null){
			openConnection();
		}
		PreparedStatement pst=null;
		Cartao cartao = (Cartao)entidade;
		StringBuilder sql = new StringBuilder();
		
		try {
			connection.setAutoCommit(false);
			
			sql.append("UPDATE cartao SET dt_ultima_alteracao=?, numero=?, codigo_seguranca=?, bandeira_cartao=? ");
			sql.append("WHERE id=?");
			
			pst = connection.prepareStatement(sql.toString());
			
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setString(2, cartao.getNumero());
			pst.setString(3, cartao.getCodigoSeguranca());
			pst.setLong(4, cartao.getBandeiraCartao().getId());
			pst.setLong(5, cartao.getId());
			
			pst.executeUpdate();
			
			connection.commit();					
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		if(connection == null){
			openConnection();
		}
		PreparedStatement pst=null;
		Cartao cartao = (Cartao)entidade;
		StringBuilder sql = new StringBuilder();
		
		try {
			connection.setAutoCommit(false);
			
			sql.append("UPDATE cartao SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
			
			pst = connection.prepareStatement(sql.toString());
			
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, false);
			pst.setLong(3, cartao.getId());
			
			pst.executeUpdate();
			
			connection.commit();					
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		if(connection == null){
			openConnection();
		}
		PreparedStatement pst=null;
		Cartao cartao = (Cartao)entidade;
		StringBuilder sql = new StringBuilder();
		
		try {
			connection.setAutoCommit(false);
			
			sql.append("UPDATE cartao SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
			
			pst = connection.prepareStatement(sql.toString());
			
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, true);
			pst.setLong(3, cartao.getId());
			
			pst.executeUpdate();		
			
			connection.commit();					
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Cartao cartao = (Cartao) entidade;
		ArrayList<String> whr = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();

		if (cartao.getPassageiro().getId() != null) {
			whr.add(" passageiro = " + cartao.getPassageiro().getId());
		}

		sql.append("SELECT * FROM cartao ");
		sql.append("left join bandeira_cartao");
		sql.append(" on(cartao.bandeira_cartao = bandeira_cartao.id) ");

		if (!whr.isEmpty()) {
			sql.append("where");
			for (String where : whr) {
				if (where.equals(whr.get(whr.size() - 1)))
					sql.append(where);
				else
					sql.append(where + " and");
			}
		}

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());
			List<EntidadeImpl> cartoes = new ArrayList<EntidadeImpl>();
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				BandeiraCartao b = new BandeiraCartao();
				Cartao c = new Cartao();
				c.setPassageiro(cartao.getPassageiro());
				c.setId(rs.getLong("id"));
				c.setDtCadastro(rs.getTimestamp("dt_cadastro").toLocalDateTime());
				c.setDtUltimaAlteracao(rs.getTimestamp("dt_ultima_alteracao").toLocalDateTime());
				c.setAtivo(rs.getBoolean("ativo"));
				c.setHash(UUID.fromString(rs.getString("hash")));
				c.setNumero(rs.getString("numero"));
				c.setCodigoSeguranca(rs.getString("codigo_seguranca"));
				b.setId(rs.getLong("bandeira_cartao"));
				b.setNome(rs.getString("nome"));
				c.setBandeiraCartao(b);
				
				cartoes.add(c);
			}

			return cartoes;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
