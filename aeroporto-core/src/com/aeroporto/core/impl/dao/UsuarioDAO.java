package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.aeroporto.dominio.Cartao;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Sexo;
import com.aeroporto.dominio.TipoTelefone;
import com.aeroporto.dominio.Usuario;

public class UsuarioDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		if (connection == null) {
			openConnection();
		}
		PreparedStatement pst = null;
		Usuario usuario = (Usuario) entidade;
		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO usuario(dt_cadastro, dt_ultima_alteracao, ");
		sql.append("ativo, hash, email, senha, isAdmin) ");
		sql.append(" VALUES (?, ?, ?, ?, ?, ?, ?)");
		try {
			connection.setAutoCommit(false);

			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			pst.setTimestamp(1, Timestamp.valueOf(usuario.getDtCadastro()));
			pst.setTimestamp(2, Timestamp.valueOf(usuario.getDtUltimaAlteracao()));
			pst.setBoolean(3, usuario.isAtivo());
			pst.setString(4, usuario.getHash().toString());
			pst.setString(5, usuario.getEmail());
			pst.setString(6, usuario.getSenha());
			pst.setBoolean(7, false);

			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();
			Long idUser = null;
			if (rs.next())
				idUser = rs.getLong(1);
			usuario.setId(idUser);

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		if (connection == null) {
			openConnection();
		}
		PreparedStatement pst = null;
		Usuario usuario = (Usuario) entidade;
		StringBuilder sql = new StringBuilder();

		try {
			connection.setAutoCommit(false);

			sql.append("UPDATE usuario SET dt_ultima_alteracao=?, email=?, senha=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setString(2, usuario.getEmail());
			pst.setString(3, usuario.getSenha());
			pst.setLong(4, usuario.getId());

			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		if (connection == null) {
			openConnection();
		}
		PreparedStatement pst = null;
		Usuario usuario = (Usuario) entidade;
		StringBuilder sql = new StringBuilder();

		try {
			connection.setAutoCommit(false);

			sql.append("UPDATE usuario SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, false);
			pst.setLong(3, usuario.getId());

			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		if (connection == null) {
			openConnection();
		}
		PreparedStatement pst = null;
		Usuario usuario = (Usuario) entidade;
		StringBuilder sql = new StringBuilder();

		try {
			connection.setAutoCommit(false);

			sql.append("UPDATE usuario SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, true);
			pst.setLong(3, usuario.getId());

			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Usuario usuario = (Usuario) entidade;
		CartaoDAO cartaoDAO;
		ArrayList<String> whr = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();

		if (usuario.getId() != null) {
			whr.add(" id = " + usuario.getId());
		}

		if (usuario.getEmail() == null || usuario.getEmail().equals("")) {
			usuario.setEmail("");
		} else {
			whr.add(" email = " + "'" + usuario.getEmail() + "'");
		}

		if (usuario.getSenha() == null || usuario.getSenha().equals("")) {
			usuario.setSenha("");
		} else {
			whr.add(" senha = " + "'" + usuario.getSenha() + "'");
		}

		sql.append("SELECT * FROM usuario u ");
		sql.append("left join passageiro p");
		sql.append(" on(u.id = p.usuario) ");

		if (!whr.isEmpty()) {
			sql.append("where");
			for (String where : whr) {
				if (where.equals(whr.get(whr.size() - 1)))
					sql.append(where);
				else
					sql.append(where + " and");
			}
		}

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());

			ResultSet rs = pst.executeQuery();
			List<EntidadeImpl> usuarios = new ArrayList<EntidadeImpl>();
			while (rs.next()) {
				Passageiro p = new Passageiro();
				Cartao c = new Cartao();
				Usuario u = new Usuario();
				u.setId(rs.getLong("u.id"));
				u.setDtCadastro(rs.getTimestamp("u.dt_cadastro").toLocalDateTime());
				u.setDtUltimaAlteracao(rs.getTimestamp("u.dt_ultima_alteracao").toLocalDateTime());
				u.setAtivo(rs.getBoolean("u.ativo"));
				u.setHash(UUID.fromString(rs.getString("u.hash")));
				u.setEmail(rs.getString("u.email"));
				u.setSenha(rs.getString("u.senha"));
				u.setAdmin(rs.getBoolean("u.isAdmin"));
				if (!u.getAdmin()) {
					p.setId(rs.getLong("p.id"));
					p.setDtCadastro(rs.getTimestamp("p.dt_cadastro").toLocalDateTime());
					p.setDtUltimaAlteracao(rs.getTimestamp("p.dt_ultima_alteracao").toLocalDateTime());
					p.setAtivo(rs.getBoolean("p.ativo"));
					p.setHash(UUID.fromString(rs.getString("p.hash")));
					p.setNome(rs.getString("p.nome"));
					p.setDtNascimento(rs.getDate("p.dt_nascimento").toLocalDate());
					Sexo sexo = Enum.valueOf(Sexo.class, rs.getString("p.sexo").toUpperCase());
					p.setSexo(sexo);
					p.setCpf(rs.getString("p.cpf"));
					TipoTelefone tipoTelefone = Enum.valueOf(TipoTelefone.class,
							rs.getString("tipo_telefone").toUpperCase());
					p.setTipoTelefone(tipoTelefone);
					p.setTelefone(rs.getString("p.telefone"));

					// Consultar os cartoes desse passageiro
					c.setPassageiro(p);
					cartaoDAO = new CartaoDAO();
					Cartao ct = new Cartao();
					ArrayList<Cartao> carts = new ArrayList<Cartao>();
					try {
						for (EntidadeImpl cartao : cartaoDAO.consultar(c)) {
							ct = (Cartao) cartao;
							carts.add(ct);
						}
					} catch (Exception ex) {
						// Pra n�o pegar mais cart�es que o passageiro possui
					}
					p.setCartoes(carts);

					u.setPassageiro(p);
				}
				usuarios.add(u);
			}
			return usuarios;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
