package com.aeroporto.core.impl.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.aeroporto.dominio.Cartao;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Sexo;
import com.aeroporto.dominio.TipoTelefone;
import com.aeroporto.dominio.Usuario;

public class PassageiroDAO extends AbstractJdbcDAO{

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Passageiro passageiro = (Passageiro)entidade;
		Cartao cartao = passageiro.getCartoes().get(0);
		Usuario usuario = passageiro.getUsuario();
		
		try {
			connection.setAutoCommit(false);
			UsuarioDAO usuarioDAO = new UsuarioDAO();
			usuarioDAO.connection = connection;
			usuarioDAO.ctrlTransaction = false;
			usuarioDAO.salvar(usuario);
			
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO passageiro(dt_cadastro, dt_ultima_alteracao, ");
			sql.append("ativo, hash, nome, dt_nascimento, sexo, cpf, tipo_telefone, ");
			sql.append("telefone, usuario) ");
			sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			
			pst.setTimestamp(1, Timestamp.valueOf(passageiro.getDtCadastro()));
			pst.setTimestamp(2, Timestamp.valueOf(passageiro.getDtUltimaAlteracao()));
			pst.setBoolean(3, passageiro.isAtivo());
			pst.setString(4, passageiro.getHash().toString());
			pst.setString(5, passageiro.getNome());
			pst.setDate(6, Date.valueOf(passageiro.getDtNascimento()));
			pst.setString(7, passageiro.getSexo().getDescricao());
			pst.setString(8, passageiro.getCpf());
			pst.setString(9, passageiro.getTipoTelefone().getDescricao());
			pst.setString(10, passageiro.getTelefone());
			pst.setLong(11, passageiro.getUsuario().getId());
			
			pst.executeUpdate();
			
			ResultSet rs = pst.getGeneratedKeys();
			Long id = null;
			if (rs.next())
				id = rs.getLong(1);
			passageiro.setId(id);
			
			cartao.setPassageiro(passageiro);
			
			CartaoDAO cartaoDAO = new CartaoDAO();
			cartaoDAO.connection = connection;
			cartaoDAO.ctrlTransaction = false;
			cartaoDAO.salvar(cartao);

			connection.commit(); // commita
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Passageiro passageiro = (Passageiro)entidade;
		Cartao cartao = passageiro.getCartoes().get(0);
		Usuario usuario = passageiro.getUsuario();
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE passageiro SET dt_ultima_alteracao=?, ");
			sql.append("nome=?, dt_nascimento=?, sexo=?, cpf=?, tipo_telefone=?, ");
			sql.append("telefone=? ");
			sql.append("WHERE id=?");
			
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setString(2, passageiro.getNome());
			pst.setDate(3, Date.valueOf(passageiro.getDtNascimento()));
			pst.setString(4, passageiro.getSexo().getDescricao());
			pst.setString(5, passageiro.getCpf());
			pst.setString(6, passageiro.getTipoTelefone().getDescricao());
			pst.setString(7, passageiro.getTelefone());
			pst.setLong(8, passageiro.getId());
			pst.executeUpdate();
			
			UsuarioDAO usuarioDAO = new UsuarioDAO();
			usuarioDAO.connection = connection;
			usuarioDAO.ctrlTransaction = false;
			usuarioDAO.alterar(usuario);
			
			cartao.setPassageiro(passageiro);
			
			CartaoDAO cartaoDAO = new CartaoDAO();
			cartaoDAO.connection = connection;
			cartaoDAO.ctrlTransaction = false;
			cartaoDAO.alterar(cartao);
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Passageiro passageiro = (Passageiro)entidade;
		Cartao cartao = passageiro.getCartoes().get(0);
		Usuario usuario = passageiro.getUsuario();
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE passageiro SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, false);
			pst.setLong(3, passageiro.getId());
			pst.executeUpdate();
			
			UsuarioDAO usuarioDAO = new UsuarioDAO();
			usuarioDAO.connection = connection;
			usuarioDAO.ctrlTransaction = false;
			usuarioDAO.excluir(usuario);
			
			cartao.setPassageiro(passageiro);
			
			CartaoDAO cartaoDAO = new CartaoDAO();
			cartaoDAO.connection = connection;
			cartaoDAO.ctrlTransaction = false;
			cartaoDAO.excluir(cartao);
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Passageiro passageiro = (Passageiro)entidade;
		Cartao cartao = passageiro.getCartoes().get(0);
		Usuario usuario = passageiro.getUsuario();
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE passageiro SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, true);
			pst.setLong(3, passageiro.getId());
			pst.executeUpdate();
			
			UsuarioDAO usuarioDAO = new UsuarioDAO();
			usuarioDAO.connection = connection;
			usuarioDAO.ctrlTransaction = false;
			usuarioDAO.excluir(usuario);
			
			cartao.setPassageiro(passageiro);
			
			CartaoDAO cartaoDAO = new CartaoDAO();
			cartaoDAO.connection = connection;
			cartaoDAO.ctrlTransaction = false;
			cartaoDAO.excluir(cartao);
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Passageiro passageiro = (Passageiro) entidade;
		ArrayList<String> whr = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();
		CartaoDAO cartaoDAO;

		if (passageiro.getId() != null) {
			whr.add(" id = " + passageiro.getId());
		}

		if (passageiro.getUsuario().getEmail() == null || passageiro.getUsuario().getEmail().equals("")) {
			passageiro.getUsuario().setEmail("");
		} else {
			whr.add(" email like " + "'%" + passageiro.getUsuario().getEmail() + "%'");
		}

		if (passageiro.getNome() == null || passageiro.getNome().equals("")) {
			passageiro.setNome("");
		} else {
			whr.add(" nome like " + "'%" + passageiro.getNome() + "%'");
		}

		if (passageiro.getDtNascimento() != null) {
			whr.add(" dt_nascimento = '" + passageiro.getDtNascimento() + "'");
		}
		
		if (passageiro.getSexo() != null) {
			whr.add(" sexo = '" + passageiro.getSexo().getDescricao() + "'");
		}
		
		if (passageiro.getCpf() == null || passageiro.getCpf().equals("")) {
			passageiro.setCpf("");
		} else {
			whr.add(" cpf = '" + passageiro.getCpf() + "'");
		}

		if (passageiro.getTelefone() != null && !passageiro.getTelefone().equals("")) {
			whr.add(" telefone = '" + passageiro.getTelefone() + "'");
		}

		if (passageiro.getTipoTelefone() != null) {
			whr.add(" tipo_telefone = '" + passageiro.getTipoTelefone().getDescricao() + "'");
		}

		sql.append("SELECT * FROM passageiro p ");
		sql.append("left join usuario u");
		sql.append(" on(p.usuario = u.id) ");
		
		if (!whr.isEmpty()) {
			sql.append("where");
			for (String where : whr) {
				if(where.equals(whr.get(whr.size()-1)))
					sql.append(where);
				else
					sql.append(where + " and");
			}
		}

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeImpl> passageiros = new ArrayList<EntidadeImpl>();
			while (rs.next()) {
				Passageiro p = new Passageiro();
				Cartao c = new Cartao();
				Usuario u = new Usuario();
				p.setId(rs.getLong("p.id"));
				p.setDtCadastro(rs.getTimestamp("p.dt_cadastro").toLocalDateTime());
				p.setDtUltimaAlteracao(rs.getTimestamp("p.dt_ultima_alteracao").toLocalDateTime());
				p.setAtivo(rs.getBoolean("p.ativo"));
				p.setHash(UUID.fromString(rs.getString("p.hash")));
				p.setNome(rs.getString("p.nome"));
				p.setDtNascimento(rs.getDate("p.dt_nascimento").toLocalDate());
				Sexo sexo = Enum.valueOf(Sexo.class, rs.getString("p.sexo").toUpperCase());
				p.setSexo(sexo);
				p.setCpf(rs.getString("p.cpf"));
				TipoTelefone tipoTelefone = Enum.valueOf(TipoTelefone.class, rs.getString("tipo_telefone").toUpperCase());
				p.setTipoTelefone(tipoTelefone);
				p.setTelefone(rs.getString("p.telefone"));
				u.setId(rs.getLong("u.id"));
				u.setDtCadastro(rs.getTimestamp("u.dt_cadastro").toLocalDateTime());
				u.setDtUltimaAlteracao(rs.getTimestamp("u.dt_ultima_alteracao").toLocalDateTime());
				u.setAtivo(rs.getBoolean("u.ativo"));
				u.setHash(UUID.fromString(rs.getString("u.hash")));
				u.setEmail(rs.getString("u.email"));
				u.setSenha(rs.getString("u.senha"));
				u.setAdmin(rs.getBoolean("u.isAdmin"));
				p.setUsuario(u);
				// Consultar os cartoes desse passageiro
				c.setPassageiro(p);
				cartaoDAO = new CartaoDAO();
				Cartao ct = new Cartao();
				ArrayList<Cartao> carts = new ArrayList<Cartao>();
				try {
					for(EntidadeImpl cartao : cartaoDAO.consultar(c)) {
						ct = (Cartao)cartao;
						carts.add(ct);
					}
				} catch (Exception ex) {
					// Pra n�o pegar mais cart�es que o passageiro possui
				}
				p.setCartoes(carts);

				passageiros.add(p);
			}
			return passageiros;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
