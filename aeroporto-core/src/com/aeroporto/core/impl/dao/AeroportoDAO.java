package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.Cidade;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Estado;
import com.aeroporto.dominio.Pais;

public class AeroportoDAO extends AbstractJdbcDAO{

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Aeroporto aeroporto = (Aeroporto)entidade;
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO aeroporto(dt_cadastro, dt_ultima_alteracao, ");
			sql.append("ativo, hash, cep_zip, bairro, logradouro, numero, nome, pais, ");
			sql.append("estado, cidade) ");
			sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			pst = connection.prepareStatement(sql.toString());
			
			pst.setTimestamp(1, Timestamp.valueOf(aeroporto.getDtCadastro()));
			pst.setTimestamp(2, Timestamp.valueOf(aeroporto.getDtUltimaAlteracao()));
			pst.setBoolean(3, aeroporto.isAtivo());
			pst.setString(4, aeroporto.getHash().toString());
			pst.setString(5, aeroporto.getCepOuZip());
			pst.setString(6, aeroporto.getBairro());
			pst.setString(7, aeroporto.getLogradouro());
			pst.setString(8, aeroporto.getNumero());
			pst.setString(9, aeroporto.getNome());
			pst.setLong(10, aeroporto.getPais().getId());
			pst.setLong(11, aeroporto.getEstado().getId());
			pst.setLong(12, aeroporto.getCidade().getId());
			
			pst.executeUpdate();

			connection.commit(); // commita
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Aeroporto aeroporto = (Aeroporto)entidade;
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE aeroporto SET dt_ultima_alteracao=?, ");
			sql.append("cep_zip=?, bairro=?, logradouro=?, numero=?, nome=?, pais=?, ");
			sql.append("estado=?, cidade=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setString(2, aeroporto.getCepOuZip());
			pst.setString(3, aeroporto.getBairro());
			pst.setString(4, aeroporto.getLogradouro());
			pst.setString(5, aeroporto.getNumero());
			pst.setString(6, aeroporto.getNome());
			pst.setLong(7, aeroporto.getPais().getId());
			pst.setLong(8, aeroporto.getEstado().getId());
			pst.setLong(9, aeroporto.getCidade().getId());
			pst.setLong(10, aeroporto.getId());
			pst.executeUpdate();
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Aeroporto aeroporto = (Aeroporto)entidade;
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE aeroporto SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, false);
			pst.setLong(3, aeroporto.getId());
			pst.executeUpdate();
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Aeroporto aeroporto = (Aeroporto)entidade;
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE aeroporto SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, true);
			pst.setLong(3, aeroporto.getId());
			pst.executeUpdate();
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Aeroporto aeroporto = (Aeroporto) entidade;
		ArrayList<String> whr = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();

		if (aeroporto.getId() != null) {
			whr.add(" a.id = " + aeroporto.getId());
		}

		if (aeroporto.getCepOuZip() == null || aeroporto.getCepOuZip().trim().equals("")) {
			aeroporto.setCepOuZip("");
		} else {
			whr.add(" a.cep_zip = '" + aeroporto.getCepOuZip() + "'");
		}

		if (aeroporto.getBairro() == null || aeroporto.getBairro().trim().equals("")) {
			aeroporto.setBairro("");
		}else {
			whr.add(" a.bairro like '%" + aeroporto.getBairro() + "%'");
		}
		
		if (aeroporto.getLogradouro() == null || aeroporto.getLogradouro().trim().equals("")) {
			aeroporto.setLogradouro("");
		}else {
			whr.add(" a.logradouro like '%" + aeroporto.getLogradouro() + "%'");
		}
		
		if (aeroporto.getNumero() == null || aeroporto.getNumero().trim().equals("")) {
			aeroporto.setNumero("");
		} else {
			whr.add(" a.numero = '" + aeroporto.getNumero() + "'");
		}

		if (aeroporto.getNome() == null || aeroporto.getNome().trim().equals("")) {
			aeroporto.setNome("");
		} else {
			whr.add(" a.nome like '%" + aeroporto.getNome() + "%'");
		}
		
		if (aeroporto.getPais().getId() != null) {
			whr.add(" a.pais = " + aeroporto.getPais().getId());
		}

		if (aeroporto.getEstado().getId() != null) {
			whr.add(" a.estado = " + aeroporto.getEstado().getId());
		}
		
		if(aeroporto.getCidade().getId() != null) {
			whr.add(" a.cidade = " + aeroporto.getCidade().getId());
		}
		

		sql.append("SELECT * FROM aeroporto a ");
		sql.append("left join pais p");
		sql.append(" on(a.pais = p.id) ");
		sql.append("left join estado e");
		sql.append(" on(a.estado = e.id) ");
		sql.append("left join cidade c");
		sql.append(" on(a.cidade = c.id) ");
		
		if (!whr.isEmpty()) {
			sql.append("where");
			for (String where : whr) {
				if(where.equals(whr.get(whr.size()-1)))
					sql.append(where);
				else
					sql.append(where + " and");
			}
		}

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeImpl> aeroportos = new ArrayList<EntidadeImpl>();
			while (rs.next()) {
				Aeroporto a = new Aeroporto();
				Pais p = new Pais();
				Estado e = new Estado();
				Cidade c = new Cidade();
				a.setId(rs.getLong("a.id"));
				a.setDtCadastro(rs.getTimestamp("a.dt_cadastro").toLocalDateTime());
				a.setDtUltimaAlteracao(rs.getTimestamp("a.dt_ultima_alteracao").toLocalDateTime());
				a.setAtivo(rs.getBoolean("a.ativo"));
				a.setHash(UUID.fromString(rs.getString("a.hash")));
				a.setCepOuZip(rs.getString("a.cep_zip"));
				a.setBairro(rs.getString("a.bairro"));
				a.setLogradouro(rs.getString("a.logradouro"));
				a.setNumero(rs.getString("a.numero"));
				a.setNome(rs.getString("a.nome"));
				
				p.setId(rs.getLong("p.id"));
				p.setNome(rs.getString("p.nome"));
				a.setPais(p);
				
				e.setId(rs.getLong("e.id"));
				e.setNome(rs.getString("e.nome"));
				a.setEstado(e);
				
				c.setId(rs.getLong("c.id"));
				c.setNome(rs.getString("c.nome"));
				a.setCidade(c);

				aeroportos.add(a);
			}
			return aeroportos;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
