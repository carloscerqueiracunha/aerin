package com.aeroporto.core.impl.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Grafico;

public class GraficoDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		Grafico graf = (Grafico) entidade;
		PreparedStatement pst = null;

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT viagem.id, origem.nome AS nome_origem, destino.nome AS nome_destino "); 
		sql.append("FROM passagem ");
		sql.append("JOIN pedido ON (pedido.id = passagem.pedido) ");
		sql.append("JOIN viagem ON (viagem.id = pedido.viagem) ");
		sql.append("JOIN trajeto ON (viagem.trajeto = trajeto.id) ");
		sql.append("JOIN aeroporto origem ON (trajeto.origem = origem.id) ");
		sql.append("JOIN aeroporto destino ON (trajeto.destino = destino.id) ");
		sql.append("WHERE passagem.dt_cadastro BETWEEN ? AND ? ");
		sql.append("GROUP BY viagem.id");

		try {
			openConnection();
			
			pst = connection.prepareStatement(sql.toString());
			pst.setDate(1, Date.valueOf(graf.getDataInicio()));
			pst.setDate(2, Date.valueOf(graf.getDataFim()));

			ResultSet rs = pst.executeQuery();
			List<EntidadeImpl> graficos = new ArrayList<>();
			while (rs.next()) {
				Grafico grafico = new Grafico();
				grafico.setNomeOrigem(rs.getString("nome_origem"));
				grafico.setNomeDestino(rs.getString("nome_destino"));
				grafico.setIdViagem(rs.getLong("viagem.id"));
				
				graficos.add(grafico);
			}
			
			sql = new StringBuilder();
    		sql.append("SELECT viagem.id, DATE_FORMAT(passagem.dt_cadastro, '%c' '%Y') AS mesAno, COUNT(MONTH(passagem.dt_cadastro)) as quantidade_mes ");
    		sql.append("FROM passagem ");
    		sql.append("JOIN pedido ON (pedido.id = passagem.pedido) ");
    		sql.append("JOIN viagem ON (viagem.id = pedido.viagem) ");
    		sql.append("JOIN trajeto ON (viagem.trajeto = trajeto.id) ");
    		sql.append("JOIN aeroporto origem ON (trajeto.origem = origem.id) ");
    		sql.append("JOIN aeroporto destino ON (trajeto.destino = destino.id) ");
    		sql.append("WHERE passagem.dt_cadastro BETWEEN ? AND ? ");
    		sql.append("GROUP BY viagem.id, mesAno");
    		
    		pst = connection.prepareStatement(sql.toString());
    		pst.setDate(1, Date.valueOf(graf.getDataInicio()));
			pst.setDate(2, Date.valueOf(graf.getDataFim()));
			
    		rs = pst.executeQuery();
    		
    		Grafico graficoAux;
    		Long idAux;
    		String mesAux;
    		Integer quantidadeAux;
    		Map<String, Integer> quantidadeMes;
    		while(rs.next()) {
    			for(int i = 0; i < graficos.size(); i++) {
    				graficoAux = (Grafico) graficos.get(i);
    				idAux = rs.getLong("id");
    				mesAux = rs.getString("mesAno");
    				quantidadeAux = rs.getInt("quantidade_mes");
    				if(graficoAux.getIdViagem() == idAux) {
    					if (graficoAux.getQuantidadeMes() == null) {
    						quantidadeMes = new HashMap<>();
    					} else {
    						quantidadeMes = graficoAux.getQuantidadeMes();
    					}
    					quantidadeMes.put(mesAux, quantidadeAux);
    					graficoAux.setQuantidadeMes(quantidadeMes);
    				}
    			}
    		}
			return graficos;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
