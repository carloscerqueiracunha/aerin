package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Trajeto;

public class TrajetoDAO extends AbstractJdbcDAO{

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		if(connection == null){
			openConnection();
		}
		PreparedStatement pst = null;
		Trajeto trajeto = (Trajeto)entidade;
		StringBuilder sql = new StringBuilder();
		
		sql.append("INSERT INTO trajeto(dt_cadastro, dt_ultima_alteracao, ");
		sql.append("ativo, hash, origem, destino) ");
		sql.append("VALUES (?, ?, ?, ?, ?, ?)");
		try {
			connection.setAutoCommit(false);
			
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			
			pst.setTimestamp(1, Timestamp.valueOf(trajeto.getDtCadastro()));
			pst.setTimestamp(2, Timestamp.valueOf(trajeto.getDtUltimaAlteracao()));
			pst.setBoolean(3, trajeto.isAtivo());
			pst.setString(4, trajeto.getHash().toString());
			pst.setLong(5, trajeto.getOrigem().getId());
			pst.setLong(6, trajeto.getDestino().getId());
			
			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();
			Long idTrajeto = null;
			if(rs.next())
				idTrajeto = rs.getLong(1);
			trajeto.setId(idTrajeto);
			
			connection.commit(); // commita
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
		}
		
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		if(connection == null){
			openConnection();
		}
		PreparedStatement pst=null;
		Trajeto trajeto = (Trajeto)entidade;
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE trajeto SET dt_ultima_alteracao=?, ");
			sql.append("origem=?, destino=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setLong(2, trajeto.getOrigem().getId());
			pst.setLong(3, trajeto.getDestino().getId());
			pst.setLong(4, trajeto.getId());
			pst.executeUpdate();
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Trajeto trajeto = (Trajeto)entidade;
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE trajeto SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, false);
			pst.setLong(3, trajeto.getId());
			pst.executeUpdate();
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Trajeto trajeto = (Trajeto)entidade;
		
		try {
			connection.setAutoCommit(false);
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE trajeto SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");
					
			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, true);
			pst.setLong(3, trajeto.getId());
			pst.executeUpdate();
			
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Trajeto trajeto = (Trajeto) entidade;
		ArrayList<String> whr = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();

		if (trajeto.getId() != null) {
			whr.add(" id = " + trajeto.getId());
		}

		if(trajeto.getOrigem().getId() != null) {
			whr.add(" origem = " + trajeto.getOrigem().getId());
		}
		
		if(trajeto.getDestino().getId() != null) {
			whr.add(" destino = " + trajeto.getDestino().getId());
		}

		sql.append("SELECT * FROM trajeto t ");
		sql.append("left join aeroporto o ");
		sql.append("on(t.origem = o.id) ");
		sql.append("left join aeroporto d ");
		sql.append("on(t.destino = d.id) ");
		
		if (!whr.isEmpty()) {
			sql.append("where");
			for (String where : whr) {
				if(where.equals(whr.get(whr.size()-1)))
					sql.append(where);
				else
					sql.append(where + " and");
			}
		}

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeImpl> passageiros = new ArrayList<EntidadeImpl>();
			while (rs.next()) {
				Trajeto t = new Trajeto();
				Aeroporto origem = new Aeroporto();
				Aeroporto destino = new Aeroporto();
				t.setId(rs.getLong("id"));
				t.setDtCadastro(rs.getTimestamp("dt_cadastro").toLocalDateTime());
				t.setDtUltimaAlteracao(rs.getTimestamp("dt_ultima_alteracao").toLocalDateTime());
				t.setAtivo(rs.getBoolean("ativo"));
				t.setHash(UUID.fromString(rs.getString("hash")));
				origem.setId(rs.getLong("o.id"));
				origem.setNome(rs.getString("o.nome"));
				destino.setId(rs.getLong("d.id"));
				destino.setNome(rs.getString("d.nome"));

				t.setOrigem(origem);
				t.setDestino(destino);
				
				passageiros.add(t);
			}
			return passageiros;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
