package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.Bagagem;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Pais;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Passagem;
import com.aeroporto.dominio.Pedido;
import com.aeroporto.dominio.Trajeto;
import com.aeroporto.dominio.Viagem;

public class PassagemDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Passagem passagem = (Passagem) entidade;

		try {
			connection.setAutoCommit(false);

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO passagem(dt_cadastro, dt_ultima_alteracao, ");
			sql.append("ativo, hash, status, preco, tipo, faixa_etaria, pedido) ");
			sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(passagem.getDtCadastro()));
			pst.setTimestamp(2, Timestamp.valueOf(passagem.getDtUltimaAlteracao()));
			pst.setBoolean(3, passagem.isAtivo());
			pst.setString(4, passagem.getHash().toString());
			pst.setString(5, passagem.getStatus());
			pst.setDouble(6, passagem.getPreco());
			pst.setString(7, passagem.getTipo());
			pst.setString(8, passagem.getFaixaEtaria());
			pst.setLong(9, passagem.getPedido().getId());

			pst.executeUpdate();

			connection.commit(); // commita
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Passagem passagem = (Passagem) entidade;

		try {
			connection.setAutoCommit(false);

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE passagem SET dt_ultima_alteracao=?, ");
			sql.append("status=?, observacao=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setString(2, passagem.getStatus());
			if(passagem.getStatus().equals("BARRADO RAIO-X") || passagem.getStatus().equals("BARRADO ALFANDEGA"))
				pst.setString(3, passagem.getObservacao());
			else
				pst.setString(3, "");
			pst.setLong(4, passagem.getId());
			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Passagem passagem = (Passagem) entidade;

		try {
			connection.setAutoCommit(false);

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE passagem SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, false);
			pst.setLong(3, passagem.getId());
			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Passagem passagem = (Passagem) entidade;

		try {
			connection.setAutoCommit(false);

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE passagem SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());
			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, true);
			pst.setLong(3, passagem.getId());
			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Passagem passagem = (Passagem) entidade;
		ArrayList<String> whr = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();
		BagagemDAO bagagemDAO;

		if (passagem.getId() != null) {
			whr.add(" p.id = " + passagem.getId());
		}
		
		if (passagem.getPedido() != null && passagem.getPedido().getId() != null) {
			whr.add(" p.pedido = " + passagem.getPedido().getId());
		}

		if (passagem.getStatus() == null || passagem.getStatus().equals("")) {
			passagem.setStatus("");
		} else {
			whr.add(" p.status like " + "'%" + passagem.getStatus() + "%'");
		}

		if (passagem.getPreco() != null) {
			whr.add(" p.preco = " + passagem.getPreco());
		}

		if (passagem.getTipo() == null || passagem.getTipo().equals("")) {
			passagem.setTipo("");
		} else {
			whr.add(" p.tipo like " + "%'" + passagem.getTipo() + "'%");
		}

		sql.append("SELECT * FROM passagem p ");
		sql.append("left join pedido pe ");
		sql.append("on(pe.id = p.pedido) ");
		sql.append("left join viagem vi ");
		sql.append("on(pe.viagem = vi.id) ");
		sql.append("left join passageiro pa ");
		sql.append("on(pe.passageiro = pa.id) ");
		sql.append("left join trajeto t ");
		sql.append("on(vi.trajeto = t.id) ");
		sql.append("left join aeroporto o ");
		sql.append("on(t.origem = o.id) ");
		sql.append("left join aeroporto d ");
		sql.append("on(t.destino = d.id) ");

		if (!whr.isEmpty()) {
			sql.append("where");
			for (String where : whr) {
				if (where.equals(whr.get(whr.size() - 1)))
					sql.append(where);
				else
					sql.append(where + " and");
			}
		}

		try {
			if (connection == null)
				openConnection();
			pst = connection.prepareStatement(sql.toString());

			ResultSet rs = pst.executeQuery();
			List<EntidadeImpl> passagens = new ArrayList<EntidadeImpl>();
			while (rs.next()) {
				Passagem p = new Passagem();
				Pedido pe = new Pedido();
				Bagagem b = new Bagagem();
				Viagem v = new Viagem();
				Passageiro passa =  new Passageiro();
				Trajeto t = new Trajeto();
				Aeroporto o = new Aeroporto();
				Aeroporto d = new Aeroporto();
				Pais paisOrigem = new Pais();
				Pais paisDestino = new Pais();
				p.setId(rs.getLong("p.id"));
				p.setDtCadastro(rs.getTimestamp("p.dt_cadastro").toLocalDateTime());
				p.setDtUltimaAlteracao(rs.getTimestamp("p.dt_ultima_alteracao").toLocalDateTime());
				p.setAtivo(rs.getBoolean("p.ativo"));
				p.setHash(UUID.fromString(rs.getString("p.hash")));
				p.setStatus(rs.getString("p.status"));
				p.setPreco(rs.getDouble("p.preco"));
				p.setTipo(rs.getString("p.tipo"));
				p.setFaixaEtaria(rs.getString("faixa_etaria"));
				p.setObservacao(rs.getString("p.observacao"));
				
				pe.setId(rs.getLong("pe.id"));
				
				v.setId(rs.getLong("vi.id"));
				v.setIdaVolta(rs.getBoolean("vi.ida_volta"));
				v.setDataHoraPartida(rs.getTimestamp("vi.dataHoraPartida").toLocalDateTime());
				v.setDataHoraRetorno(rs.getTimestamp("vi.dataHoraRetorno").toLocalDateTime());
				
				paisOrigem.setId(rs.getLong("o.pais"));
				paisDestino.setId(rs.getLong("d.pais"));
				o.setPais(paisOrigem);
				d.setPais(paisDestino);
				t.setOrigem(o);
				t.setDestino(d);
				
				v.setTrajeto(t);
				
				passa.setId(rs.getLong("pa.id"));
				
				pe.setViagem(v);
				pe.setPassageiro(passa);
				
				p.setPedido(pe);
				
				// Consultar as bagagens dessa passagem
				b.setPassagem(p);
				bagagemDAO = new BagagemDAO();
				bagagemDAO.ctrlTransaction = false;
				bagagemDAO.connection = connection;
				Bagagem ba = new Bagagem();
				ArrayList<Bagagem> bagagens = new ArrayList<Bagagem>();
				try {
					for (EntidadeImpl bagagem : bagagemDAO.consultar(b)) {
						ba = (Bagagem) bagagem;
						bagagens.add(ba);
					}
				} catch (Exception ex) {
					// Pra n�o pegar mais cart�es que o passageiro possui
				}
				p.setBagagens(bagagens);

				passagens.add(p);
			}
			return passagens;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
