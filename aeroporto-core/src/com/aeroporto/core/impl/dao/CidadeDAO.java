package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Cidade;

public class CidadeDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Cidade cidade = (Cidade) entidade;
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM cidade");
		
		if(cidade.getId() != null)
			sql.append(" where id=" + cidade.getId());

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());
			
			List<EntidadeImpl> cidades = new ArrayList<EntidadeImpl>();
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Cidade c = new Cidade();
				c.setId(rs.getLong("id"));
				c.setNome(rs.getString("nome"));
				
				cidades.add(c);
			}

			return cidades;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
