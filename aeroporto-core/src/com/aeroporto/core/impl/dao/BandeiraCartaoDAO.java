package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.aeroporto.dominio.BandeiraCartao;
import com.aeroporto.dominio.EntidadeImpl;

public class BandeiraCartaoDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		BandeiraCartao bandeiraCartao = (BandeiraCartao) entidade;
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM bandeira_cartao");
		
		if(bandeiraCartao.getId() != null)
			sql.append(" where id=" + bandeiraCartao.getId());

		try {
			openConnection();
			pst = connection.prepareStatement(sql.toString());
			
			List<EntidadeImpl> bandeiras = new ArrayList<EntidadeImpl>();
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				BandeiraCartao b = new BandeiraCartao();
				b.setId(rs.getLong("id"));
				b.setNome(rs.getString("nome"));
				
				bandeiras.add(b);
			}

			return bandeiras;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
