package com.aeroporto.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.aeroporto.dominio.Bagagem;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passagem;

public class BagagemDAO extends AbstractJdbcDAO {

	@Override
	public void salvar(EntidadeImpl entidade) throws SQLException {
		openConnection();

		PreparedStatement pst = null;
		Bagagem bagagem = (Bagagem) entidade;
		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO bagagem(dt_cadastro, dt_ultima_alteracao, ");
		sql.append("ativo, hash, peso, altura, largura, profundidade, passagem)");
		sql.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		try {
			connection.setAutoCommit(false);

			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			pst.setTimestamp(1, Timestamp.valueOf(bagagem.getDtCadastro()));
			pst.setTimestamp(2, Timestamp.valueOf(bagagem.getDtUltimaAlteracao()));
			pst.setBoolean(3, bagagem.isAtivo());
			pst.setString(4, bagagem.getHash().toString());
			pst.setDouble(5, bagagem.getPeso());
			pst.setDouble(6, bagagem.getAltura());
			pst.setDouble(7, bagagem.getLargura());
			pst.setDouble(8, bagagem.getProfundidade());
			pst.setLong(9, bagagem.getPassagem().getId());
			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();
			Long idBag = null;
			if (rs.next())
				idBag = rs.getLong(1);
			bagagem.setId(idBag);

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void alterar(EntidadeImpl entidade) throws SQLException {
		if (connection == null) {
			openConnection();
		}
		PreparedStatement pst = null;
		Bagagem bagagem = (Bagagem) entidade;
		StringBuilder sql = new StringBuilder();

		try {
			connection.setAutoCommit(false);

			sql.append("UPDATE bagagem SET dt_ultima_alteracao=?, peso=?, altura=?, largura=?, profundidade=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setDouble(2, bagagem.getPeso());
			pst.setDouble(3, bagagem.getAltura());
			pst.setDouble(4, bagagem.getLargura());
			pst.setDouble(5, bagagem.getProfundidade());
			pst.setLong(6, bagagem.getPassagem().getId());
			pst.setLong(7, bagagem.getId());

			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void excluir(EntidadeImpl entidade) throws SQLException {
		openConnection();

		PreparedStatement pst = null;
		Bagagem bagagem = (Bagagem) entidade;
		StringBuilder sql = new StringBuilder();

		try {
			connection.setAutoCommit(false);

			sql.append("UPDATE bagagem SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, false);
			pst.setLong(3, bagagem.getId());

			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void ativar(EntidadeImpl entidade) throws SQLException {
		openConnection();

		PreparedStatement pst = null;
		Bagagem bagagem = (Bagagem) entidade;
		StringBuilder sql = new StringBuilder();

		try {
			connection.setAutoCommit(false);

			sql.append("UPDATE bagagem SET dt_ultima_alteracao=?, ");
			sql.append("ativo=? ");
			sql.append("WHERE id=?");

			pst = connection.prepareStatement(sql.toString());

			pst.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
			pst.setBoolean(2, true);
			pst.setLong(3, bagagem.getId());

			pst.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<EntidadeImpl> consultar(EntidadeImpl entidade) throws SQLException {
		PreparedStatement pst = null;

		Bagagem bagagem = (Bagagem) entidade;
		ArrayList<String> whr = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();

		if (bagagem.getPassagem().getId() != null) {
			whr.add(" passagem = " + bagagem.getPassagem().getId());
		}

		sql.append("SELECT * FROM bagagem ");

		if (!whr.isEmpty()) {
			sql.append("where");
			for (String where : whr) {
				if (where.equals(whr.get(whr.size() - 1)))
					sql.append(where);
				else
					sql.append(where + " and");
			}
		}

		try {
			if (connection == null) {
				openConnection();
			}
			pst = connection.prepareStatement(sql.toString());
			List<EntidadeImpl> bagagens = new ArrayList<EntidadeImpl>();
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Bagagem b = new Bagagem();
				Passagem p = new Passagem();
				b.setPassagem(bagagem.getPassagem());
				b.setId(rs.getLong("id"));
				b.setDtCadastro(rs.getTimestamp("dt_cadastro").toLocalDateTime());
				b.setDtUltimaAlteracao(rs.getTimestamp("dt_ultima_alteracao").toLocalDateTime());
				b.setAtivo(rs.getBoolean("ativo"));
				b.setHash(UUID.fromString(rs.getString("hash")));
				b.setPeso(rs.getDouble("peso"));
				b.setAltura(rs.getDouble("altura"));
				b.setLargura(rs.getDouble("largura"));
				b.setProfundidade(rs.getDouble("profundidade"));
				p.setId(rs.getLong("passagem"));
				b.setPassagem(p);

				bagagens.add(b);
			}

			return bagagens;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
