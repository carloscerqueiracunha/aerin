package com.aeroporto.core.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Realiza a conex�o com o banco de dados mysql
 * @author Carlos
 *
 */
public class Conexao {
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://localhost/aeroporto?useSSL=false&useTimezone=true&serverTimezone=GMT-2";
		String user = "root";
		String password = "CG1d)*J13m@*";
		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, user, password);
		
		return conn;
		
	}
	
}
