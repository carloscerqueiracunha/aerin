
package com.aeroporto.controle.web.command.impl;

import com.aeroporto.controle.web.command.ICommand;
import com.aeroporto.core.IFachada;
import com.aeroporto.core.impl.controle.Fachada;



public abstract class AbstractCommand implements ICommand {

	protected IFachada fachada = new Fachada();

}
