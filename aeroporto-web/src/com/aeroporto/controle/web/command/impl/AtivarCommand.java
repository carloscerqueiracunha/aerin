
package com.aeroporto.controle.web.command.impl;

import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.EntidadeImpl;


public class AtivarCommand extends AbstractCommand{

	
	public Resultado execute(EntidadeImpl entidade) {
		
		return fachada.ativar(entidade);
	}

}
