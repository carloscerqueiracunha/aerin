
package com.aeroporto.controle.web.command;

import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.EntidadeImpl;


public interface ICommand {

	public Resultado execute(EntidadeImpl entidade);
	
}
