package com.aeroporto.controle.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aeroporto.controle.web.command.ICommand;
import com.aeroporto.controle.web.command.impl.AlterarCommand;
import com.aeroporto.controle.web.command.impl.AtivarCommand;
import com.aeroporto.controle.web.command.impl.ConsultarCommand;
import com.aeroporto.controle.web.command.impl.ExcluirCommand;
import com.aeroporto.controle.web.command.impl.SalvarCommand;
import com.aeroporto.controle.web.command.impl.VisualizarCommand;
import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.controle.web.vh.impl.AeroportoViewHelper;
import com.aeroporto.controle.web.vh.impl.BagagemViewHelper;
import com.aeroporto.controle.web.vh.impl.CartaoViewHelper;
import com.aeroporto.controle.web.vh.impl.GraficoViewHelper;
import com.aeroporto.controle.web.vh.impl.PassageiroViewHelper;
import com.aeroporto.controle.web.vh.impl.PassagemViewHelper;
import com.aeroporto.controle.web.vh.impl.PedidoViewHelper;
import com.aeroporto.controle.web.vh.impl.UsuarioViewHelper;
import com.aeroporto.controle.web.vh.impl.ViagemViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.EntidadeImpl;

/**
 * Servlet implementation class Servlet
 */
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static Map<String, ICommand> commands;
	private static Map<String, IViewHelper> vhs;

	/**
	 * Default constructor.
	 */
	public Servlet() {

		/*
		 * Utilizando o command para chamar a fachada e indexando cada command pela
		 * opera��o garantimos que esta servelt atender� qualquer opera��o
		 */
		commands = new HashMap<String, ICommand>();

		commands.put("SALVAR", new SalvarCommand());
		commands.put("EXCLUIR", new ExcluirCommand());
		commands.put("CONSULTAR", new ConsultarCommand());
		commands.put("VISUALIZAR", new VisualizarCommand());
		commands.put("ALTERAR", new AlterarCommand());
		commands.put("ATIVAR", new AtivarCommand());

		/*
		 * Utilizando o ViewHelper para tratar especifica��es de qualquer tela e
		 * indexando cada viewhelper pela url em que esta servlet � chamada no form
		 * garantimos que esta servelt atender� qualquer entidade
		 */

		vhs = new HashMap<String, IViewHelper>();
		/*
		 * A chave do mapa � o mapeamento da servlet para cada form que est� configurado
		 * no web.xml e sendo utilizada no action do html
		 */
		vhs.put("/aeroporto-web/Passageiro", new PassageiroViewHelper());
		vhs.put("/aeroporto-web/Aeroporto", new AeroportoViewHelper());
		vhs.put("/aeroporto-web/Viagem", new ViagemViewHelper());
		vhs.put("/aeroporto-web/Usuario", new UsuarioViewHelper());
		vhs.put("/aeroporto-web/Cartao", new CartaoViewHelper());
		vhs.put("/aeroporto-web/Passagem", new PassagemViewHelper());
		vhs.put("/aeroporto-web/Pedido", new PedidoViewHelper());
		vhs.put("/aeroporto-web/Bagagem", new BagagemViewHelper());
		vhs.put("/aeroporto-web/Grafico", new GraficoViewHelper());

	}

	/**
	 * TODO Descri��o do M�todo
	 * 
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcessRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcessRequest(request, response);
	}

	protected void doProcessRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Obt�m a uri que invocou esta servlet (O que foi definido no methdo do form
		// html)
		String uri = request.getRequestURI();

		// Obt�m a opera��o executada
		String operacao = request.getParameter("operacao");

		// Obt�m um viewhelper indexado pela uri que invocou esta servlet
		IViewHelper vh = vhs.get(uri);

		// O viewhelper retorna a entidade especifica para a tela que chamou esta
		// servlet
		EntidadeImpl entidade = vh.getEntidade(request);

		// Obt�m o command para executar a respectiva opera��o
		ICommand command = commands.get(operacao);

		/*
		 * Executa o command que chamar� a fachada para executar a opera��o requisitada
		 * o retorno � uma inst�ncia da classe resultado que pode conter mensagens derro
		 * ou entidades de retorno
		 */
		Resultado resultado = command.execute(entidade);

		/*
		 * Executa o m�todo setView do view helper espec�fico para definir como dever�
		 * ser apresentado o resultado para o usu�rio
		 */
		vh.setView(resultado, request, response);

	}
}
