package com.aeroporto.controle.web.vh.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.BandeiraCartao;
import com.aeroporto.dominio.Cartao;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Sexo;
import com.aeroporto.dominio.TipoTelefone;
import com.aeroporto.dominio.Usuario;

public class CartaoViewHelper implements IViewHelper {

	@Override
	public EntidadeImpl getEntidade(HttpServletRequest request) {

		String operacao = request.getParameter("operacao");
		Cartao cartao = null;

		if (!operacao.equals("VISUALIZAR")) {
			cartao = new Cartao();
			Passageiro passageiro = new Passageiro();

			String idPassageiro = request.getParameter("txtIdPassageiro");

			if (idPassageiro != null && !idPassageiro.trim().equals("")) {
				passageiro.setId(Long.parseLong(idPassageiro));
			}
			
			cartao.setPassageiro(passageiro);

		} else {
			cartao = null;
			HttpSession session = request.getSession();
			Resultado resultado = (Resultado) session.getAttribute("resultado");
			String txtId = request.getParameter("txtId");
			Long id = null;

			if (txtId != null && !txtId.trim().equals("")) {
				id = Long.parseLong(txtId);
			}

			for (EntidadeImpl e : resultado.getEntidades()) {
				if (e.getId() == id) {
					cartao = (Cartao) e;
				}
			}
		}
		return cartao;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RequestDispatcher d = null;

		String operacao = request.getParameter("operacao");

		if (resultado.getMensagem() == null) {
			request.getSession().setAttribute("cartoes", resultado.getEntidades().get(0));
			d = request.getRequestDispatcher("CompraPassagem.jsp");
		}

		d.forward(request, response);
	}

}
