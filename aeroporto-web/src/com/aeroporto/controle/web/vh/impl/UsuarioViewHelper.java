package com.aeroporto.controle.web.vh.impl;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Usuario;

public class UsuarioViewHelper implements IViewHelper {

	@Override
	public EntidadeImpl getEntidade(HttpServletRequest request) {

		String operacao = request.getParameter("operacao");
		Usuario usuario = null;
		if (!operacao.equals("VISUALIZAR")) {
			usuario = new Usuario();

			String id = request.getParameter("txtId");

			if (id != null && !id.trim().equals("")) {
				usuario.setId(Long.parseLong(id));
			}

			String email = request.getParameter("txtEmail");
			String senha = request.getParameter("txtSenha");
			String confirmaSenha = request.getParameter("txtConfirmaSenha");

			if (email != null && !email.trim().equals("")) {
				usuario.setEmail(email);
			}
			if (senha != null && !email.trim().equals("")) {
				usuario.setSenha(senha);
			}
			if (confirmaSenha != null && !confirmaSenha.trim().equals("")) {
				usuario.setConfirmaSenha(confirmaSenha);
			}

		} else {
			usuario = null;
			HttpSession session = request.getSession();
			Resultado resultado = (Resultado) session.getAttribute("resultado");
			String txtId = request.getParameter("txtId");
			Long id = null;

			if (txtId != null && !txtId.trim().equals("")) {
				id = Long.parseLong(txtId);
			}

			for (EntidadeImpl e : resultado.getEntidades()) {
				if (e.getId() == id) {
					usuario = (Usuario) e;
				}
			}
		}
		return usuario;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RequestDispatcher d = null;
		
		Usuario usuario = null;
				
		for(EntidadeImpl entidade : resultado.getEntidades()) {
			usuario = (Usuario) entidade;
		}

		String operacao = request.getParameter("operacao");

		if (resultado.getEntidades() == null || resultado.getEntidades().isEmpty()) {
			resultado.setMensagem("E-mail ou senha incorretos");
			request.getSession().setAttribute("resultadoLogin", resultado);
			d = request.getRequestDispatcher("login.jsp");
		} else {
			if (resultado.getMensagem() == null) {
				if (operacao.equals("CONSULTAR")) {
					request.getSession().setAttribute("usuarioLogado", resultado);
					if (usuario.getAdmin())
						d = request.getRequestDispatcher("ConsultaAeroporto.jsp");
					else
						d = request.getRequestDispatcher("ConsultaViagem.jsp");
				}
			}
		}

		d.forward(request, response);
	}

}
