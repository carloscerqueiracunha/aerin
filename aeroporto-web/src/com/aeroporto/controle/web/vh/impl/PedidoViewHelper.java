package com.aeroporto.controle.web.vh.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.Cartao;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Passagem;
import com.aeroporto.dominio.Pedido;
import com.aeroporto.dominio.Viagem;

public class PedidoViewHelper implements IViewHelper {

	@Override
	public EntidadeImpl getEntidade(HttpServletRequest request) {

		String operacao = request.getParameter("operacao");
		Pedido pedido = null;

		if (operacao.equals("CONSULTAR")) {
			pedido = new Pedido();
		} else if (!operacao.equals("VISUALIZAR")) {
			Cartao cartao = new Cartao();
			Passageiro passageiro = new Passageiro();
			Viagem viagem;
			if("SALVAR".equals(operacao)) {
				viagem = (Viagem) request.getSession().getAttribute("viagemPedido");
			} else {
				viagem = new Viagem();
			}
			
			List<Passagem> passagens = new ArrayList<>();
			Passagem passagem;
			pedido = new Pedido();

			String idPassageiro = request.getParameter("txtIdPassageiro");
			String idCartao = request.getParameter("txtIdCartao");

			if (idPassageiro != null && !idPassageiro.trim().equals("")) {
				passageiro.setId(Long.parseLong(idPassageiro));
			}
			if (idCartao != null && !idCartao.trim().equals("")) {
				cartao.setId(Long.parseLong(idCartao));
			}
			
			String tipoPassagem = request.getParameter("txtTipoPassagem");
			String quantidadePassagensAdulto = request.getParameter("txtQuantidadePassagensAdulto");
			String quantidadePassagensCrianca = request.getParameter("txtQuantidadePassagensCrianca");
			String quantidadePassagensBebe = request.getParameter("txtQuantidadePassagensBebe");
			
			if (tipoPassagem != null && !tipoPassagem.trim().equals("")) {
				pedido.setTipoPassagem(tipoPassagem);
			}
			if (quantidadePassagensAdulto != null && !quantidadePassagensAdulto.trim().equals("")) {
				pedido.setQuantidadePassagensAdulto(Integer.parseInt(quantidadePassagensAdulto));
				for(int i = 0; i < pedido.getQuantidadePassagensAdulto(); i++) {
					passagem = new Passagem();
					passagem.setFaixaEtaria("Adulto");
					passagem.setStatus("AGUARDANDO CHECK-IN");
					passagem.setTipo(tipoPassagem);
					if(tipoPassagem.equals("Economica")) {
						passagem.setPreco(viagem.getPrecoEconomica());
					} else if(tipoPassagem.equals("Executiva")) {
						passagem.setPreco(viagem.getPrecoExecutiva());
					} else if(tipoPassagem.equals("Primeira")) {
						passagem.setPreco(viagem.getPrecoPrimeira());
					}
					passagens.add(passagem);
				}
			} else {
				pedido.setQuantidadePassagensAdulto(0);
			}
			if (quantidadePassagensCrianca != null && !quantidadePassagensCrianca.trim().equals("")) {
				pedido.setQuantidadePassagensCrianca(Integer.parseInt(quantidadePassagensCrianca));
				for(int i = 0; i < pedido.getQuantidadePassagensCrianca(); i++) {
					passagem = new Passagem();
					passagem.setFaixaEtaria("Crianca");
					passagem.setStatus("AGUARDANDO CHECK-IN");
					passagem.setTipo(tipoPassagem);
					if(tipoPassagem.equals("Economica")) {
						passagem.setPreco(viagem.getPrecoEconomica() / 2);
					} else if(tipoPassagem.equals("Executiva")) {
						passagem.setPreco(viagem.getPrecoExecutiva() / 2);
					} else if(tipoPassagem.equals("Primeira")) {
						passagem.setPreco(viagem.getPrecoPrimeira() / 2);
					}
					passagens.add(passagem);
				}
			} else {
				pedido.setQuantidadePassagensCrianca(0);
			}
			if (quantidadePassagensBebe != null && !quantidadePassagensBebe.trim().equals("")) {
				pedido.setQuantidadePassagensBebe(Integer.parseInt(quantidadePassagensBebe));
				for(int i = 0; i < pedido.getQuantidadePassagensBebe(); i++) {
					passagem = new Passagem();
					passagem.setFaixaEtaria("Bebe");
					passagem.setStatus("AGUARDANDO CHECK-IN");
					passagem.setTipo(tipoPassagem);
					if(tipoPassagem.equals("Economica")) {
						passagem.setPreco(new Double(0));
					} else if(tipoPassagem.equals("Executiva")) {
						passagem.setPreco(new Double(0));
					} else if(tipoPassagem.equals("Primeira")) {
						passagem.setPreco(new Double(0));
					}
					passagens.add(passagem);
				}
			} else {
				pedido.setQuantidadePassagensBebe(0);
			}
			pedido.setPassagens(passagens);
			pedido.setCartaoUtilizado(cartao);
			pedido.setPassageiro(passageiro);
			pedido.setViagem(viagem);

		} else {
			pedido = null;
			HttpSession session = request.getSession();
			Resultado resultado = (Resultado) session.getAttribute("resultado");
			String txtId = request.getParameter("txtId");
			Long id = null;

			if (txtId != null && !txtId.trim().equals("")) {
				id = Long.parseLong(txtId);
			}

			for (EntidadeImpl e : resultado.getEntidades()) {
				if (e.getId() == id) {
					pedido = (Pedido) e;
				}
			}
		}
		return pedido;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RequestDispatcher d = null;

		String operacao = request.getParameter("operacao");

		if (resultado.getMensagem() == null) {
			if (operacao.equals("SALVAR")) {
				resultado.setMensagem("Pedido realizado com sucesso!");
			}
			request.getSession().setAttribute("resultado", resultado);
			d = request.getRequestDispatcher("ConsultaPedido.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("ALTERAR")) {

			d = request.getRequestDispatcher("ConsultaPedido.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("VISUALIZAR")) {
			request.getSession().setAttribute("pedidoSessao", resultado.getEntidades().get(0));
			request.setAttribute("pedido", resultado.getEntidades().get(0));
			d = request.getRequestDispatcher("EditarPedido.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("EXCLUIR")) {

			request.getSession().setAttribute("resultado", null);
			d = request.getRequestDispatcher("ConsultaPedido.jsp");
		}

		if (resultado.getMensagem() != null) {
			if (operacao.equals("SALVAR") || operacao.equals("ALTERAR")) {
				request.getSession().setAttribute("resultado", resultado);
				d = request.getRequestDispatcher("ConsultaPedido.jsp");
			}
		}

		d.forward(request, response);
	}

}
