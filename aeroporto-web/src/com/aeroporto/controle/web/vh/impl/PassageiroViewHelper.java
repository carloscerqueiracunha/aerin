package com.aeroporto.controle.web.vh.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.BandeiraCartao;
import com.aeroporto.dominio.Cartao;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passageiro;
import com.aeroporto.dominio.Sexo;
import com.aeroporto.dominio.TipoTelefone;
import com.aeroporto.dominio.Usuario;

public class PassageiroViewHelper implements IViewHelper {

	@Override
	public EntidadeImpl getEntidade(HttpServletRequest request) {

		String operacao = request.getParameter("operacao");
		Passageiro passageiro = null;

		if (!operacao.equals("VISUALIZAR")) {
			Cartao cartao = new Cartao();
			BandeiraCartao bandeira = new BandeiraCartao();
			Usuario usuario = new Usuario();
			ArrayList<Cartao> cartoes = new ArrayList<>();
			passageiro = new Passageiro();

			String id = request.getParameter("txtId");
			String idUsuario = request.getParameter("txtIdUsuario");
			String idCartao = request.getParameter("txtIdCartao");

			if (id != null && !id.trim().equals("")) {
				passageiro.setId(Long.parseLong(id));
			}
			if (idUsuario != null && !idUsuario.trim().equals("")) {
				usuario.setId(Long.parseLong(idUsuario));
			}
			if (idCartao != null && !idCartao.trim().equals("")) {
				cartao.setId(Long.parseLong(idCartao));
			}

			String email = request.getParameter("txtEmail");
			String senha = request.getParameter("txtSenha");
			String confirmaSenha = request.getParameter("txtConfirmaSenha");
			String nome = request.getParameter("txtNome");
			LocalDate dtNascimento = null;
			try {
				String[] dtNasc = request.getParameter("txtDtNascimento").split("-");
				int dia = Integer.parseInt(dtNasc[2]);
				int mes = Integer.parseInt(dtNasc[1]);
				int ano = Integer.parseInt(dtNasc[0]);
				dtNascimento = LocalDate.of(ano, mes, dia);
			} catch (Exception e) {
				// Caso a data seja mandada vazia
			}
			String sexoString = request.getParameter("txtSexo");
			String cpf = request.getParameter("txtCpf");
			String tipoTelefoneString = request.getParameter("txtTipoTelefone");
			String telefone = request.getParameter("txtTelefone");
			// Cartao
			String numeroCartao = request.getParameter("txtNumeroCartao");
			String codigoSeguranca = request.getParameter("txtCodigoSeguranca");
			String bandeiraCartao = request.getParameter("txtBandeiraCartao");

			if (email != null && !email.trim().equals("")) {
				usuario.setEmail(email);
			}
			if (senha != null && !email.trim().equals("")) {
				usuario.setSenha(senha);
			}
			if (confirmaSenha != null && !confirmaSenha.trim().equals("")) {
				usuario.setConfirmaSenha(confirmaSenha);
			}

			passageiro.setUsuario(usuario);

			if (nome != null && !nome.trim().equals("")) {
				passageiro.setNome(nome);
			}
			if (dtNascimento != null) {
				passageiro.setDtNascimento(dtNascimento);
			}
			if (sexoString != null && !sexoString.trim().equals("")) {
				Sexo sexo = Enum.valueOf(Sexo.class, sexoString);
				passageiro.setSexo(sexo);
			}
			if (cpf != null && !cpf.trim().equals("")) {
				passageiro.setCpf(cpf);
			}
			if (telefone != null && !telefone.trim().equals("")) {
				passageiro.setTelefone(telefone);
			}
			if (tipoTelefoneString != null && !tipoTelefoneString.trim().equals("")) {
				TipoTelefone tipoTelefone = Enum.valueOf(TipoTelefone.class, tipoTelefoneString);
				passageiro.setTipoTelefone(tipoTelefone);
			}

			// Cartao
			if (numeroCartao != null && !numeroCartao.trim().equals("")) {
				cartao.setNumero(numeroCartao);
			}
			if (bandeiraCartao != null && !bandeiraCartao.trim().equals("")) {
				bandeira.setId(Long.parseLong(bandeiraCartao));
			}
			cartao.setBandeiraCartao(bandeira);
			if (codigoSeguranca != null && !codigoSeguranca.trim().equals("")) {
				cartao.setCodigoSeguranca(codigoSeguranca);
			}
			cartao.setBandeiraCartao(bandeira);
			cartoes.add(cartao);
			passageiro.setCartoes(cartoes);
		} else {
			passageiro = null;
			HttpSession session = request.getSession();
			Resultado resultado = (Resultado) session.getAttribute("resultado");
			String txtId = request.getParameter("txtId");
			Long id = null;

			if (txtId != null && !txtId.trim().equals("")) {
				id = Long.parseLong(txtId);
			}

			for (EntidadeImpl e : resultado.getEntidades()) {
				if (e.getId() == id) {
					passageiro = (Passageiro) e;
				}
			}
		}
		return passageiro;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RequestDispatcher d = null;

		String operacao = request.getParameter("operacao");
		String compra = request.getParameter("flgCompra");

		if (resultado.getMensagem() == null) {
			if (operacao.equals("SALVAR")) {
				resultado.setMensagem("Passageiro cadastrado com sucesso!");
			}
			if (compra != null && compra.trim().equals("true")) {
				request.setAttribute("passageiroCompra", resultado.getEntidades().get(0));
				d = request.getRequestDispatcher("CompraPassagem.jsp");
			} else {
				request.getSession().setAttribute("resultado", resultado);
				d = request.getRequestDispatcher("ConsultaPassageiro.jsp");
			}
		}

		if (resultado.getMensagem() == null && operacao.equals("ALTERAR")) {

			d = request.getRequestDispatcher("ConsultaPassageiro.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("VISUALIZAR")) {

			request.setAttribute("passageiro", resultado.getEntidades().get(0));
			d = request.getRequestDispatcher("EditarPassageiro.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("EXCLUIR")) {

			request.getSession().setAttribute("resultado", null);
			d = request.getRequestDispatcher("ConsultaPassageiro.jsp");
		}

		if (resultado.getMensagem() != null) {
			if (operacao.equals("SALVAR") || operacao.equals("ALTERAR")) {
				request.getSession().setAttribute("resultado", resultado);
				d = request.getRequestDispatcher("ConsultaPassageiro.jsp");
			}
		}

		d.forward(request, response);
	}

}
