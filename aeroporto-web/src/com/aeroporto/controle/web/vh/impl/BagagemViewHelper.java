package com.aeroporto.controle.web.vh.impl;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.Bagagem;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passagem;

public class BagagemViewHelper implements IViewHelper {

	@Override
	public EntidadeImpl getEntidade(HttpServletRequest request) {

		String operacao = request.getParameter("operacao");
		Bagagem bagagem = null;

		if (!operacao.equals("VISUALIZAR")) {
			bagagem = new Bagagem();
			Passagem passagem = new Passagem();

			String idBagagem = request.getParameter("txtId");
			String idPassagem = request.getParameter("txtIdPassagem");
			String peso = request.getParameter("txtPeso");
			String altura = request.getParameter("txtAltura");
			String largura = request.getParameter("txtLargura");
			String profundidade = request.getParameter("txtProfundidade");
			
			if (idPassagem != null && !idPassagem.trim().equals("")) {
				passagem.setId(Long.parseLong(idPassagem));
			}
			if (peso != null && !peso.trim().equals("")) {
				bagagem.setPeso(Double.parseDouble(peso));
			}
			if (altura != null && !altura.trim().equals("")) {
				bagagem.setAltura(Double.parseDouble(altura));
			}
			if (largura != null && !largura.trim().equals("")) {
				bagagem.setLargura(Double.parseDouble(largura));
			}
			if (profundidade != null && !profundidade.trim().equals("")) {
				bagagem.setProfundidade(Double.parseDouble(profundidade));
			}
			if (idBagagem != null && !idBagagem.trim().equals("")) {
				bagagem.setId(Long.parseLong(idBagagem));
			}
			
			bagagem.setPassagem(passagem);
		} else {
			bagagem = null;
			HttpSession session = request.getSession();
			Resultado resultado = (Resultado) session.getAttribute("resultado");
			String txtId = request.getParameter("txtId");
			Long id = null;

			if (txtId != null && !txtId.trim().equals("")) {
				id = Long.parseLong(txtId);
			}

			for (EntidadeImpl e : resultado.getEntidades()) {
				if (e.getId() == id) {
					bagagem = (Bagagem) e;
				}
			}
		}
		return bagagem;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RequestDispatcher d = null;

		String operacao = request.getParameter("operacao");

		if (resultado.getMensagem() == null) {
			if (operacao.equals("SALVAR")) {
				resultado.setMensagem("Passagem comprada com sucesso!");
			}
			request.getSession().setAttribute("bagagens", resultado);
			d = request.getRequestDispatcher("ConsultaPassagem.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("ALTERAR")) {

			d = request.getRequestDispatcher("ConsultaPassagem.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("VISUALIZAR")) {

			request.setAttribute("bagagem", resultado.getEntidades().get(0));
			d = request.getRequestDispatcher("EditarPassagem.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("EXCLUIR")) {

			request.getSession().setAttribute("bagagens", null);
			d = request.getRequestDispatcher("ConsultaPassagem.jsp");
		}

		if (resultado.getMensagem() != null) {
			if (operacao.equals("SALVAR") || operacao.equals("ALTERAR")) {
				request.getSession().setAttribute("bagagens", resultado);
				d = request.getRequestDispatcher("ConsultaPassagem.jsp");
			}
		}

		d.forward(request, response);
	}

}
