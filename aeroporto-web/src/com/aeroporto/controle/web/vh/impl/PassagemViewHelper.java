package com.aeroporto.controle.web.vh.impl;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Passagem;
import com.aeroporto.dominio.Pedido;

public class PassagemViewHelper implements IViewHelper {

	@Override
	public EntidadeImpl getEntidade(HttpServletRequest request) {

		String operacao = request.getParameter("operacao");
		Passagem passagem = null;

		if (!operacao.equals("VISUALIZAR")) {
			Pedido pedido = (Pedido) request.getSession().getAttribute("pedidoSessao");
			if("CONSULTAR".equals(operacao)) {
				pedido = new Pedido();
				pedido.setId(null);
			}
			passagem = new Passagem();
			passagem.setPedido(pedido);
			
			String status = request.getParameter("txtStatus");
			String idPassagem = request.getParameter("txtIdPassagem");
			String observacao = request.getParameter("txtObservacao");
			
			if (status != null && !status.trim().equals("")) {
				passagem.setStatus(status);
			}
			if (idPassagem != null && !idPassagem.trim().equals("")) {
				passagem.setId(Long.parseLong(idPassagem));
			}
			if (observacao != null && !observacao.trim().equals("")) {
				passagem.setObservacao(observacao);
			}
		} else {
			passagem = null;
			HttpSession session = request.getSession();
			Resultado resultado = (Resultado) session.getAttribute("resultado");
			String txtId = request.getParameter("txtId");
			Long id = null;

			if (txtId != null && !txtId.trim().equals("")) {
				id = Long.parseLong(txtId);
			}

			for (EntidadeImpl e : resultado.getEntidades()) {
				if (e.getId() == id) {
					passagem = (Passagem) e;
				}
			}
		}
		return passagem;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RequestDispatcher d = null;

		String operacao = request.getParameter("operacao");

		if (resultado.getMensagem() == null) {
			if (operacao.equals("SALVAR")) {
				resultado.setMensagem("Passagem comprada com sucesso!");
			}
			request.getSession().setAttribute("resultado", resultado);
			d = request.getRequestDispatcher("ConsultaPassagem.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("ALTERAR")) {

			request.getSession().setAttribute("resultado", null);
			d = request.getRequestDispatcher("ConsultaPassagem.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("VISUALIZAR")) {

			request.setAttribute("passagem", resultado.getEntidades().get(0));
			d = request.getRequestDispatcher("EditarPassagem.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("EXCLUIR")) {

			request.getSession().setAttribute("resultado", null);
			d = request.getRequestDispatcher("ConsultaPassagem.jsp");
		}

		if (resultado.getMensagem() != null) {
			if (operacao.equals("SALVAR") || operacao.equals("ALTERAR")) {
				request.getSession().setAttribute("resultado", resultado);
				d = request.getRequestDispatcher("ConsultaPassagem.jsp");
			}
		}

		d.forward(request, response);
	}

}
