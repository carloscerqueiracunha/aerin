package com.aeroporto.controle.web.vh.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Trajeto;
import com.aeroporto.dominio.Viagem;

public class ViagemViewHelper implements IViewHelper {

	@Override
	public EntidadeImpl getEntidade(HttpServletRequest request) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
		
		String operacao = request.getParameter("operacao");
		Viagem viagem = null;

		if (!operacao.equals("VISUALIZAR")) {
			Trajeto trajeto = new Trajeto();
			viagem = new Viagem();
			Aeroporto origem = new Aeroporto();
			Aeroporto destino = new Aeroporto();
			
			String id = request.getParameter("txtIdViagem");
			String idTrajeto = request.getParameter("txtIdTrajeto");

			if (id != null && !id.trim().equals("")) {
				viagem.setId(Long.parseLong(id));
			}

			String idOrigem = request.getParameter("txtOrigem");
			String idDestino = request.getParameter("txtDestino");

			if (idOrigem != null && !idOrigem.trim().equals("")) {
				origem.setId(Long.parseLong(idOrigem));
			} else {
				origem.setId(null);
			}
			if (idDestino != null && !idDestino.trim().equals("")) {
				destino.setId(Long.parseLong(idDestino));
			} else {
				destino.setId(null);
			}

			trajeto.setOrigem(origem);
			trajeto.setDestino(destino);
			
			viagem.setTrajeto(trajeto);
			
			if (idTrajeto != null && !idTrajeto.trim().equals("")) {
				viagem.getTrajeto().setId(Long.parseLong(idTrajeto));
			}
			
			String idaVolta = request.getParameter("txtIdaVolta");
			String dataHoraPartida = request.getParameter("txtDataHoraPartida");
			String dataHoraRetorno = request.getParameter("txtDataHoraRetorno");
			String qtdeAssentosEconomica = request.getParameter("txtQtdeAssentosEconomica");
			String qtdeAssentosExecutiva = request.getParameter("txtQtdeAssentosExecutiva");
			String qtdeAssentosPrimeira = request.getParameter("txtQtdeAssentosPrimeira");
			String precoEconomica = request.getParameter("txtPrecoEconomica");
			String precoExecutiva = request.getParameter("txtPrecoExecutiva");
			String precoPrimeira = request.getParameter("txtPrecoPrimeira");
			String acessibilidade = request.getParameter("txtAcessibilidade");
			
			if (idaVolta != null && !idaVolta.trim().equals("")) {
				viagem.setIdaVolta(true);
			} else {
				viagem.setIdaVolta(false);
			}
			
			if (dataHoraPartida != null && !dataHoraPartida.trim().equals("")) {
				dataHoraPartida = dataHoraPartida + ":00.000";
				viagem.setDataHoraPartida(LocalDateTime.parse(dataHoraPartida, formatter));
			}
			
			if (dataHoraRetorno != null && !dataHoraRetorno.trim().equals("")) {
				dataHoraRetorno = dataHoraRetorno + ":00.000";
				viagem.setDataHoraRetorno(LocalDateTime.parse(dataHoraRetorno, formatter));
			}
			
			if(qtdeAssentosEconomica != null && !qtdeAssentosEconomica.trim().equals("")) {
				viagem.setQtdeAssentosEconomica(Integer.parseInt(qtdeAssentosEconomica));
			}
			
			if(qtdeAssentosExecutiva != null && !qtdeAssentosExecutiva.trim().equals("")) {
				viagem.setQtdeAssentosExecutiva(Integer.parseInt(qtdeAssentosExecutiva));
			}
			
			if(qtdeAssentosPrimeira != null && !qtdeAssentosPrimeira.trim().equals("")) {
				viagem.setQtdeAssentosPrimeira(Integer.parseInt(qtdeAssentosPrimeira));
			}
			
			if(precoEconomica != null && !precoEconomica.trim().equals("")) {
				viagem.setPrecoEconomica(Double.parseDouble(precoEconomica));
			}
			
			if(precoExecutiva != null && !precoExecutiva.trim().equals("")) {
				viagem.setPrecoExecutiva(Double.parseDouble(precoExecutiva));
			}
			
			if(precoPrimeira != null && !precoPrimeira.trim().equals("")) {
				viagem.setPrecoPrimeira(Double.parseDouble(precoPrimeira));
			}
			
			if(acessibilidade != null && !acessibilidade.trim().equals("")) {
				viagem.setAcessibilidade(true);
			}else {
				viagem.setAcessibilidade(false);
			}
			
		} else {
			viagem = null;
			HttpSession session = request.getSession();
			Resultado resultado = (Resultado) session.getAttribute("resultadoViagem");
			String txtId = request.getParameter("txtIdViagem");
			Long id = null;

			if (txtId != null && !txtId.trim().equals("")) {
				id = Long.parseLong(txtId);
			}

			for (EntidadeImpl e : resultado.getEntidades()) {
				if (e.getId() == id) {
					viagem = (Viagem) e;
				}
			}
		}
		return viagem;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RequestDispatcher d = null;

		String operacao = request.getParameter("operacao");

		if (resultado.getMensagem() == null) {
			if (operacao.equals("SALVAR")) {
				resultado.setMensagem("Viagem cadastrada com sucesso!");
			}
			request.getSession().setAttribute("resultadoViagem", resultado);
			d = request.getRequestDispatcher("ConsultaViagem.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("ALTERAR")) {
			
			d = request.getRequestDispatcher("ConsultaViagem.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("VISUALIZAR")) {

			request.setAttribute("viagem", resultado.getEntidades().get(0));
			request.getSession().setAttribute("viagemPedido", resultado.getEntidades().get(0));
			d = request.getRequestDispatcher("EditarViagem.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("EXCLUIR")) {

			request.getSession().setAttribute("resultadoViagem", null);
			d = request.getRequestDispatcher("ConsultaViagem.jsp");
		}

		if (resultado.getMensagem() != null) {
			if (operacao.equals("SALVAR") || operacao.equals("ALTERAR")) {
				request.getSession().setAttribute("resultadoViagem", resultado);
				d = request.getRequestDispatcher("ConsultaViagem.jsp");
			}
		}

		d.forward(request, response);
	}

}
