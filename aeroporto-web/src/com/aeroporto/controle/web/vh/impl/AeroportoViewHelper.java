package com.aeroporto.controle.web.vh.impl;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.Aeroporto;
import com.aeroporto.dominio.Cidade;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Estado;
import com.aeroporto.dominio.Pais;

public class AeroportoViewHelper implements IViewHelper {

	@Override
	public EntidadeImpl getEntidade(HttpServletRequest request) {

		String operacao = request.getParameter("operacao");
		Aeroporto aeroporto = null;

		if (!operacao.equals("VISUALIZAR")) {
			aeroporto = new Aeroporto();
			Pais p = new Pais();
			Estado e = new Estado();
			Cidade c = new Cidade();

			String id = request.getParameter("txtIdAeroporto");

			if (id != null && !id.trim().equals("")) {
				aeroporto.setId(Long.parseLong(id));
			}

			String cepOuZip = request.getParameter("txtCepOuZip");
			String bairro = request.getParameter("txtBairro");
			String logradouro = request.getParameter("txtLogradouro");
			String numero = request.getParameter("txtNumero");
			String nome = request.getParameter("txtNome");
			String pais = request.getParameter("txtPais");
			String estado = request.getParameter("txtEstado");
			String cidade = request.getParameter("txtCidade");

			if (cepOuZip != null && !cepOuZip.trim().equals("")) {
				aeroporto.setCepOuZip(cepOuZip);
			}
			if (bairro != null && !bairro.trim().equals("")) {
				aeroporto.setBairro(bairro);
			}
			if (logradouro != null && !logradouro.trim().equals("")) {
				aeroporto.setLogradouro(logradouro);
			}
			if (numero != null && !numero.trim().equals("")) {
				aeroporto.setNumero(numero);
			}
			if (nome != null && !nome.trim().equals("")) {
				aeroporto.setNome(nome);
			}
			if (pais != null && !pais.trim().equals("")) {
				p.setId(Long.parseLong(pais));
			}
			if (estado != null && !estado.trim().equals("")) {
				e.setId(Long.parseLong(estado));
			}
			if (cidade != null && !cidade.trim().equals("")) {
				c.setId(Long.parseLong(cidade));
			}
			aeroporto.setPais(p);
			aeroporto.setEstado(e);
			aeroporto.setCidade(c);
		} else {
			aeroporto = null;
			HttpSession session = request.getSession();
			Resultado resultado = (Resultado) session.getAttribute("resultadoAeroporto");
			String txtId = request.getParameter("txtIdAeroporto");
			Long id = null;

			if (txtId != null && !txtId.trim().equals("")) {
				id = Long.parseLong(txtId);
			}

			for (EntidadeImpl e : resultado.getEntidades()) {
				if (e.getId() == id) {
					aeroporto = (Aeroporto) e;
				}
			}
		}
		return aeroporto;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RequestDispatcher d = null;

		String operacao = request.getParameter("operacao");

		if (resultado.getMensagem() == null) {
			if (operacao.equals("SALVAR")) {
				resultado.setMensagem("Aeroporto cadastrado com sucesso!");
			}
			request.getSession().setAttribute("resultadoAeroporto", resultado);
			d = request.getRequestDispatcher("ConsultaAeroporto.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("ALTERAR")) {

			d = request.getRequestDispatcher("ConsultaAeroporto.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("VISUALIZAR")) {

			request.setAttribute("aeroporto", resultado.getEntidades().get(0));
			d = request.getRequestDispatcher("EditarAeroporto.jsp");
		}

		if (resultado.getMensagem() == null && operacao.equals("EXCLUIR")) {

			request.getSession().setAttribute("resultadoAeroporto", null);
			d = request.getRequestDispatcher("ConsultaAeroporto.jsp");
		}

		if (resultado.getMensagem() != null) {
			if (operacao.equals("SALVAR") || operacao.equals("ALTERAR")) {
				request.getSession().setAttribute("resultadoAeroporto", resultado);
				d = request.getRequestDispatcher("ConsultaAeroporto.jsp");
			}
		}

		d.forward(request, response);
	}

}
