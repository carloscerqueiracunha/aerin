package com.aeroporto.controle.web.vh.impl;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aeroporto.controle.web.vh.IViewHelper;
import com.aeroporto.core.aplicacao.Resultado;
import com.aeroporto.dominio.EntidadeImpl;
import com.aeroporto.dominio.Grafico;

public class GraficoViewHelper implements IViewHelper {

	@Override
	public EntidadeImpl getEntidade(HttpServletRequest request) {
		Grafico grafico = new Grafico();
		
		String dataInicio = request.getParameter("dataInicio");
		String dataFim = request.getParameter("dataFim");
		
		if (dataInicio != null && !dataInicio.trim().equals("")) {
			String[] auxInicio = dataInicio.split("-");
			int dia = 1;
			int mes = Integer.parseInt(auxInicio[0]);
			int ano = Integer.parseInt(auxInicio[1]);
			LocalDate dtInicio = LocalDate.of(ano, mes, dia);
			grafico.setDataInicio(dtInicio);
			request.getSession().setAttribute("dataInicio", dtInicio);
		}
		
		if (dataFim != null && !dataFim.trim().equals("")) {
			String[] auxFim = dataFim.split("-");
			int dia = 1;
			int mes = Integer.parseInt(auxFim[0]);
			int ano = Integer.parseInt(auxFim[1]);
			LocalDate dtFim = LocalDate.of(ano, mes, dia);
			grafico.setDataFim(dtFim);
			request.getSession().setAttribute("dataFim", dtFim);
		}
		
		return grafico;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RequestDispatcher d = null;

		if (resultado.getMensagem() == null) {
			request.getSession().setAttribute("grafico", resultado);
			d = request.getRequestDispatcher("Grafico.jsp");
		} else {
			request.setAttribute("resultadoGrafico", resultado);
			d = request.getRequestDispatcher("Grafico.jsp");
		}

		d.forward(request, response);
	}

}
