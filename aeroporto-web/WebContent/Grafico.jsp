<%@page import="java.time.LocalDate"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>An�lise</title>
<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<!-- Datepicker -->
<link href="vendor/datepicker/css/datepicker.css" rel="stylesheet">

<!-- Link para Chart.js -->
<script src="vendor/chart.js/Chart.bundle.min.js"></script>

<style>
canvas {
	-moz-user-select: none;
	-webkit-user-select: none;
	-ms-user-select: none;
}
</style>

<link href="estilo.css" rel="stylesheet">
</head>

<body id="page-top">
	<%
		Resultado graficos = (Resultado) request.getSession().getAttribute("grafico");
		LocalDate dataInicio = (LocalDate) request.getSession().getAttribute("dataInicio");
		LocalDate dataFim = (LocalDate) request.getSession().getAttribute("dataFim");
		Resultado resultado = (Resultado) request.getAttribute("resultadoGrafico");
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">


			<div class="container-fluid">
				<div class="container">
					<%
						if (resultado != null && resultado.getMensagem() != null) {
							out.print(resultado.getMensagem());
						}
					%>
					<form method="POST" action="Grafico" autocomplete="off">
						<div class="row">
							<div class="col-md-3">
								<label>Data Inicio:</label>
								<input type="text" class="form-control datepicker" name="dataInicio" id="dataInicio" required />
							</div>
							<div class="col-md-3">
								<label>Data Fim:</label>
								<input type="text" class="form-control datepicker" name="dataFim" id="dataFim" required />
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-3">
								<button type="submit" class="btn btn-success" name="operacao" id="operacao" value="CONSULTAR">Buscar</button>
							</div>
						</div>
						<br>
						<% if(graficos != null && graficos.getEntidades().size() == 0)
						out.print("<h4>N�o foram encontrados registros para essa busca!<h4>");%>
					</form>
					<div class="col-md-12 order-md-1">
						<canvas id="line-chart" class="grafico"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>

	<!-- DatePicker -->
	<script src="vendor/datepicker/js/bootstrap-datepicker.js"></script>

	<script type="text/javascript">
	var dataInicio = $("#dataInicio").datepicker( {
	    format: "mm-yyyy",
	    viewMode: "months",
	    minViewMode: "months"
	}).on('changeDate', function() {
		dataInicio.hide();
	}).data('datepicker');
	
	var dataFim = $("#dataFim").datepicker( {
	    format: "mm-yyyy",
	    viewMode: "months",
	    minViewMode: "months"
	}).on('changeDate', function() {
		dataFim.hide();
	}).data('datepicker');
	</script><!-- Script para gerar o gr�fico -->
	<%
		if (resultado == null || resultado.getMensagem() == null) {
			if (graficos != null) {
				int mesInicio = dataInicio.getMonthValue();
				int anoInicio = dataInicio.getYear();
				int mesFim = dataFim.getMonthValue();
				int anoFim = dataFim.getYear();
				out.print("<script type='text/javascript'>");
				out.print("new Chart(document.getElementById('line-chart'), { ");
				out.print("type : 'line', ");
				out.print("data : { ");
				out.print("labels : [ '0', ");
				do {
					out.print("'" + mesInicio + "/" + anoInicio + "'");
					if (mesInicio == mesFim && anoInicio == anoFim) {
						out.print(" ], ");
					} else {
						out.print(", ");
					}
					mesInicio++;
					if (mesInicio == 13) {
						mesInicio = 1;
						anoInicio++;
					}
				} while (mesInicio <= mesFim && anoInicio <= anoFim);
				int mesInicial = dataInicio.getMonthValue();
				int anoInicial = dataInicio.getYear();
				out.print("datasets : [ ");
				for (int i = 0; i < graficos.getEntidades().size(); i++) {
					Grafico grafico = (Grafico) graficos.getEntidades().get(i);
					out.print("{");
					out.print("data : [ 0, ");
					for (Integer ano = anoInicial, mes = mesInicial;; mes++) {
						if (mes == 13) {
							ano++;
							mes = 1;
						}
						if (grafico.getQuantidadeMes().get(mes.toString() + ano.toString()) == null) {
							if (mes <= mesFim && ano <= anoFim)
								out.print(" 0, ");
							else
								out.print(" 0 ");
						} else {
							if (mes <= mesFim && ano <= anoFim)
								out.print(grafico.getQuantidadeMes().get(mes.toString() + ano.toString()) + ", ");
							else
								out.print(grafico.getQuantidadeMes().get(mes.toString() + ano.toString()) + " ");
						}
						if (mes == mesFim && ano == anoFim) {
							break;
						}
					}
					out.print("], ");
					out.print("label : '" + grafico.getNomeOrigem() + " - " + grafico.getNomeDestino() + "', ");
					// Gerador de Cor randomica
					Random random = new Random();
					final char[] hex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
							'f' };
					char[] charCor = new char[7];
					int corNumerica = random.nextInt(0x1000000);

					charCor[0] = '#';
					for (int k = 1; k < 7; k++) {
						charCor[k] = hex[corNumerica & 0xf];
						corNumerica >>= 4;
					}
					String cor = new String(charCor);
					// Fim
					out.print("borderColor : '" + cor + "', ");
					out.print("fill : false } ");
					if (i == (graficos.getEntidades().size() - 1)) {
						out.print(" ] ");
					} else {
						out.print(" , ");
					}
				}
				out.print(" }, ");
				out.print(" options : { ");
				out.print(" elements : { ");
				out.print(" line : { ");
				out.print(" tension : 0, ");
				out.print(" } ");
				out.print(" }, ");
				out.print(" title : { ");
				out.print(" display : true, ");
				out.print(" text : 'Quantidade de passagens compradas de cada viagem por m�s' ");
				out.print(" } ");
				out.print(" } ");
				out.print(" }); ");
				out.print(" </script> ");
			}
		}
	%>
</body>
</html>