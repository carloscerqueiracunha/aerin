<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<title>Dados de passageiro</title>
<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">
</head>

<body id="page-top">
	<%
		Passageiro passageiro = (Passageiro) request.getAttribute("passageiroCompra");
		Viagem viagem = (Viagem) session.getAttribute("viagemPedido");
		List<Sexo> sexos = Arrays.asList(Sexo.values());
		List<TipoTelefone> tiposTelefone = Arrays.asList(TipoTelefone.values());
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">
		
			<div class="container-fluid">
			<h1>Comprar passagem</h1>
				<form action='Pedido' method='POST'>
					<input type="text" hidden="true" id="txtIdPassageiro" name="txtIdPassageiro" value="<% if(passageiro != null) out.print(passageiro.getId());%>" />
					<input type="text" hidden="true" id="txtValorEconomica" value="<%out.print(viagem.getPrecoEconomica());%>"/>
					<input type="text" hidden="true" id="txtValorExecutiva" value="<%out.print(viagem.getPrecoExecutiva());%>"/>
					<input type="text" hidden="true" id="txtValorPrimeira" value="<%out.print(viagem.getPrecoPrimeira());%>"/>
					<br>
					<hr>
					<%
					if(viagem != null){
						out.print("<h5>Trajeto</h5>");
						out.print("<br>");
						out.print(viagem.getTrajeto().getOrigem().getNome() + " - " + viagem.getTrajeto().getDestino().getNome());
						out.print("<hr>");
						out.print("<br>");
					}
					%>
					<label for="txtNome">Nome do passageiro*</label>
					<input disabled class="form-control" type="text" id="txtNome" name="txtNome" value=<%if (passageiro != null)
				out.print("'" + passageiro.getNome() + "'");
			else
				out.print("''");%> />
					<br>
					<label>Tipo de Passagem</label>
					<select class="form-control preco" id="txtTipoPassagem" name="txtTipoPassagem">
						<option value="Economica">Classe econ�mica</option>
						<option value="Executiva">Classe executiva</option>
						<option value="Primeira">Primeira classe</option>
					</select>
					<br>
					<label>Quantidade de passagens</label>
					<div class="row">
						<div class="col-md-2">
							<label>Adulto</label>
						</div>
						<div class="col-md-2">
							<label>Crian�a</label>
						</div>
						<div class="col-md-2">
							<label>Beb�</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<input type="number" class="form-control preco" name="txtQuantidadePassagensAdulto" id="txtQuantidadePassagensAdulto"/>
						</div>
						<div class="col-md-2">
							<input type="number" class="form-control preco" name="txtQuantidadePassagensCrianca" id="txtQuantidadePassagensCrianca"/>
						</div>
						<div class="col-md-2">
							<input type="number" class="form-control preco" name="txtQuantidadePassagensBebe" id="txtQuantidadePassagensBebe"/>
						</div>
					</div>
					<br>
					<hr>
					<label>Pre�o total</label>
					<input type="text" id="txtPrecoTotal" class="form-control" disabled/>
					<br>
					<hr>
					<label>Cart�o de cr�dito para pagamento</label>
					<select class="form-control" id="txtIdCartao" name="txtIdCartao">
						<option value=""></option>
						<%
						if (passageiro != null) {
							ArrayList<Cartao> cartoes = passageiro.getCartoes();
							StringBuilder sbRegistro = new StringBuilder();
							StringBuilder sbLink = new StringBuilder();

							if (cartoes != null) {
								for (int i = 0; i < cartoes.size(); i++) {
									out.print("<option value='");
									out.print(cartoes.get(i).getId());
									out.print("' ");
									out.print(">");
									out.print(cartoes.get(i).getBandeiraCartao().getNome() + " - " + cartoes.get(i).getNumero());
									out.print("</option>");
								}
							}
						}
						%>
					</select>
					<br>
					<button class="btn btn-success" type="submit" value="SALVAR" name="operacao">Comprar</button>
				</form>
			</div>
		</div>
	</div>
	
	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>
	
	<!-- Compra de passagem -->
	<script src="js/compra-passagem.js"></script>
</body>
</html>