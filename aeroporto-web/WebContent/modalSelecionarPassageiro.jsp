<!-- Modal -->
<div class="modal fade" id="modalSelecionarPassageiro" tabindex="-1" role="dialog" aria-labelledby="modalSelecionarPassageiroLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form method="POST" action="Passageiro">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Digite o Id do passageiro que vai comprar a passagem:</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="hidden" name="flgCompra" id="flgCompra" value="true" />
					<input type="text" name="txtIdPassageiro" id="txtIdPassageiro" class="form-control" />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" id="operacao" name="operacao" value="CONSULTAR">Continuar com a compra</button>
				</div>
			</div>
		</form>
	</div>
</div>