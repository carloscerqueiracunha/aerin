<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Dados de Viagem</title>

<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">

</head>

<body id="page-top">

	<%
		Aeroporto aeroporto = (Aeroporto) request.getAttribute("aeroporto");
		Viagem viagem = (Viagem) request.getAttribute("viagem");
		Resultado resultadoAeroporto = (Resultado) session.getAttribute("resultadoAeroporto");
		Resultado resultadoViagem = (Resultado) session.getAttribute("resultadoViagem");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp" %>

		<div id="content-wrapper">

			<div class="container-fluid">
			<%
					if (resultadoViagem != null && resultadoViagem.getMensagem() != null) {
						out.print(resultadoViagem.getMensagem());
					}
				%>
				<form action='Viagem' method='POST'>
					<h1>Consulta de Viagem</h1>
					<br>
					<div class="row">
						<div class="col">
							<label for="txtOrigem">Origem</label>
							<select class="form-control" name="txtOrigem">
								<option value=""></option>
								<%
									if (resultadoAeroporto != null) {
										List<EntidadeImpl> entidades = resultadoAeroporto.getEntidades();
										StringBuilder sbRegistro = new StringBuilder();
										StringBuilder sbLink = new StringBuilder();

										if (entidades != null) {
											for (int i = 0; i < entidades.size(); i++) {
												Aeroporto a = (Aeroporto) entidades.get(i);
												out.print("<option value='");
												out.print(a.getId());
												out.print("'>");
												out.print(a.getNome());
												out.print("</option>");
											}
										}
									}
								%>
							</select>
						</div>

						<div class="col">
							<label for="txtDestino">Destino</label>
							<select class="form-control" name="txtDestino">
								<option value=""></option>
								<%
									if (resultadoAeroporto != null) {
										List<EntidadeImpl> entidades = resultadoAeroporto.getEntidades();
										StringBuilder sbRegistro = new StringBuilder();
										StringBuilder sbLink = new StringBuilder();

										if (entidades != null) {
											for (int i = 0; i < entidades.size(); i++) {
												Aeroporto a = (Aeroporto) entidades.get(i);
												out.print("<option value='");
												out.print(a.getId());
												out.print("'>");
												out.print(a.getNome());
												out.print("</option>");
											}
										}
									}
								%>
							</select>
						</div>
					</div>
					<hr>
					<h4>Dados de Viagem</h4>
					<br>
					<div class="row">
						<div class="col-md-2 ml-4">
							<input type="checkbox" class="form-check-input" name="txtIdaVolta" value="idaVolta">
							<label for="txtIdaVolta">Ida e volta?</label>
							<button type="button" class="btn btn-box-tool" data-toggle="tooltip" data-placement="top" title="Viagem ser� de ida e volta? (Marque caso positivo)">
								<i class="fa fa-question-circle"></i>
							</button>
						</div>
					</div>
					<br>
					<label for="txtDataHoraPartida">Data e hora de Partida</label>
					<input class="data-hora form-control" type="text" id="txtDataHoraPartida" name="txtDataHoraPartida" />
					<br>
					<label for="txtDataHoraRetorno">Data e hora de retorno</label>
					<input class="data-hora form-control" type="text" id="txtDataHoraRetorno" name="txtDataHoraRetorno" />
					<br>
					<label for="txtQtdeAssentosEconomica">Quantidade de assentos da classe econ�mica</label>
					<input class="form-control" type="text" id="txtQtdeAssentosEconomica" name="txtQtdeAssentosEconomica">
					<br>
					<label for="txtQtdeAssentosExecutiva">Quantidade de assentos da classe executiva</label>
					<input class="form-control" type="text" id="txtQtdeAssentosExecutiva" name="txtQtdeAssentosExecutiva">
					<br>
					<label for="txtQtdeAssentosPrimeira">Quantidade de assentos da primeira classe</label>
					<input class="form-control" type="text" id="txtQtdeAssentosPrimeira" name="txtQtdeAssentosPrimeira">
					<br>
					<label for="txtPrecoEconomica">Pre�o da classe econ�mica</label>
					<input class="preco form-control" type="text" id="txtPrecoEconomica" name="txtPrecoEconomica" />
					<br>
					<label for="txtPrecoExecutiva">Pre�o da classe executiva</label>
					<input class="preco form-control" type="text" id="txtPrecoExecutiva" name="txtPrecoExecutiva" />
					<br>
					<label for="txtPrecoPrimeira">Pre�o da primeira classe</label>
					<input class="preco form-control" type="text" id="txtPrecoPrimeira" name="txtPrecoPrimeira" />
					<br>
					<div class="ml-4">
						<input type="checkbox" class="form-check-input" name="txtAcessibilidade">
						<label for="txtAcessibilidade">Fornece acessibilidade?</label>
						<button type="button" class="btn btn-box-tool" data-toggle="tooltip" data-placement="top" title="Viagem ir� oferecem assist�ncia a pessoas com defici�ncia">
							<i class="fa fa-question-circle"></i>
						</button>
					</div>
					<br>
					<input class="btn btn-success" type="submit" id="operacaoConsultar" name="operacao" value="CONSULTAR" />
				</form>
				<br>
				<H3>Viagens</H3>
				<table class="table">
					<tr>
						<th scope="col">Origem:</th>
						<th scope="col">Destino:</th>
						<th scope="col">Data e Hora da Partida:</th>
						<th scope="col">Data e Hora de Retorno:</th>
						<th scope="col">Acessibilidade:</th>
						<th scope="col">Status:</th>
					</tr>
					<%
						if (resultadoViagem != null) {
							List<EntidadeImpl> entidades = resultadoViagem.getEntidades();
							StringBuilder sbRegistro = new StringBuilder();
							StringBuilder sbLink = new StringBuilder();

							if (entidades != null) {
								if (entidades.size() == 0) {
									out.print("<tr><td colspan='5' class='text-center'>Nenhum registro encontrado!</td></tr>");
								}
								for (int i = 0; i < entidades.size(); i++) {
									Viagem v = (Viagem) entidades.get(i);
									sbRegistro.setLength(0);
									sbLink.setLength(0);

									sbRegistro.append("<TR>");

									sbLink.append("<a href=Viagem?");
									sbLink.append("txtIdViagem=");
									sbLink.append(v.getId());
									sbLink.append("&");
									sbLink.append("operacao=");
									sbLink.append("VISUALIZAR");

									sbLink.append(">");

									sbRegistro.append("<TD scope='row'>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(v.getTrajeto().getOrigem().getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(v.getTrajeto().getDestino().getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(v.getDataHoraPartida().format(formatter));
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(v.getDataHoraRetorno().format(formatter));
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									if (v.getAcessibilidade()) {
										sbRegistro.append("Possui Acessibilidade");
									} else {
										sbRegistro.append("N�o Possui Acessibilidade");
									}
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									if (v.isAtivo()) {
										sbRegistro.append("Ativo");
									} else {
										sbRegistro.append("Inativo");
									}
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("</TR>");

									out.print(sbRegistro.toString());

								}
							}
						}
					%>
				</table>

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>
	<script>
		$(function() {
			$('[data-toggle="tooltip"]').tooltip()
		});
		$(document).ready(function() {
			$('.data-hora').mask('00/00/0000 00:00');
			$('.preco').mask('999999900.00', {
				reverse : true
			});
		});
	</script>

</body>

</html>