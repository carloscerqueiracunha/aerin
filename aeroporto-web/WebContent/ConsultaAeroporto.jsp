<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Dados de Aeroporto</title>

<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">

</head>

<body id="page-top">

	<%
		Aeroporto aeroporto = (Aeroporto) request.getAttribute("aeroporto");
		Resultado resultadoAeroporto = (Resultado) session.getAttribute("resultadoAeroporto");
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp" %>

		<div id="content-wrapper">

			<div class="container-fluid">
				<%
					if (resultadoAeroporto != null && resultadoAeroporto.getMensagem() != null) {
						out.print(resultadoAeroporto.getMensagem());
					}
				%>
				<h1>Consulta de aeroporto</h1>
				<br>
				<form action="Aeroporto" method="POST">
					<label for="txtNome">Nome</label>
					<input class="form-control" type="text" id="txtNome" name="txtNome" />
					<br>
					<label for="txtPais">Pa�s</label>
					<select class="form-control" name="txtPais">
						<option value=""></option>
						<option value="1">Argentina</option>
						<option value="2">Alemanha</option>
						<option value="3">Brasil</option>
						<option value="4">Canad�</option>
						<option value="5">China</option>
						<option value="6">Estados Unidos da Am�rica</option>
						<option value="7">Fran�a</option>
						<option value="8">Russia</option>
						<option value="9">Jap�o</option>
						<option value="10">Paraguai</option>
					</select>
					<br>
					<label for="txtEstado">Estado</label>
					<select class="form-control" name="txtEstado">
						<option value=""></option>
						<option value="1">Buenos Aires</option>
						<option value="2">Berlim</option>
						<option value="3">S�o Paulo</option>
						<option value="4">Ont�rio</option>
						<option value="5">Shanxi</option>
						<option value="6">Fl�rida</option>
						<option value="7">Ilha de Fran�a</option>
						<option value="8">Moscou</option>
						<option value="9">Kant�</option>
						<option value="10">San Pedro</option>
					</select>
					<br>
					<label for="txtCidade">Cidade</label>
					<select class="form-control" name="txtCidade">
						<option value=""></option>
						<option value="1">La Plata</option>
						<option value="2">Berlim</option>
						<option value="3">S�o Paulo</option>
						<option value="4">Toronto</option>
						<option value="5">Taiyuan</option>
						<option value="6">Jacksonville</option>
						<option value="7">Paris</option>
						<option value="8">Moscovo</option>
						<option value="9">T�quio</option>
						<option value="10">San Pedro</option>
					</select>
					<br>
					<label for="txtCepOuZip">CEP/ZIP</label>
					<input class="form-control" type="text" id="txtCepOuZip" name="txtCepOuZip" />
					<br>
					<label for="txtBairro">Bairro</label>
					<input class="form-control" type="text" id="txtBairro" name="txtBairro" />
					<br>
					<label for="txtLogradouro">Logradouro</label>
					<input class="form-control" type="text" id="txtLogradouro" name="txtLogradouro" />
					<br>
					<label for="txtNumero">Numero</label>
					<input class="form-control" type="text" id="txtNumero" name="txtNumero" />
					<br>
					<input type="submit" class="btn btn-info" id="operacao" name="operacao" value="CONSULTAR" />
					<br>
				</form>
				<br>
				<H3>Aeroportos</H3>
				<table class="table">
					<tr>
						<th scope="col">Nome:</th>
						<th scope="col">Pais:</th>
						<th scope="col">Estado:</th>
						<th scope="col">Cidade:</th>
						<th scope="col">Status:</th>
					</tr>
					<%
						if (resultadoAeroporto != null) {
							List<EntidadeImpl> entidades = resultadoAeroporto.getEntidades();
							StringBuilder sbRegistro = new StringBuilder();
							StringBuilder sbLink = new StringBuilder();

							if (entidades != null) {
								if (entidades.size() == 0) {
									out.print("<tr><td colspan='5' class='text-center'>Nenhum registro encontrado!</td></tr>");
								}
								for (int i = 0; i < entidades.size(); i++) {
									Aeroporto a = (Aeroporto) entidades.get(i);
									sbRegistro.setLength(0);
									sbLink.setLength(0);

									sbRegistro.append("<TR>");

									sbLink.append("<a href=Aeroporto?");
									sbLink.append("txtIdAeroporto=");
									sbLink.append(a.getId());
									sbLink.append("&");
									sbLink.append("operacao=");
									sbLink.append("VISUALIZAR");

									sbLink.append(">");

									sbRegistro.append("<TD scope='row'>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(a.getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(a.getPais().getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(a.getEstado().getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(a.getCidade().getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									if (a.isAtivo()) {
										sbRegistro.append("Ativo");
									} else {
										sbRegistro.append("Inativo");
									}
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("</TR>");

									out.print(sbRegistro.toString());

								}
							}
						}
					%>
				</table>
			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>
	<script>
		$(function() {
			$('[data-toggle="tooltip"]').tooltip()
		});
		$(document).ready(function() {
			$('.data-hora').mask('00/00/0000 00:00');
			$('.preco').mask('999999900.00', {
				reverse : true
			});
		});
	</script>

</body>

</html>