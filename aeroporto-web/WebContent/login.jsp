<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Login</title>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body class="bg-secondary">
	<%
		Resultado resultadoLogin = (Resultado) session.getAttribute("resultadoLogin");
	%>
	<div id="nav"></div>

	<div class="container">
		<div class="card card-login mx-auto mt-5">
			<div class="text-center card-header">Login</div>
			<div class="card-body">
				<form method="POST" action="Usuario">
					<div class="form-group">
						<div class="form-label-group">
							<input type="email" id="txtEmail" name="txtEmail" class="form-control" required="required" autofocus="autofocus">
							<label for="txtEmail">E-mail</label>
						</div>
					</div>
					<div class="form-group">
						<div class="form-label-group">
							<input type="password" id="txtSenha" name="txtSenha" class="form-control" required="required">
							<label for="txtSenha">Senha</label>
						</div>
					</div>
					<div class="form-group">
						<div class="checkbox"></div>
					</div>
					<%
						if (resultadoLogin != null && resultadoLogin.getMensagem() != null) {
							out.print("* " + resultadoLogin.getMensagem() + "<br><br>");
						}
					%>
					<button type="submit" class="btn btn-success" value="CONSULTAR" name="operacao">Login</button>
				</form>
				<div class="text-center mt-3">
					Ainda n�o tem conta?
					<a href="EditarPassageiro.jsp">Registrar</a>
				</div>
			</div>
		</div>
	</div>

	<script>
		$("#nav").load("navbar.html");
	</script>

</body>
</html>