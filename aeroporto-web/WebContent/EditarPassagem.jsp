<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Passagem</title>
<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">
</head>

<body id="page-top">
	<%
		Passagem passagem = (Passagem) request.getAttribute("passagem");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">

			<div class="container-fluid">
				<h1>Passagem</h1>
				<br>
				<div class="row">
					<div class="col-md-4">
						<%
							if (passagem != null && passagem.getId() != null) {
								out.print("<label><strong>Id</strong></label>");
								out.print("<br>");
								out.print(passagem.getId());
								out.print("<br>");
								out.print("<br>");
							}
						%>
						<%
							if (passagem != null && passagem.getStatus() != null) {
								out.print("<label><strong>Status</strong></label>");
								out.print("<br>");
								out.print(passagem.getStatus());
								out.print("<br>");
								out.print("<br>");
							}
						%>
						<%
							if (passagem.getObservacao() != null &&	!passagem.getObservacao().equals("") &&
							(passagem.getStatus().equals("BARRADO RAIO-X")|| passagem.getStatus().equals("BARRADO ALFANDEGA"))) {
								out.print("<label><strong>Observacao</strong></label>");
								out.print("<br>");
								out.print(passagem.getObservacao());
								out.print("<br>");
								out.print("<br>");
							}
						%>
						<%
							if (passagem != null && passagem.getPreco() != null) {
								out.print("<label><strong>Preco</strong></label>");
								out.print("<br>");
								out.print(passagem.getPreco());
								out.print("<br>");
								out.print("<br>");
							}
						%>
						<%
							if (passagem != null && passagem.getFaixaEtaria() != null) {
								out.print("<label><strong>Faixa et�ria da passagem</strong></label>");
								out.print("<br>");
								out.print(passagem.getFaixaEtaria());
								out.print("<br>");
								out.print("<br>");
							}
						%>
						<%
							if (passagem != null && passagem.getTipo() != null) {
								out.print("<label><strong>Classe da passagem</strong></label>");
								out.print("<br>");
								out.print(passagem.getTipo());
								out.print("<br>");
								out.print("<br>");
							}
						%>
						<%
							if (passagem != null && passagem.getPedido() != null && passagem.getPedido().getViagem() != null
									&& passagem.getPedido().getViagem().getDataHoraPartida() != null) {
								out.print("<label><strong>Data e hora de partida</strong></label>");
								out.print("<br>");
								out.print(passagem.getPedido().getViagem().getDataHoraPartida().format(formatter));
								out.print("<br>");
								out.print("<br>");
							}
						%>
						<%
							if (passagem != null && passagem.getPedido() != null && passagem.getPedido().getViagem() != null
									&& passagem.getPedido().getViagem().getDataHoraRetorno() != null) {
								out.print("<label><strong>Data e hora de retorno</strong></label>");
								out.print("<br>");
								out.print(passagem.getPedido().getViagem().getDataHoraRetorno().format(formatter));
								out.print("<br>");
								out.print("<br>");
							}
						%>
						<!-- Fluxo feliz -->
						<form action='Passagem' method='POST'>
							<input type="hidden" id="txtIdPassagem" name="txtIdPassagem" value="<%out.print(passagem.getId());%>" />
							<input type="hidden" name="txtStatus" id="txtStatus"
								value=<%if (passagem != null) {
				if ("AGUARDANDO CHECK-IN".equals(passagem.getStatus())) {
					out.print("'EM RAIO-X'");
				} else if ("EM RAIO-X".equals(passagem.getStatus())) {
					if (passagem.getPedido().getViagem().getTrajeto().getOrigem().getPais().getId() != passagem
							.getPedido().getViagem().getTrajeto().getDestino().getPais().getId()) {
						out.print("'EM ALFANDEGA'");
					} else {
						out.print("'EM EMBARQUE'");
					}
				} else if ("EM ALFANDEGA".equals(passagem.getStatus())) {
					out.print("'EM EMBARQUE'");
				} else if ("EM EMBARQUE".equals(passagem.getStatus())) {
					out.print("'EM VOO'");
				} else if ("EM VOO".equals(passagem.getStatus())) {
					if (passagem.getPedido().getViagem().getIdaVolta()
							&& passagem.getPedido().getViagem().getDataHoraPartida().isBefore(LocalDateTime.now())
							&& passagem.getPedido().getViagem().getDataHoraRetorno().isAfter(LocalDateTime.now())) {
						out.print("'AGUARDANDO CHECK-IN'");
					} else {
						out.print("'ATERRISSOU'");
					}
				}
			}%> />
							<%
								if (passagem != null) {
									if (!"ATERRISSOU".equals(passagem.getStatus()) && !"BARRADO RAIO-X".equals(passagem.getStatus())
											&& !"BARRADO ALFANDEGA".equals(passagem.getStatus())) {
										out.print("<button class='btn btn-info' value='ALTERAR' name='operacao' id='operacao'>");
										if ("AGUARDANDO CHECK-IN".equals(passagem.getStatus())) {
											out.print("Realizar check-in");
										} else if ("EM RAIO-X".equals(passagem.getStatus())) {
											out.print("Aprovar raio-x");
										} else if ("EM ALFANDEGA".equals(passagem.getStatus())) {
											out.print("Aprovar alf�ndega");
										} else if ("EM EMBARQUE".equals(passagem.getStatus())) {
											out.print("Confirmar embarque");
										} else if ("EM VOO".equals(passagem.getStatus())) {
											if (passagem.getPedido().getViagem().getIdaVolta()
													&& passagem.getPedido().getViagem().getDataHoraPartida().isBefore(LocalDateTime.now())
													&& passagem.getPedido().getViagem().getDataHoraRetorno().isAfter(LocalDateTime.now())) {
												out.print("Realizar check-in");
											} else {
												out.print("Confirmar aterrissagem");
											}
										}
										out.print("</button>");
									}

								}
							%>
						</form>
						<!-- /Fim do fluxo feliz -->
						<br>
						<!-- Fluxo alternativo -->
						<form action="Passagem" method="POST">
						<% if (passagem != null) {
							if ("EM RAIO-X".equals(passagem.getStatus()) || "EM ALFANDEGA".equals(passagem.getStatus())) {
								out.print("<label>Observacao</label>");
								out.print("<textarea class='form-control' name='txtObservacao' id='txtObservacao' rows='4' cols='50' required></textarea><br>");
							}
						}
						%>
							<input type="hidden" id="txtIdPassagem" name="txtIdPassagem" value="<%out.print(passagem.getId());%>" />
							<input type="hidden" name="txtStatus" id="txtStatus"
								value=<%if (passagem != null) {
				if ("EM RAIO-X".equals(passagem.getStatus())) {
					out.print("'BARRADO RAIO-X'");
				} else if ("EM ALFANDEGA".equals(passagem.getStatus())) {
					out.print("'BARRADO ALFANDEGA'");
				}
			}%> />
							<%
								if (passagem != null) {
									if ("EM RAIO-X".equals(passagem.getStatus()) || "EM ALFANDEGA".equals(passagem.getStatus())) {
										out.print("<button class='btn btn-danger' value='ALTERAR' name='operacao' id='operacao'>");
										if ("EM RAIO-X".equals(passagem.getStatus())) {
											out.print("Barrar no raio-x");
										}
										if ("EM ALFANDEGA".equals(passagem.getStatus())) {
											out.print("Barrar na alfandega");
										}
										out.print("</button>");
									}

								}
							%>
						</form>
						<!-- /Fim do fluxo alternativo -->
					</div>
					<div class="col-md-8">
						<h5>Bagagens</h5>
						<table class="table text-center">
							<thead>
								<tr>
									<th scope="col">Peso</th>
									<th scope="col">Altura</th>
									<th scope="col">Largura</th>
									<th scope="col">Profundidade</th>
									<th scope="col">-</th>
								</tr>
							</thead>
							<%
								if (passagem != null) {
									StringBuilder sbRegistro = new StringBuilder();
									StringBuilder sbLink = new StringBuilder();

									if (passagem != null && "AGUARDANDO CHECK-IN".equals(passagem.getStatus())) {
										out.print(
												"<button type='button' class='btn btn-success' data-toggle='modal' data-target='#modalCadastrarBagagem'>Inserir Bagagem</button>");
										out.print("<br>");
										out.print("<br>");
									}
									if (passagem != null && passagem.getBagagens() != null) {
										if (passagem.getBagagens().size() == 0) {
											out.print("<tr><td colspan='5' class='text-center'>Nenhum registro encontrado!</td></tr>");
										}
										for (int i = 0; i < passagem.getBagagens().size(); i++) {
											Bagagem b = passagem.getBagagens().get(i);
											if (b.isAtivo()) {
												sbRegistro.setLength(0);
												sbLink.setLength(0);

												sbRegistro.append("<TR>");

												sbLink.append("<a href=Bagagem?");
												sbLink.append("txtId=");
												sbLink.append(b.getId());
												sbLink.append("&");
												sbLink.append("operacao=");
												sbLink.append("EXCLUIR");

												sbLink.append(">");

												sbRegistro.append("<TD scope='row'>");
												sbRegistro.append(b.getPeso());
												sbRegistro.append("</TD>");

												sbRegistro.append("<TD scope='row'>");
												sbRegistro.append(b.getAltura());
												sbRegistro.append("</TD>");

												sbRegistro.append("<TD>");
												sbRegistro.append(b.getLargura());
												sbRegistro.append("</TD>");

												sbRegistro.append("<TD>");
												sbRegistro.append(b.getProfundidade());
												sbRegistro.append("</TD>");

												sbRegistro.append("<TD>");
												if (passagem != null && "AGUARDANDO CHECK-IN".equals(passagem.getStatus())) {
													sbRegistro.append(sbLink.toString());
													sbRegistro.append("<button type='button' class='btn btn-danger'>Remover bagagem");
													sbRegistro.append("</button>");
													sbRegistro.append("</a>");
												}
												sbRegistro.append("</TD>");

												sbRegistro.append("</TR>");

												out.print(sbRegistro.toString());
											}

										}
									}
								}
							%>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="/scrolltop.jsp"%>
	<%@ include file="/modalCadastrarBagagem.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>
	<script>
		$(document).ready(function() {
			$('.double').mask('999999900.00', {
				reverse : true
			});
		});
	</script>

</body>
</html>