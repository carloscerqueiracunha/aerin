<!-- Sidebar -->
<ul class="sidebar navbar-nav">
	<li class="nav-item">
		<a class="nav-link" href="EditarPassageiro.jsp">
			<i class="fa fa-plus"></i>
			<span>Cadastrar Passageiro</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="Passageiro?operacao=CONSULTAR">
			<i class="fa fa-list"></i>
			<span>Lista de Passageiros</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="EditarAeroporto.jsp">
			<i class="fa fa-plus"></i>
			<span>Cadastrar Aeroporto</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="Aeroporto?operacao=CONSULTAR">
			<i class="fa fa-list"></i>
			<span>Lista de Aeroportos</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="EditarViagem.jsp">
			<i class="fa fa-plus"></i>
			<span>Cadastrar Viagem</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="Viagem?operacao=CONSULTAR">
			<i class="fa fa-list"></i>
			<span>Lista de Viagens</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="Pedido?operacao=CONSULTAR">
			<i class="fa fa-list"></i>
			<span>Lista de Pedidos</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="Passagem?operacao=CONSULTAR">
			<i class="fa fa-list"></i>
			<span>Lista de Passagens</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="Grafico?operacao=CONSULTAR&dataInicio=01-2018&dataFim=12-2018">
			<i class="fas fa-chart-line"></i>
			<span>Gr�fico de an�lise</span>
		</a>
	</li>
</ul>