<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Dados de Pedido</title>

<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">

</head>

<body id="page-top">

	<%
		Pedido pedido = (Pedido) request.getAttribute("pedido");
		Resultado resultado = (Resultado) session.getAttribute("resultado");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">

			<div class="container-fluid">
				<%
					if (resultado != null && resultado.getMensagem() != null) {
						out.print(resultado.getMensagem());
					}
				%>
				<h1>Lista de Pedidos</h1>
				<form action="Pedido" method="POST">
					<button class="btn btn-success" type="submit" id="operacao" name="operacao" value="CONSULTAR">Buscar Pedidos</button>
					<br>
					<br>
					<table class="table text-center">
						<thead>
							<tr>
								<th scope="col">Passageiro</th>
								<th scope="col">Origem</th>
								<th scope="col">Destino</th>
								<th scope="col">Data e Hora da Partida</th>
								<th scope="col">Data e Hora de Retorno</th>
								<th scope="col">Tipo de Passagem</th>
								<th scope="col">Preco Total</th>
								<th scope="col">Status</th>
							</tr>
						</thead>
						<%
						if (resultado != null) {
							List<EntidadeImpl> entidades = resultado.getEntidades();
							StringBuilder sbRegistro = new StringBuilder();
							StringBuilder sbLink = new StringBuilder();

							if (entidades != null) {
								if (entidades.size() == 0) {
									out.print("<tr><td colspan='8' class='text-center'>Nenhum registro encontrado!</td></tr>");
								}
								for (int i = 0; i < entidades.size(); i++) {
									Pedido p = (Pedido) entidades.get(i);
									sbRegistro.setLength(0);
									sbLink.setLength(0);

									sbRegistro.append("<TR>");

									sbLink.append("<a href=Pedido?");
									sbLink.append("txtId=");
									sbLink.append(p.getId());
									sbLink.append("&");
									sbLink.append("operacao=");
									sbLink.append("VISUALIZAR");

									sbLink.append(">");

									sbRegistro.append("<TD scope='row'>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getPassageiro().getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");
									
									sbRegistro.append("<TD scope='row'>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getViagem().getTrajeto().getOrigem().getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getViagem().getTrajeto().getDestino().getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getViagem().getDataHoraPartida().format(formatter));
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");
									
									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getViagem().getDataHoraRetorno().format(formatter));
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getTipoPassagem());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getPrecoTotal());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");
									
									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									if (p.isAtivo()) {
										sbRegistro.append("Ativo");
									} else {
										sbRegistro.append("Inativo");
									}
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("</TR>");

									out.print(sbRegistro.toString());

								}
							}
						}
					%>
					</table>
				</form>
			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>

</body>

</html>