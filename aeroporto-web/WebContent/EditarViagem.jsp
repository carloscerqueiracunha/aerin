<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Dados de Aeroporto</title>

<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">

</head>

<body id="page-top">

	<%
		Viagem viagem = (Viagem) request.getAttribute("viagem");
		Resultado resultadoAeroporto = (Resultado) session.getAttribute("resultadoAeroporto");
		Resultado resultadoTrajeto = (Resultado) session.getAttribute("resultadoViagem");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">

			<div class="container-fluid">
				<form action='Viagem' method='POST'>
					<input type="text" hidden="true" id="txtIdViagem" name="txtIdViagem" value="${viagem != null ? viagem.getId() : ''}" />
					<input type="text" hidden="true" id="txtIdTrajeto" name="txtIdTrajeto" value="${viagem != null ? viagem.getTrajeto().getId() : ''}" />
					<%
						if (viagem != null)
							out.print("<h1>Alterar Viagem</h1>");
						else
							out.print("<h1>Cadastro de Viagem</h1>");
					%>
					<br>
					<h4>Trajeto</h4>
					<div class="row">
						<div class="col">
							<label for="txtOrigem">Origem*</label>
							<select class="form-control" name="txtOrigem">
								<option value=""></option>
								<%
									if (resultadoAeroporto != null) {
										List<EntidadeImpl> entidades = resultadoAeroporto.getEntidades();
										StringBuilder sbRegistro = new StringBuilder();
										StringBuilder sbLink = new StringBuilder();

										if (entidades != null) {
											for (int i = 0; i < entidades.size(); i++) {
												Aeroporto a = (Aeroporto) entidades.get(i);
												out.print("<option value='");
												out.print(a.getId());
												out.print("' ");
												if (viagem != null) {
													if (a.getId() == viagem.getTrajeto().getOrigem().getId())
														out.print("selected");
												}
												out.print(">");
												out.print(a.getNome());
												out.print("</option>");
											}
										}
									}
								%>
							</select>
						</div>

						<div class="col">
							<label for="txtDestino">Destino*</label>
							<select class="form-control" name="txtDestino">
								<option value=""></option>
								<%
									if (resultadoAeroporto != null) {
										List<EntidadeImpl> entidades = resultadoAeroporto.getEntidades();
										StringBuilder sbRegistro = new StringBuilder();
										StringBuilder sbLink = new StringBuilder();

										if (entidades != null) {
											for (int i = 0; i < entidades.size(); i++) {
												Aeroporto a = (Aeroporto) entidades.get(i);
												out.print("<option value='");
												out.print(a.getId());
												out.print("' ");
												if (viagem != null) {
													if (a.getId() == viagem.getTrajeto().getDestino().getId())
														out.print("selected");
												}
												out.print(">");
												out.print(a.getNome());
												out.print("</option>");
											}
										}
									}
								%>
							</select>
						</div>
					</div>
					<hr>
					<h4>Dados de Viagem</h4>
					<br>
					<div class="row">
						<div class="col-md-2 ml-4">
							<input type="checkbox" class="form-check-input" name="txtIdaVolta" value="idaVolta" <%if (viagem != null) {
				if (viagem.getIdaVolta())
					out.print(" checked ");
			}%>>
							<label for="txtIdaVolta">Ida e volta?</label>
							<button type="button" class="btn btn-box-tool" data-toggle="tooltip" data-placement="top" title="Viagem ser� de ida e volta? (Marque caso positivo)">
								<i class="fa fa-question-circle"></i>
							</button>
						</div>
					</div>
					<br>
					<label for="txtDataHoraPartida">Data e hora de Partida*</label>
					<input class="data-hora form-control" type="text" id="txtDataHoraPartida" name="txtDataHoraPartida"
						value=<%if (viagem != null)
				out.print("'" + viagem.getDataHoraPartida().format(formatter) + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtDataHoraRetorno">Data e hora de retorno</label>
					<input class="data-hora form-control" type="text" id="txtDataHoraRetorno" name="txtDataHoraRetorno"
						value=<%if (viagem != null) {
				if (viagem.getDataHoraRetorno() != null)
					out.print("'" + viagem.getDataHoraRetorno().format(formatter) + "'");
				else
					out.print("''");
			} else
				out.print("''");%> />
					<br>
					<label for="txtQtdeAssentosEconomica">Quantidade de assentos da classe econ�mica</label>
					<input class="form-control" type="text" id="txtQtdeAssentosEconomica" name="txtQtdeAssentosEconomica"
						value=<%if (viagem != null) {
				if (viagem.getQtdeAssentosEconomica() != null)
					out.print("'" + viagem.getQtdeAssentosEconomica() + "'");
				else
					out.print("''");
			} else
				out.print("''");%> />
					<br>
					<label for="txtQtdeAssentosExecutiva">Quantidade de assentos da classe executiva</label>
					<input class="form-control" type="text" id="txtQtdeAssentosExecutiva" name="txtQtdeAssentosExecutiva"
						value=<%if (viagem != null) {
				if (viagem.getQtdeAssentosExecutiva() != null)
					out.print("'" + viagem.getQtdeAssentosExecutiva() + "'");
				else
					out.print("''");
			} else
				out.print("''");%> />
					<br>
					<label for="txtQtdeAssentosPrimeira">Quantidade de assentos da primeira classe</label>
					<input class="form-control" type="text" id="txtQtdeAssentosPrimeira" name="txtQtdeAssentosPrimeira"
						value=<%if (viagem != null) {
				if (viagem.getQtdeAssentosPrimeira() != null)
					out.print("'" + viagem.getQtdeAssentosPrimeira() + "'");
				else
					out.print("''");
			} else
				out.print("''");%> />
					<br>
					<label for="txtPrecoEconomica">Pre�o da classe econ�mica</label>
					<input class="preco form-control" type="text" id="txtPrecoEconomica" name="txtPrecoEconomica"
						value=<%if (viagem != null) {
				if (viagem.getPrecoEconomica() != null)
					out.print("'" + viagem.getPrecoEconomica() + "'");
				else
					out.print("''");
			} else
				out.print("''");%> />
					<br>
					<label for="txtPrecoExecutiva">Pre�o da classe executiva</label>
					<input class="preco form-control" type="text" id="txtPrecoExecutiva" name="txtPrecoExecutiva"
						value=<%if (viagem != null) {
				if (viagem.getPrecoExecutiva() != null)
					out.print("'" + viagem.getPrecoExecutiva() + "'");
				else
					out.print("''");
			} else
				out.print("''");%> />
					<br>
					<label for="txtPrecoPrimeira">Pre�o da primeira classe</label>
					<input class="preco form-control" type="text" id="txtPrecoPrimeira" name="txtPrecoPrimeira"
						value=<%if (viagem != null) {
				if (viagem.getPrecoPrimeira() != null)
					out.print("'" + viagem.getPrecoPrimeira() + "'");
				else
					out.print("''");
			} else
				out.print("''");%> />
					<br>
					<div class="ml-4">
						<input type="checkbox" class="form-check-input" name="txtAcessibilidade" <%if (viagem != null) {
				if (viagem.getAcessibilidade())
					out.print(" checked ");
			}%>>
						<label for="txtAcessibilidade">Fornece acessibilidade?</label>
						<button type="button" class="btn btn-box-tool" data-toggle="tooltip" data-placement="top" title="Viagem ir� oferecem assist�ncia a pessoas com defici�ncia">
							<i class="fa fa-question-circle"></i>
						</button>
					</div>
					<br>
					<%
						if (viagem != null) {
							if (viagem.isAtivo()) {
								out.print(
										"<input class='btn btn-warning' type='submit' id='operacaoAlterar' name='operacao' value='ALTERAR'/>");
								out.print(
										"<input class='btn btn-danger' type='submit' id='operacaoExcluir' name='operacao' value='EXCLUIR'/>");
								out.print(
										"<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalSelecionarPassageiro'>Comprar passagens</button>'");
							} else {
								out.print(
										"<input class='btn btn-success' type='submit' id='operacaoAtivar' name='operacao' value='ATIVAR'/>");
							}
						} else
							out.print(
									"<input class='btn btn-success' type='submit' id='operacaoSalvar' name='operacao' value='SALVAR'/>");
					%>
				</form>
			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<%@ include file="/scrolltop.jsp"%>
	<%@ include file="/modalSelecionarPassageiro.jsp"%>
	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>
	<script>
		$(function() {
			$('[data-toggle="tooltip"]').tooltip()
		});
		$(document).ready(function() {
			$('.data-hora').mask('00/00/0000 00:00');
			$('.preco').mask('999999900.00', {
				reverse : true
			});
		});
	</script>

</body>

</html>