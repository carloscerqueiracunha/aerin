<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<title>Consulta de passageiro</title>
<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">
</head>

<body id="page-top">
	<%
		Resultado resultado = (Resultado) session.getAttribute("resultado");
		List<Sexo> sexos = Arrays.asList(Sexo.values());
		List<TipoTelefone> tiposTelefone = Arrays.asList(TipoTelefone.values());
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">
			<div class="container-fluid">
				<h1>Consulta de passageiro</h1>
				<br>
				<%
					if (resultado != null && resultado.getMensagem() != null) {
						out.print(resultado.getMensagem());
					}
				%>
				<form action="Passageiro" method="POST">
					<label for="txtEmail">E-mail*</label>
					<input class="form-control" type="email" id="txtEmail" name="txtEmail" />
					<br>
					<label for="txtNome">Nome*</label>
					<input class="form-control" type="text" id="txtNome" name="txtNome" />
					<br>
					<label for="txtDtNascimento">Data de Nascimento*</label>
					<input class="form-control" type="date" id="txtDtNascimento" name="txtDtNascimento" />
					<br>
					<label for="txtSexo">Sexo*</label>
					<select class="form-control" id="txtSexo" name="txtSexo">
						<option value="">Selecione o sexo</option>

						<%
							for (Sexo sexo : sexos) {
								out.print("<option value='" + sexo + "'>" + sexo.getDescricao() + "</option>");
							}
						%>
					</select>
					<br>
					<label for="txtCpf">CPF*</label>
					<input class="form-control" type="text" id="txtCpf" name="txtCpf" />
					<br>
					<label for="txtTipoTelefone">Tipo de telefone*</label>
					<select class="form-control" id="txtTipoTelefone" name="txtTipoTelefone">
						<option value="">Selecione o tipo</option>

						<%
							for (TipoTelefone tipoTelefone : tiposTelefone) {
								out.print("<option value='" + tipoTelefone + "'>" + tipoTelefone.getDescricao() + "</option>");
							}
						%>
					</select>
					<br>
					<label for="txtTelefone">Telefone*</label>
					<input class="form-control" type="text" id="txtTelefone" name="txtTelefone" />
					<br>
					<input type="submit" class="btn btn-info" id="operacao" name="operacao" value="CONSULTAR" />
				</form>

				<BR>
				<BR>
				<H3>Passageiros</H3>
				<table class="table">
					<tr>
						<th scope="col">Id:</th>
						<th scope="col">Nome:</th>
						<th scope="col">CPF:</th>
						<th scope="col">Telefone:</th>
						<th scope="col">Status:</th>
					</tr>
					<%
						if (resultado != null) {
							List<EntidadeImpl> entidades = resultado.getEntidades();
							StringBuilder sbRegistro = new StringBuilder();
							StringBuilder sbLink = new StringBuilder();

							if (entidades != null) {
								for (int i = 0; i < entidades.size(); i++) {
									Passageiro p = (Passageiro) entidades.get(i);
									sbRegistro.setLength(0);
									sbLink.setLength(0);

									sbRegistro.append("<TR>");

									sbLink.append("<a href=Passageiro?");
									sbLink.append("txtId=");
									sbLink.append(p.getId());
									sbLink.append("&");
									sbLink.append("operacao=");
									sbLink.append("VISUALIZAR");

									sbLink.append(">");

									sbRegistro.append("<TD scope='row'>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getId());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");
									
									sbRegistro.append("<TD scope='row'>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getNome());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getCpf());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									sbRegistro.append(p.getTelefone());
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("<TD>");
									sbRegistro.append(sbLink.toString());
									if (p.isAtivo()) {
										sbRegistro.append("Ativo");
									} else {
										sbRegistro.append("Inativo");
									}
									sbRegistro.append("</a>");
									sbRegistro.append("</TD>");

									sbRegistro.append("</TR>");

									out.print(sbRegistro.toString());

								}
							}

						}
					%>
				</table>
			</div>
		</div>
	</div>
	
	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>
</body>
</html>