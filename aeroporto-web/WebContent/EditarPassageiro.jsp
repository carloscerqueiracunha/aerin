<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<title>Dados de passageiro</title>
<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">
</head>

<body id="page-top">
	<%
		Passageiro passageiro = (Passageiro) request.getAttribute("passageiro");
		List<Sexo> sexos = Arrays.asList(Sexo.values());
		List<TipoTelefone> tiposTelefone = Arrays.asList(TipoTelefone.values());
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">
			<div class="container-fluid">
				<%
					if (passageiro != null)
						out.print("<h1>Alterar passageiro</h1>");
					else
						out.print("<h1>Cadastro de passageiro</h1>");
				%>
				<form action="Passageiro" method="POST">
					<input type="text" hidden="true" id="txtId" name="txtId" value="${passageiro != null ? passageiro.getId() : ''}" />
					<input type="text" hidden="true" id="txtIdUsuario" name="txtIdUsuario" value="${passageiro != null ? passageiro.getUsuario().getId() : ''}" />
					<input type="text" hidden="true" id="txtIdCartao" name="txtIdCartao" value="${passageiro != null ? passageiro.getCartoes().get(0).getId() : ''}" />
					<%
						if (passageiro != null) {
							out.print("<label for='txtHash'>C�digo �nico</label>");
							out.print("<input class='form-control' type='text' id='txtHash' name='txtHash' readonly value= '"
									+ passageiro.getHash() + "'/>");
						}
					%>
					<br>
					<label for="txtEmail">E-mail*</label>
					<input class="form-control" type="email" id="txtEmail" name="txtEmail"
						value=<%if (passageiro != null)
				out.print("'" + passageiro.getUsuario().getEmail() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtSenha">Senha*</label>
					<input class="form-control" type="password" id="txtSenha" name="txtSenha"
						value=<%if (passageiro != null)
				out.print("'" + passageiro.getUsuario().getSenha() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtConfirmaSenha">Confirmacao de Senha*</label>
					<input class="form-control" type="password" id="txtConfirmaSenha" name="txtConfirmaSenha"
						value=<%if (passageiro != null)
				out.print("'" + passageiro.getUsuario().getSenha() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtNome">Nome*</label>
					<input class="form-control" type="text" id="txtNome" name="txtNome" value=<%if (passageiro != null)
				out.print("'" + passageiro.getNome() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtDtNascimento">Data de Nascimento*</label>
					<input class="form-control" type="date" id="txtDtNascimento" name="txtDtNascimento"
						value=<%if (passageiro != null)
				out.print("'" + passageiro.getDtNascimento() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtSexo">Sexo*</label>
					<select class="form-control" id="txtSexo" name="txtSexo">
						<option value="">Selecione o sexo</option>

						<%
							for (Sexo sexo : sexos) {
								if (passageiro != null && passageiro.getSexo().getDescricao().equals(sexo.getDescricao())) {
									out.print("<option selected value='" + sexo + "'>" + sexo.getDescricao() + "</option>");
								} else
									out.print("<option value='" + sexo + "'>" + sexo.getDescricao() + "</option>");
							}
						%>
					</select>
					<br>
					<label for="txtCpf">CPF*</label>
					<input class="form-control" type="text" id="txtCpf" name="txtCpf" value=<%if (passageiro != null)
				out.print("'" + passageiro.getCpf() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtTipoTelefone">Tipo de telefone*</label>
					<select class="form-control" id="txtTipoTelefone" name="txtTipoTelefone">
						<option value="">Selecione o tipo</option>

						<%
							for (TipoTelefone tipoTelefone : tiposTelefone) {
								if (passageiro != null
										&& passageiro.getTipoTelefone().getDescricao().equals(tipoTelefone.getDescricao())) {
									out.print("<option selected value='" + tipoTelefone + "'>" + tipoTelefone.getDescricao()
											+ "</option>");
								} else
									out.print("<option value='" + tipoTelefone + "'>" + tipoTelefone.getDescricao() + "</option>");
							}
						%>
					</select>
					<br>
					<label for="txtTelefone">Telefone*</label>
					<input class="form-control" type="text" id="txtTelefone" name="txtTelefone"
						value=<%if (passageiro != null)
				out.print("'" + passageiro.getTelefone() + "'");
			else
				out.print("''");%> />
					<br>
					<hr>
					<h2>Cart�o de cr�dito</h2>
					<label for="txtNumeroCartao">N�mero*</label>
					<input class="form-control" type="text" id="txtNumeroCartao" name="txtNumeroCartao"
						value=<%if (passageiro != null)
				out.print("'" + passageiro.getCartoes().get(0).getNumero() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtCodigoSeguranca">C�digo de seguran�a*</label>
					<input class="form-control" type="text" id="txtCodigoSeguranca" name="txtCodigoSeguranca"
						value=<%if (passageiro != null)
				out.print("'" + passageiro.getCartoes().get(0).getCodigoSeguranca() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtBandeiraCartao">Bandeira*</label>
					<select class="form-control" id="txtBandeiraCartao" name="txtBandeiraCartao">
						<option value="">Selecione a bandeira</option>
						<option <%if (passageiro != null && passageiro.getCartoes().get(0).getBandeiraCartao().getId() == 1)
				out.print("selected");%> value="1">Elo</option>
						<option <%if (passageiro != null && passageiro.getCartoes().get(0).getBandeiraCartao().getId() == 2)
				out.print("selected");%> value="2">Visa</option>
						<option <%if (passageiro != null && passageiro.getCartoes().get(0).getBandeiraCartao().getId() == 3)
				out.print("selected");%> value="3">MasterCard</option>
					</select>
					<br>
					<%
						if (passageiro != null) {
							if (passageiro.isAtivo()) {
								out.print(
										"<input class='btn btn-warning' type='submit' id='operacao' name='operacao' value='ALTERAR'/>");
								out.print(
										"<input class='btn btn-danger' type='submit' id='operacao' name='operacao' value='EXCLUIR'/>");
							} else {
								out.print(
										"<input class='btn btn-success' type='submit' id='operacao' name='operacao' value='ATIVAR'/>");
							}
						} else
							out.print(
									"<input class='btn btn-success' type='submit' id='operacao' name='operacao' value='SALVAR'/>");
					%>
				</form>
			</div>
		</div>
	</div>
	
	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>
</body>
</html>