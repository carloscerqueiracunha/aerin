<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<title>Passagens</title>
<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<!-- Grafico -->
<script src="vendor/grafico/chart.js/dist/Chart.js"></script>

<link href="estilo.css" rel="stylesheet">
</head>

<body id="page-top">
	<%
		Pedido pedido = (Pedido) session.getAttribute("pedidoSessao");
		Resultado resultado = (Resultado) session.getAttribute("resultado");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">

			<div class="container-fluid">
				<h2>Passagens</h2>
				<br>
				<form action="Passagem" method="POST">
					<label>Id da passagem</label>
					<div class="row">
						<div class="col-md-2">
							<input type="text" class="form-control" id="txtIdPassagem" name="txtIdPassagem" />
						</div>
					</div>
					<br>
					<button class="btn btn-success" type="submit" value="CONSULTAR" name="operacao" id="operacao">Buscar</button>
				</form>
				<br>
				<div class="row">
					<div class="col-md-6">
						<table class="table text-center">
							<thead>
								<tr>
									<th scope="col">Id</th>
									<th scope="col">Id Pedido</th>
									<th scope="col">Status</th>
									<th scope="col">Preco</th>
									<th scope="col">Faixa et�ria</th>
								</tr>
							</thead>
							<tbody id="table">
							</tbody>
						</table>
					</div>
					<div class="col-md-6">
						<canvas id="myChart"></canvas>
						<div class="row">
							<div class="col-md-8"></div>
							<div class="col-md-4">
								<button class="btn btn-success" type="button" id="mostraTudo">Mostrar tudo</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>

	<script type="text/javascript">
		window.onload = function() {
			var quantidadeAguardandoCheckIn = 0;
			var quantidadeRaioX = 0;
			var quantidadeBarradoRaioX = 0;
			var quantidadeAlfandega = 0;
			var quantidadeBarradoAlfandega = 0;
			var quantidadeEmbarque = 0;
			var quantidadeVoo = 0;
			var quantidadeAterrissou = 0;
			var passagens = [];
			var passagem = {};
			<% if (resultado != null && resultado.getEntidades() != null) {%>
				<% Passagem passagem; %>
				<%for (int i = 0; i < resultado.getEntidades().size(); i++) {%>
					<% passagem = (Passagem) resultado.getEntidades().get(i); %>
					passagem = {};
					passagem.id = <%=passagem.getId()%>;
					passagem.idPedido = <%=passagem.getPedido().getId()%>;
					passagem.status = '<%=passagem.getStatus()%>';
					passagem.preco = <%=passagem.getPreco()%>;
					passagem.faixaEtaria = '<%=passagem.getFaixaEtaria()%>';
					passagem.isAtivo = <%=passagem.isAtivo()%>;
					passagens[<%=i%>] = passagem;
					
					if(passagem.status === 'AGUARDANDO CHECK-IN')
						quantidadeAguardandoCheckIn++;
					else if(passagem.status === 'EM RAIO-X')
						quantidadeRaioX++;
					else if(passagem.status === 'BARRADO RAIO-X')
						quantidadeBarradoRaioX++;
					else if(passagem.status === 'EM ALFANDEGA')
						quantidadeAlfandega++;
					else if(passagem.status === 'BARRADO ALFANDEGA')
						quantidadeBarradoAlfandega++;
					else if(passagem.status === 'EM EMBARQUE')
						quantidadeEmbarque++;
					else if(passagem.status === 'EM VOO')
						quantidadeVoo++;
					else if(passagem.status === 'ATERRISSOU')
						quantidadeAterrissou++;
				<%}%>
			<%}%>
			var ctx = $("#myChart");
			var myPieChart = new Chart(ctx, {
				type : 'pie',
				data : {
					datasets : [ {
						data : [ quantidadeAguardandoCheckIn,
							quantidadeRaioX, quantidadeBarradoRaioX,
							quantidadeAlfandega, quantidadeBarradoAlfandega,
							quantidadeEmbarque, quantidadeVoo, quantidadeAterrissou ],
						backgroundColor : [ '#ffd800', '#0055ff', '#ff1000',
								'#ff6e00', '#000000', '#6a00ff', '#65ff00',
								'#006606' ]
					} ],
					labels : [ "AGUARDANDO CHECK-IN",
							"EM RAIO-X",
							"BARRADO RAIO-X",
							"EM ALFANDEGA",
							"BARRADO ALFANDEGA",
							"EM EMBARQUE",
							"EM VOO",
							"ATERRISSOU" ]
				},
				options : {
					responsive : true
				}
			});
			
			ctx.click(function(evt) {
				var activePoints = myPieChart.getElementsAtEvent(evt);
				var chartData = activePoints[0]['_chart'].config.data;
				var idx = activePoints[0]['_index'];
				
				var label = chartData.labels[idx];
				var value = chartData.datasets[0].data[idx];
				
				$('tr.item').each(function() { // loop em cada linha da tabela
				  	var trdata = $(this).find("td:eq(2)").text();
				  	if (trdata === label) {
				  		$(this).show();
				  	} else {
					  	$(this).hide();
				  	}
				  
				})
			});
			
			$('#mostraTudo').click(function(){
				$('tr.item').each(function() {
					$(this).show();
				})
			});
			
			// Mapeia a lista de passagens;
			var tabela = $("#table"); 
			for(var i = 0; i < passagens.length; i++){
				var id = passagens[i].id;
				tabela.append('<tr class="item"> <td scope="row">'+
					'<a href="Passagem?' +
					'txtId=' + passagens[i].id + '&operacao=VISUALIZAR">' +
					passagens[i].id + '</a></td>' +
					
					'<td scope="row">' +
					'<a href="Passagem?' +
					'txtId=' + passagens[i].id + '&operacao=VISUALIZAR">' +
					passagens[i].idPedido + '</a></td>' +
					
					'<td scope="row">' +
					'<a href="Passagem?' +
					'txtId=' + passagens[i].id + '&operacao=VISUALIZAR">' +
					passagens[i].status + '</a></td>' +
					
					'<td scope="row">' +
					'<a href="Passagem?' +
					'txtId=' + passagens[i].id + '&operacao=VISUALIZAR">' +
					passagens[i].preco + '</a></td>' +
					
					'<td scope="row">' +
					'<a href="Passagem?' +
					'txtId=' + passagens[i].id + '&operacao=VISUALIZAR">' +
					passagens[i].faixaEtaria + '</a></td>');
			}
		}
		
	</script>

</body>
</html>