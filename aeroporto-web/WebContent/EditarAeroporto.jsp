<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Dados de Aeroporto</title>

<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">

</head>

<body id="page-top">

	<%
		Aeroporto aeroporto = (Aeroporto) request.getAttribute("aeroporto");
		Resultado resultadoAeroporto = (Resultado) session.getAttribute("resultadoAeroporto");
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">

			<div class="container-fluid">
				<%
					if (aeroporto != null)
						out.print("<h1>Alterar aeroporto</h1>");
					else
						out.print("<h1>Cadastro de aeroporto</h1>");
				%>
				<form action="Aeroporto" method="POST">
					<input type="text" hidden="true" id="txtIdAeroporto" name="txtIdAeroporto" value="${aeroporto != null ? aeroporto.getId() : ''}" />
					<%
						if (aeroporto != null) {
							out.print("<label for='txtHash'>C�digo �nico</label>");
							out.print("<input class='form-control' type='text' id='txtHash' name='txtHash' readonly value= '"
									+ aeroporto.getHash() + "'/>");
						}
					%>
					<br>
					<label for="txtNome">Nome*</label>
					<input class="form-control" type="text" id="txtNome" name="txtNome" value=<%if (aeroporto != null)
				out.print("'" + aeroporto.getNome() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtPais">Pa�s*</label>
					<select class="form-control" name="txtPais">
						<option value=""></option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 1)
				out.print("selected");%> value="1">Argentina</option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 2)
				out.print("selected");%> value="2">Alemanha</option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 3)
				out.print("selected");%> value="3">Brasil</option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 4)
				out.print("selected");%> value="4">Canad�</option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 5)
				out.print("selected");%> value="5">China</option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 6)
				out.print("selected");%> value="6">Estados Unidos da Am�rica</option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 7)
				out.print("selected");%> value="7">Fran�a</option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 8)
				out.print("selected");%> value="8">Russia</option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 9)
				out.print("selected");%> value="9">Jap�o</option>
						<option <%if (aeroporto != null && aeroporto.getPais().getId() == 10)
				out.print("selected");%> value="10">Paraguai</option>
					</select>
					<br>
					<label for="txtEstado">Estado*</label>
					<select class="form-control" name="txtEstado">
						<option value=""></option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 1)
				out.print("selected");%> value="1">Buenos Aires</option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 2)
				out.print("selected");%> value="2">Berlim</option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 3)
				out.print("selected");%> value="3">S�o Paulo</option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 4)
				out.print("selected");%> value="4">Ont�rio</option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 5)
				out.print("selected");%> value="5">Shanxi</option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 6)
				out.print("selected");%> value="6">Fl�rida</option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 7)
				out.print("selected");%> value="7">Ilha de Fran�a</option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 8)
				out.print("selected");%> value="8">Moscou</option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 9)
				out.print("selected");%> value="9">Kant�</option>
						<option <%if (aeroporto != null && aeroporto.getEstado().getId() == 10)
				out.print("selected");%> value="10">San Pedro</option>
					</select>
					<br>
					<label for="txtCidade">Cidade*</label>
					<select class="form-control" name="txtCidade">
						<option value=""></option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 1)
				out.print("selected");%> value="1">La Plata</option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 2)
				out.print("selected");%> value="2">Berlim</option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 3)
				out.print("selected");%> value="3">S�o Paulo</option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 4)
				out.print("selected");%> value="4">Toronto</option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 5)
				out.print("selected");%> value="5">Taiyuan</option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 6)
				out.print("selected");%> value="6">Jacksonville</option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 7)
				out.print("selected");%> value="7">Paris</option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 8)
				out.print("selected");%> value="8">Moscovo</option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 9)
				out.print("selected");%> value="9">T�quio</option>
						<option <%if (aeroporto != null && aeroporto.getCidade().getId() == 10)
				out.print("selected");%> value="10">San Pedro</option>
					</select>
					<br>
					<label for="txtCepOuZip">CEP/ZIP*</label>
					<input class="form-control" type="text" id="txtCepOuZip" name="txtCepOuZip" value=<%if (aeroporto != null)
				out.print("'" + aeroporto.getCepOuZip() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtBairro">Bairro*</label>
					<input class="form-control" type="text" id="txtBairro" name="txtBairro" value=<%if (aeroporto != null)
				out.print("'" + aeroporto.getBairro() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtLogradouro">Logradouro*</label>
					<input class="form-control" type="text" id="txtLogradouro" name="txtLogradouro"
						value=<%if (aeroporto != null)
				out.print("'" + aeroporto.getLogradouro() + "'");
			else
				out.print("''");%> />
					<br>
					<label for="txtNumero">Numero*</label>
					<input class="form-control" type="text" id="txtNumero" name="txtNumero" value=<%if (aeroporto != null)
				out.print("'" + aeroporto.getNumero() + "'");
			else
				out.print("''");%> />
					<br>
					<%
						if (aeroporto != null) {
							if (aeroporto.isAtivo()) {
								out.print(
										"<input class='btn btn-warning' type='submit' id='operacaoAlterar' name='operacao' value='ALTERAR'/>");
								out.print(
										"<input class='btn btn-danger' type='submit' id='operacaoExcluir' name='operacao' value='EXCLUIR'/>");
							} else {
								out.print(
										"<input class='btn btn-success' type='submit' id='operacaoAtivar' name='operacao' value='ATIVAR'/>");
							}
						} else
							out.print(
									"<input class='btn btn-success' type='submit' id='operacaoSalvar' name='operacao' value='SALVAR'/>");
					%>
				</form>
			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>
	<script>
		$(function() {
			$('[data-toggle="tooltip"]').tooltip()
		});
		$(document).ready(function() {
			$('.data-hora').mask('00/00/0000 00:00');
			$('.preco').mask('999999900.00', {
				reverse : true
			});
		});
	</script>

</body>

</html>