<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page import="com.aeroporto.core.aplicacao.Resultado, com.aeroporto.dominio.*, java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<title>Pedido</title>
<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">

<link href="estilo.css" rel="stylesheet">
</head>

<body id="page-top">
	<%
		Pedido pedido = (Pedido) request.getAttribute("pedido");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	%>
	<%@ include file="/navtop.jsp"%>

	<div id="wrapper">

		<%@ include file="/sidebar.jsp"%>

		<div id="content-wrapper">

			<div class="container-fluid">
				<h1>Pedido</h1>
				<br>
				<form action='Pedido' method='POST'>
					<div class="row">
						<div class="col-md-4">
							<%
								if (pedido != null && pedido.getPassageiro() != null) {
									out.print("<label><strong>Passageiro</strong></label>");
									out.print("<br>");
									out.print(pedido.getPassageiro().getNome());
									out.print("<br>");
									out.print("<br>");
								}
							%>
							<%
								if (pedido != null && pedido.getViagem() != null) {
									out.print("<label><strong>Trajeto</strong></label>");
									out.print("<br>");
									out.print(pedido.getViagem().getTrajeto().getOrigem().getNome() + " - "
											+ pedido.getViagem().getTrajeto().getDestino().getNome());
									out.print("<br>");
									out.print("<br>");
								}
							%>
							<%
								if (pedido != null && pedido.getViagem() != null) {
									out.print("<label><strong>Data e hora de partida</strong></label>");
									out.print("<br>");
									out.print(pedido.getViagem().getDataHoraPartida().format(formatter));
									out.print("<br>");
									out.print("<br>");
								}
							%>
							<%
								if (pedido != null && pedido.getViagem() != null) {
									out.print("<label><strong>Data e hora de retorno</strong></label>");
									out.print("<br>");
									out.print(pedido.getViagem().getDataHoraRetorno().format(formatter));
									out.print("<br>");
									out.print("<br>");
								}
							%>
							<%
								if (pedido != null && pedido.getTipoPassagem() != null) {
									out.print("<label><strong>Tipo de Passagem</strong></label>");
									out.print("<br>");
									out.print(pedido.getTipoPassagem());
									out.print("<br>");
									out.print("<br>");
								}
							%>
							<%
								if (pedido != null && pedido.getPrecoTotal() != null) {
									out.print("<label><strong>Preco total</strong></label>");
									out.print("<br>");
									out.print(pedido.getPrecoTotal());
									out.print("<br>");
									out.print("<br>");
								}
							%>
						</div>
						<div class="col-md-8">
							<h5>Passagens</h5>
							<table class="table text-center">
								<thead>
									<tr>
										<th scope="col">Id</th>
										<th scope="col">Status</th>
										<th scope="col">Preco</th>
										<th scope="col">Faixa et�ria</th>
										<th scope="col">-</th>
									</tr>
								</thead>
								<%
									if (pedido != null) {
										StringBuilder sbRegistro = new StringBuilder();
										StringBuilder sbLink = new StringBuilder();

										if (pedido != null && pedido.getPassagens() != null) {
											if (pedido.getPassagens().size() == 0) {
												out.print("<tr><td colspan='5' class='text-center'>Nenhum registro encontrado!</td></tr>");
											}
											for (int i = 0; i < pedido.getPassagens().size(); i++) {
												Passagem p = pedido.getPassagens().get(i);
												sbRegistro.setLength(0);
												sbLink.setLength(0);

												sbRegistro.append("<TR>");

												sbLink.append(">");

												sbRegistro.append("<TD scope='row'>");
												sbRegistro.append(p.getId());
												sbRegistro.append("</TD>");

												sbRegistro.append("<TD scope='row'>");
												sbRegistro.append(p.getStatus());
												sbRegistro.append("</TD>");

												sbRegistro.append("<TD>");
												sbRegistro.append(p.getPreco());
												sbRegistro.append("</TD>");

												sbRegistro.append("<TD>");
												sbRegistro.append(p.getFaixaEtaria());
												sbRegistro.append("</TD>");

												sbRegistro.append("<TD>");
												if (p.isAtivo()) {
													sbRegistro.append("Ativo");
												} else {
													sbRegistro.append("Inativo");
												}
												sbRegistro.append("</TD>");

												sbRegistro.append("</TR>");

												out.print(sbRegistro.toString());

											}
										}
									}
								%>
							</table>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<%@ include file="/scrolltop.jsp"%>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin.min.js"></script>

	<!-- JQuery Mask -->
	<script src="vendor/jquery/jquery.mask.js"></script>

</body>
</html>