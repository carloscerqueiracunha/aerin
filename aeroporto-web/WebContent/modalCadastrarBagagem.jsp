<!-- Modal -->
<div class="modal fade" id="modalCadastrarBagagem" tabindex="-1" role="dialog" aria-labelledby="modalCadastrarBagagemLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form method="POST" action="Bagagem">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Cadastro de bagagens</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="hidden" id="txtIdPassagem" name="txtIdPassagem" value="<%out.print(passagem.getId()); %>" />
					<label>Peso</label>
					<input type="text" class="form-control double" name="txtPeso" id="txtPeso" />
					<label>Altura</label>
					<input type="text" class="form-control double" name="txtAltura" id="txtAltura" />
					<label>Largura</label>
					<input type="text" class="form-control double" name="txtLargura" id="txtLargura" />
					<label>Profundidade</label>
					<input type="text" class="form-control double" name="txtProfundidade" id="txtProfundidade" />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" id="operacao" name="operacao" value="SALVAR">Cadastrar Bagagem</button>
				</div>
			</div>
		</form>
	</div>
</div>