$('.preco')
		.change(
				function() {
					var tipoPassagem = $('#txtTipoPassagem').val();
					var quantidadeAdulto = $('#txtQuantidadePassagensAdulto')
							.val();
					var quantidadeCrianca = $('#txtQuantidadePassagensCrianca')
							.val();
					if (tipoPassagem == "Economica") {
						var valorPassagem = $('#txtValorEconomica').val();
					} else if (tipoPassagem == "Executiva") {
						var valorPassagem = $('#txtValorExecutiva').val();
					} else if (tipoPassagem == "Primeira") {
						var valorPassagem = $('#txtValorPrimeira').val();
					}
					document.getElementById("txtPrecoTotal").value = (quantidadeAdulto * valorPassagem)
							+ (quantidadeCrianca * valorPassagem / 2);
				});