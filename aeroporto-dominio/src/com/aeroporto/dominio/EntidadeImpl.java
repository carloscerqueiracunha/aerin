package com.aeroporto.dominio;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Entidade abstrada que deve ser extendida por todas as classes concretas.
 * Cont�m:
 * id, dtCadastro, dtUltimaAlteracao, ativo, hash
 * Suas vari�veis s�o inicializadas pelo construtor da classe
 * @author Carlos
 *
 */
public abstract class EntidadeImpl implements IEntidade{
	
	private Long id;
	private LocalDateTime dtCadastro;
	private LocalDateTime dtUltimaAlteracao;
	private boolean ativo;
	private UUID hash;
	
	public EntidadeImpl() {
		this.dtCadastro = LocalDateTime.now();
		this.dtUltimaAlteracao = LocalDateTime.now();
		this.hash = UUID.randomUUID();
		this.ativo = true;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LocalDateTime getDtCadastro() {
		return dtCadastro;
	}
	public void setDtCadastro(LocalDateTime dtCadastro) {
		this.dtCadastro = dtCadastro;
	}
	public LocalDateTime getDtUltimaAlteracao() {
		return dtUltimaAlteracao;
	}
	public void setDtUltimaAlteracao(LocalDateTime dtUltimaAlteracao) {
		this.dtUltimaAlteracao = dtUltimaAlteracao;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public UUID getHash() {
		return hash;
	}
	public void setHash(UUID hash) {
		this.hash = hash;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntidadeImpl other = (EntidadeImpl) obj;
		if (hash == null) {
			if (other.hash != null)
				return false;
		} else if (!hash.equals(other.hash))
			return false;
		return true;
	}
	
}
