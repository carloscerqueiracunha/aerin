package com.aeroporto.dominio;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Classe que cont�m os dados de um Passageiro
 * nome, dtNascimento, sexo, cpf, tipoTelefone, telefone, cartoes, usuario
 * @author Carlos
 *
 */
public class Passageiro extends EntidadeImpl{

	private String nome;
	private LocalDate dtNascimento;
	private Sexo sexo;
	private String cpf;
	private TipoTelefone tipoTelefone;
	private String telefone;
	private ArrayList<Cartao> cartoes;
	private Usuario usuario;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public LocalDate getDtNascimento() {
		return dtNascimento;
	}
	public void setDtNascimento(LocalDate dtNascimento) {
		this.dtNascimento = dtNascimento;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public TipoTelefone getTipoTelefone() {
		return tipoTelefone;
	}
	public void setTipoTelefone(TipoTelefone tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public ArrayList<Cartao> getCartoes() {
		return cartoes;
	}
	public void setCartoes(ArrayList<Cartao> cartoes) {
		this.cartoes = cartoes;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
