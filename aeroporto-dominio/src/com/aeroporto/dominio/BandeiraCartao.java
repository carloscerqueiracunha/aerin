package com.aeroporto.dominio;

/**
 * Classe que armazena os nomes das bandeiras de cartao
 * @author Carlos
 *
 */
public class BandeiraCartao extends EntidadeImpl{
	
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
