package com.aeroporto.dominio;

/**
 * ENUM para armazenar sexo
 * valores: FEMININO, MASCULINO
 * @author Carlos
 *
 */
public enum Sexo {
	
	FEMININO("Feminino"),
	MASCULINO("Masculino");
	
	private String descricao;
	
	Sexo(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
