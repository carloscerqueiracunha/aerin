package com.aeroporto.dominio;

/**
 * Classe que armazena os cart�es de um passageiro
 * Passageiro pode ter mais de um cart�o e cart�o � de apenas um passageiro
 * @author Carlos
 *
 */
public class Cartao extends EntidadeImpl{
	
	private String numero;
	private String codigoSeguranca;
	private BandeiraCartao bandeiraCartao;
	private Passageiro passageiro;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}
	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}
	public BandeiraCartao getBandeiraCartao() {
		return bandeiraCartao;
	}
	public void setBandeiraCartao(BandeiraCartao bandeiraCartao) {
		this.bandeiraCartao = bandeiraCartao;
	}
	public Passageiro getPassageiro() {
		return passageiro;
	}
	public void setPassageiro(Passageiro passageiro) {
		this.passageiro = passageiro;
	}
	
}
