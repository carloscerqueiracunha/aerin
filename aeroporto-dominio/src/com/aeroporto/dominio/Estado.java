package com.aeroporto.dominio;
/**
 * Classe para armazenar os Estados
 * @author Carlos
 *
 */
public class Estado extends EntidadeImpl {
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
