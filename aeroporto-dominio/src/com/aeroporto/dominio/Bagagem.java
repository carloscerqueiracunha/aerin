package com.aeroporto.dominio;

/**
 * Classe para armazenar dados de Bagagens
 * @author Carlos
 *
 */
public class Bagagem extends EntidadeImpl {
	private Double peso;
	private Double altura;
	private Double largura;
	private Double profundidade;
	private Passagem passagem;
	
	public Passagem getPassagem() {
		return passagem;
	}
	public void setPassagem(Passagem passagem) {
		this.passagem = passagem;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public Double getAltura() {
		return altura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public Double getLargura() {
		return largura;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public Double getProfundidade() {
		return profundidade;
	}
	public void setProfundidade(Double profundidade) {
		this.profundidade = profundidade;
	}
	
}
