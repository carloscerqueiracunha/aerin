package com.aeroporto.dominio;
/**
 * Classe para armazenar as cidades
 * @author Carlos
 *
 */
public class Cidade extends EntidadeImpl {
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
