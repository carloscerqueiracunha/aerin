package com.aeroporto.dominio;

import java.util.List;

public class Pedido extends EntidadeImpl {
	
	private Integer quantidadePassagensAdulto;
	private Integer quantidadePassagensCrianca;
	private Integer quantidadePassagensBebe;
	private Double precoTotal;
	private String tipoPassagem;
	private Viagem viagem;
	private Passageiro passageiro;
	private List<Passagem> passagens;
	private Cartao cartaoUtilizado;
	
	public Integer getQuantidadePassagensAdulto() {
		return quantidadePassagensAdulto;
	}
	public void setQuantidadePassagensAdulto(Integer quantidadePassagensAdulto) {
		this.quantidadePassagensAdulto = quantidadePassagensAdulto;
	}
	public Integer getQuantidadePassagensCrianca() {
		return quantidadePassagensCrianca;
	}
	public void setQuantidadePassagensCrianca(Integer quantidadePassagensCrianca) {
		this.quantidadePassagensCrianca = quantidadePassagensCrianca;
	}
	public Integer getQuantidadePassagensBebe() {
		return quantidadePassagensBebe;
	}
	public void setQuantidadePassagensBebe(Integer quantidadePassagensBebe) {
		this.quantidadePassagensBebe = quantidadePassagensBebe;
	}
	public Double getPrecoTotal() {
		return precoTotal;
	}
	public void setPrecoTotal(Double precoTotal) {
		this.precoTotal = precoTotal;
	}
	public String getTipoPassagem() {
		return tipoPassagem;
	}
	public void setTipoPassagem(String tipoPassagem) {
		this.tipoPassagem = tipoPassagem;
	}
	public Viagem getViagem() {
		return viagem;
	}
	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}
	public Passageiro getPassageiro() {
		return passageiro;
	}
	public void setPassageiro(Passageiro passageiro) {
		this.passageiro = passageiro;
	}
	public List<Passagem> getPassagens() {
		return passagens;
	}
	public void setPassagens(List<Passagem> passagens) {
		this.passagens = passagens;
	}
	public Cartao getCartaoUtilizado() {
		return cartaoUtilizado;
	}
	public void setCartaoUtilizado(Cartao cartaoUtilizado) {
		this.cartaoUtilizado = cartaoUtilizado;
	}
	
}
