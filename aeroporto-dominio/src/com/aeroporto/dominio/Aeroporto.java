package com.aeroporto.dominio;
/**
 * Classe para armazenar dados de endere�o
 * Endere�o ser� usado para definir um trajeto
 * @author Carlos
 *
 */
public class Aeroporto extends EntidadeImpl {
	private Pais pais;
	private Estado estado;
	private Cidade cidade;
	private String cepOuZip;
	private String bairro;
	private String logradouro;
	private String numero;
	private String nome;
	
	public Pais getPais() {
		return pais;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public String getCepOuZip() {
		return cepOuZip;
	}
	public void setCepOuZip(String cepOuZip) {
		this.cepOuZip = cepOuZip;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
