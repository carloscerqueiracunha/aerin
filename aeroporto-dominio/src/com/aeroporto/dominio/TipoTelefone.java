package com.aeroporto.dominio;

/**
 * ENUM para armazenar Tipos de Telefone
 * valores: FIXO, MOVEL
 * @author Carlos
 *
 */
public enum TipoTelefone {
	
	FIXO("Fixo"),
	MOVEL("Movel");
	
	private String descricao;
	
	TipoTelefone(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
