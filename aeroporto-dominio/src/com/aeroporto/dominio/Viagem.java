package com.aeroporto.dominio;

import java.time.LocalDateTime;
/**
 * Classe que armazena os dados de viagem
 * Possui o trajeto, idaVolta(bool), dataHoraPartida e Retorno, Assentos, acessibilidade
 * @author Carlos
 *
 */
public class Viagem extends EntidadeImpl {
	private Trajeto trajeto;
	private Boolean idaVolta;	// se true, viagem de ida e volta / se false, viagem s� de ida
	private LocalDateTime dataHoraPartida;
	private LocalDateTime dataHoraRetorno;
	private Integer qtdeAssentosEconomica;
	private Integer qtdeAssentosExecutiva;
	private Integer qtdeAssentosPrimeira;
	private Double precoEconomica;
	private Double precoExecutiva;
	private Double precoPrimeira;
	private Boolean acessibilidade;	// se true, fornece acessibilidade / se false, n�o fornece acessibilidade
	
	public Trajeto getTrajeto() {
		return trajeto;
	}
	public void setTrajeto(Trajeto trajeto) {
		this.trajeto = trajeto;
	}
	public Boolean getIdaVolta() {
		return idaVolta;
	}
	public void setIdaVolta(Boolean idaVolta) {
		this.idaVolta = idaVolta;
	}
	public LocalDateTime getDataHoraPartida() {
		return dataHoraPartida;
	}
	public void setDataHoraPartida(LocalDateTime dataHoraPartida) {
		this.dataHoraPartida = dataHoraPartida;
	}
	public LocalDateTime getDataHoraRetorno() {
		return dataHoraRetorno;
	}
	public void setDataHoraRetorno(LocalDateTime dataHoraRetorno) {
		this.dataHoraRetorno = dataHoraRetorno;
	}
	public Integer getQtdeAssentosEconomica() {
		return qtdeAssentosEconomica;
	}
	public void setQtdeAssentosEconomica(Integer qtdeAssentosEconomica) {
		this.qtdeAssentosEconomica = qtdeAssentosEconomica;
	}
	public Integer getQtdeAssentosExecutiva() {
		return qtdeAssentosExecutiva;
	}
	public void setQtdeAssentosExecutiva(Integer qtdeAssentosExecutiva) {
		this.qtdeAssentosExecutiva = qtdeAssentosExecutiva;
	}
	public Integer getQtdeAssentosPrimeira() {
		return qtdeAssentosPrimeira;
	}
	public void setQtdeAssentosPrimeira(Integer qtdeAssentosPrimeira) {
		this.qtdeAssentosPrimeira = qtdeAssentosPrimeira;
	}
	public Double getPrecoEconomica() {
		return precoEconomica;
	}
	public void setPrecoEconomica(Double precoEconomica) {
		this.precoEconomica = precoEconomica;
	}
	public Double getPrecoExecutiva() {
		return precoExecutiva;
	}
	public void setPrecoExecutiva(Double precoExecutiva) {
		this.precoExecutiva = precoExecutiva;
	}
	public Double getPrecoPrimeira() {
		return precoPrimeira;
	}
	public void setPrecoPrimeira(Double precoPrimeira) {
		this.precoPrimeira = precoPrimeira;
	}
	public Boolean getAcessibilidade() {
		return acessibilidade;
	}
	public void setAcessibilidade(Boolean acessibilidade) {
		this.acessibilidade = acessibilidade;
	}
	
}
