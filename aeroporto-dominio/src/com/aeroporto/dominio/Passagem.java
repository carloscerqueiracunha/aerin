package com.aeroporto.dominio;

import java.util.List;

/**
 * Classe para armazenar dados de Passagem
 * Passagem possui status para fazer a condu��o
 * Armazena passageiro e bagagens do mesmo
 * @author Carlos
 *
 */
public class Passagem extends EntidadeImpl{
	private String status;
	private Double preco;
	private List<Bagagem> bagagens;
	private String tipo;
	private String faixaEtaria;
	private Pedido pedido;
	private String observacao;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public List<Bagagem> getBagagens() {
		return bagagens;
	}
	public void setBagagens(List<Bagagem> bagagens) {
		this.bagagens = bagagens;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getFaixaEtaria() {
		return faixaEtaria;
	}
	public void setFaixaEtaria(String faixaEtaria) {
		this.faixaEtaria = faixaEtaria;
	}
	public Pedido getPedido() {
		return pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
}
