package com.aeroporto.dominio;
/**
 * Classe para armazenar os Pa�ses
 * @author Carlos
 *
 */
public class Pais extends EntidadeImpl {
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
