package com.aeroporto.dominio;

/**
 * Classe de usu�rio que � usada para fazer login no sistema
 * email, senha, confirmaSenha
 * @author Carlos
 *
 */
public class Usuario extends EntidadeImpl{

	private String email;
	private String senha;
	private String confirmaSenha;
	private Boolean admin;
	private Passageiro passageiro;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getConfirmaSenha() {
		return confirmaSenha;
	}
	public void setConfirmaSenha(String confirmaSenha) {
		this.confirmaSenha = confirmaSenha;
	}
	public Boolean getAdmin() {
		return admin;
	}
	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}
	public Passageiro getPassageiro() {
		return passageiro;
	}
	public void setPassageiro(Passageiro passageiro) {
		this.passageiro = passageiro;
	}
	
}
