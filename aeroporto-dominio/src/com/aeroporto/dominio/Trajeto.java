package com.aeroporto.dominio;
/**
 * Classe que vai armazenar o Trajeto de uma viagem
 * Possui origem e destino que s�o endere�os
 * @author Carlos
 *
 */
public class Trajeto extends EntidadeImpl {
	private Aeroporto origem;
	private Aeroporto destino;
	
	public Aeroporto getOrigem() {
		return origem;
	}
	public void setOrigem(Aeroporto origem) {
		this.origem = origem;
	}
	public Aeroporto getDestino() {
		return destino;
	}
	public void setDestino(Aeroporto destino) {
		this.destino = destino;
	}
	
}
