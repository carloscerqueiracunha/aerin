package com.aeroporto.dominio;

import java.time.LocalDate;
import java.util.Map;

public class Grafico extends EntidadeImpl {
	
	private Long idViagem;
	private String nomeOrigem;
	private String nomeDestino;
	private LocalDate dataInicio;
	private LocalDate dataFim;
	private Map<String, Integer> quantidadeMes;
	
	public Long getIdViagem() {
		return idViagem;
	}
	public void setIdViagem(Long idViagem) {
		this.idViagem = idViagem;
	}
	public String getNomeOrigem() {
		return nomeOrigem;
	}
	public void setNomeOrigem(String nomeOrigem) {
		this.nomeOrigem = nomeOrigem;
	}
	public LocalDate getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}
	public LocalDate getDataFim() {
		return dataFim;
	}
	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}
	public String getNomeDestino() {
		return nomeDestino;
	}
	public void setNomeDestino(String nomeDestino) {
		this.nomeDestino = nomeDestino;
	}
	public Map<String, Integer> getQuantidadeMes() {
		return quantidadeMes;
	}
	public void setQuantidadeMes(Map<String, Integer> quantidadeMes) {
		this.quantidadeMes = quantidadeMes;
	}
	
}
