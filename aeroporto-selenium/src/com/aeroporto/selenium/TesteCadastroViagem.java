package com.aeroporto.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TesteCadastroViagem {

	public static void main(String[] args) {

		System.setProperty("webdriver.gecko.driver",
				"C:\\Users\\carlo\\Downloads\\geckodriver-v0.21.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("http://localhost:8080/aeroporto-web/ConsultaAeroporto.jsp");
		driver.manage().window().maximize();
		driver.findElement(By.name("operacao")).click();
		
		/* Teste de cadastro de viagem */
		driver.get("http://localhost:8080/aeroporto-web/EditarViagem.jsp");
		Select selOrigem = new Select(driver.findElement(By.name("txtOrigem")));
		selOrigem.selectByValue("1");
		Select selDestino = new Select(driver.findElement(By.name("txtDestino")));
		selDestino.selectByValue("2");
		driver.findElement(By.name("txtIdaVolta")).click();
		driver.findElement(By.name("txtDataHoraPartida")).sendKeys("111220182000");
		driver.findElement(By.name("txtDataHoraRetorno")).sendKeys("131220182200");
		driver.findElement(By.name("txtQtdeAssentosEconomica")).sendKeys("20");
		driver.findElement(By.name("txtQtdeAssentosExecutiva")).sendKeys("20");
		driver.findElement(By.name("txtQtdeAssentosPrimeira")).sendKeys("20");
		driver.findElement(By.name("txtPrecoEconomica")).sendKeys("20000");
		driver.findElement(By.name("txtPrecoExecutiva")).sendKeys("30000");
		driver.findElement(By.name("txtPrecoPrimeira")).sendKeys("40000");
		driver.findElement(By.name("txtAcessibilidade")).click();
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("operacaoSalvar")).click();

		/* Teste de consulta de viagem */
		driver.get("http://localhost:8080/aeroporto-web/ConsultaAeroporto.jsp");
		driver.findElement(By.name("operacao")).click();
		
		driver.get("http://localhost:8080/aeroporto-web/ConsultaViagem.jsp");
		Select selOrigemConsulta = new Select(driver.findElement(By.name("txtOrigem")));
		selOrigemConsulta.selectByValue("1");
		Select selDestinoConsulta = new Select(driver.findElement(By.name("txtDestino")));
		selDestinoConsulta.selectByValue("2");
		driver.findElement(By.name("txtIdaVolta")).click();
		driver.findElement(By.name("txtDataHoraPartida")).sendKeys("111220182000");
		driver.findElement(By.name("txtDataHoraRetorno")).sendKeys("131220182200");
		driver.findElement(By.name("txtQtdeAssentosEconomica")).sendKeys("20");
		driver.findElement(By.name("txtQtdeAssentosExecutiva")).sendKeys("20");
		driver.findElement(By.name("txtQtdeAssentosPrimeira")).sendKeys("20");
		driver.findElement(By.name("txtPrecoEconomica")).sendKeys("20000");
		driver.findElement(By.name("txtPrecoExecutiva")).sendKeys("30000");
		driver.findElement(By.name("txtPrecoPrimeira")).sendKeys("40000");
		driver.findElement(By.name("txtAcessibilidade")).click();
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.name("operacao")).click();
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		/* Teste de edição de viagem */
		driver.get("http://localhost:8080/aeroporto-web/Viagem?txtIdViagem=1&operacao=VISUALIZAR");
		Select selOrigemEdicao = new Select(driver.findElement(By.name("txtOrigem")));
		selOrigemEdicao.selectByValue("2");
		Select selDestinoEdicao = new Select(driver.findElement(By.name("txtDestino")));
		selDestinoEdicao.selectByValue("1");
		driver.findElement(By.name("txtIdaVolta")).click();
		driver.findElement(By.name("txtDataHoraPartida")).clear();
		driver.findElement(By.name("txtDataHoraPartida")).sendKeys("111220192000");
		driver.findElement(By.name("txtDataHoraRetorno")).clear();
		driver.findElement(By.name("txtDataHoraRetorno")).sendKeys("131220192200");
		driver.findElement(By.name("txtQtdeAssentosEconomica")).clear();
		driver.findElement(By.name("txtQtdeAssentosEconomica")).sendKeys("40");
		driver.findElement(By.name("txtQtdeAssentosExecutiva")).clear();
		driver.findElement(By.name("txtQtdeAssentosExecutiva")).sendKeys("40");
		driver.findElement(By.name("txtQtdeAssentosPrimeira")).clear();
		driver.findElement(By.name("txtQtdeAssentosPrimeira")).sendKeys("40");
		driver.findElement(By.name("txtPrecoEconomica")).clear();
		driver.findElement(By.name("txtPrecoEconomica")).sendKeys("30000");
		driver.findElement(By.name("txtPrecoExecutiva")).clear();
		driver.findElement(By.name("txtPrecoExecutiva")).sendKeys("40000");
		driver.findElement(By.name("txtPrecoPrimeira")).clear();
		driver.findElement(By.name("txtPrecoPrimeira")).sendKeys("50000");
		driver.findElement(By.name("txtAcessibilidade")).click();
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("operacaoAlterar")).click();

		/* EXIBE */
		driver.get("http://localhost:8080/aeroporto-web/ConsultaViagem.jsp");
		driver.findElement(By.name("operacao")).click();
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		/* Teste de inativação de viagem */
		driver.get("http://localhost:8080/aeroporto-web/Viagem?txtIdViagem=1&operacao=VISUALIZAR");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("operacaoExcluir")).click();
		
		
		
		/* EXIBE */
		driver.get("http://localhost:8080/aeroporto-web/ConsultaViagem.jsp");
		driver.findElement(By.name("operacao")).click();
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		/* Teste de ativacao de viagem */
		driver.get("http://localhost:8080/aeroporto-web/Viagem?txtIdViagem=1&operacao=VISUALIZAR");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("operacaoAtivar")).click();
		driver.findElement(By.name("operacao")).click();
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
	}

}
