package com.aeroporto.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TesteCadastroAeroporto {

	public static void main(String[] args) {

		System.setProperty("webdriver.gecko.driver",
				"C:\\Users\\carlo\\Downloads\\geckodriver-v0.21.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("http://localhost:8080/aeroporto-web/EditarAeroporto.jsp");
		driver.manage().window().maximize();

		/* Testes de cadastro de aeroporto */
		driver.findElement(By.name("txtNome")).sendKeys("Aeroporto de Guarulhos");
		Select selPais = new Select(driver.findElement(By.name("txtPais")));
		selPais.selectByValue("3");
		Select selEstado = new Select(driver.findElement(By.name("txtEstado")));
		selEstado.selectByValue("3");
		Select selCidade = new Select(driver.findElement(By.name("txtCidade")));
		selCidade.selectByValue("3");
		driver.findElement(By.name("txtCepOuZip")).sendKeys("04626-911");
		driver.findElement(By.name("txtBairro")).sendKeys("Vila Congonhas");
		driver.findElement(By.name("txtLogradouro")).sendKeys("Av. Washington Luís");
		driver.findElement(By.name("txtNumero")).sendKeys("01");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("operacaoSalvar")).click();

		/* Testes de Consulta de aeroporto */
		driver.get("http://localhost:8080/aeroporto-web/ConsultaAeroporto.jsp");
		driver.findElement(By.name("txtNome")).sendKeys("Aeroporto de Guarulhos");
		Select selPaisConsulta = new Select(driver.findElement(By.name("txtPais")));
		selPaisConsulta.selectByValue("3");
		Select selEstadoConsulta = new Select(driver.findElement(By.name("txtEstado")));
		selEstadoConsulta.selectByValue("3");
		Select selCidadeConsulta = new Select(driver.findElement(By.name("txtCidade")));
		selCidadeConsulta.selectByValue("3");
		driver.findElement(By.name("txtCepOuZip")).sendKeys("04626-911");
		driver.findElement(By.name("txtBairro")).sendKeys("Vila Congonhas");
		driver.findElement(By.name("txtLogradouro")).sendKeys("Av. Washington Luís");
		driver.findElement(By.name("txtNumero")).sendKeys("01");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.name("operacao")).click();
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		/* Testes de Edição de dados de aeroporto */
		driver.get("http://localhost:8080/aeroporto-web/ConsultaAeroporto.jsp");
		driver.findElement(By.name("operacao")).click();
		driver.get("http://localhost:8080/aeroporto-web/Aeroporto?txtIdAeroporto=1&operacao=VISUALIZAR");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.name("txtNome")).clear();
		driver.findElement(By.name("txtNome")).sendKeys("Aeroporto Internacional de Tokyo");
		Select selPaisEdicao = new Select(driver.findElement(By.name("txtPais")));
		selPaisEdicao.selectByValue("9");
		Select selEstadoEdicao = new Select(driver.findElement(By.name("txtEstado")));
		selEstadoEdicao.selectByValue("9");
		Select selCidadeEdicao = new Select(driver.findElement(By.name("txtCidade")));
		selCidadeEdicao.selectByValue("9");
		driver.findElement(By.name("txtCepOuZip")).clear();
		driver.findElement(By.name("txtCepOuZip")).sendKeys("144-0041");
		driver.findElement(By.name("txtBairro")).clear();
		driver.findElement(By.name("txtBairro")).sendKeys("Ōta");
		driver.findElement(By.name("txtLogradouro")).clear();
		driver.findElement(By.name("txtLogradouro")).sendKeys("Hanedakuko");
		driver.findElement(By.name("txtNumero")).clear();
		driver.findElement(By.name("txtNumero")).sendKeys("01");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("operacaoAlterar")).click();

		/* Teste de inativacao */
		driver.get("http://localhost:8080/aeroporto-web/ConsultaAeroporto.jsp");
		driver.findElement(By.name("operacao")).click();
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.get("http://localhost:8080/aeroporto-web/Aeroporto?txtIdAeroporto=1&operacao=VISUALIZAR");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("operacaoExcluir")).click();

		/* Teste de ativação */
		driver.get("http://localhost:8080/aeroporto-web/ConsultaAeroporto.jsp");
		driver.findElement(By.name("operacao")).click();
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.get("http://localhost:8080/aeroporto-web/Aeroporto?txtIdAeroporto=1&operacao=VISUALIZAR");
		// delay para visualização
		try {
			Thread.sleep(7500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("operacaoAtivar")).click();

		/* EXIBE */
		driver.get("http://localhost:8080/aeroporto-web/ConsultaAeroporto.jsp");
		driver.findElement(By.name("operacao")).click();
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		
	}

}
